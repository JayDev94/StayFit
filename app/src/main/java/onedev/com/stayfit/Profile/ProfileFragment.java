package onedev.com.stayfit.Profile;



import onedev.com.stayfit.database.ProfileDatabase;
import onedev.com.stayfit.R;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.TextViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;



public class ProfileFragment extends Fragment {
	ProfileDatabase myDb;
	TextView editName, editAge, editHeight, editWeight;
	Button btnDeleteData;
	String name = "";
	Integer age = 0, height = 0, weight = 0;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.profile_fragment, container, false);
	}
	//View data
    public void getData() {
    	Log.i("testint", "Message");
                Cursor cursor = myDb.getAllData();
                //Check if their is data
                if (cursor.getCount() == 0) {
                    Toast.makeText(getActivity(), "Warning, no data",  Toast.LENGTH_SHORT).show();
                    return;
                }else{
                    StringBuffer buffer = new StringBuffer();
                    //Move through the data
                    while(cursor.moveToNext()){
                        //get data from index
                    	String resp = "";
                    	name = cursor.getString(1);
                    	resp = cursor.getString(2);
                    	age = Integer.parseInt(resp);
                    	resp = cursor.getString(3);
                    	height = Integer.parseInt(resp);
                    	resp = cursor.getString(4);
                    	weight = Integer.parseInt(resp);

                    }
                    //Show all data on edit texts
                    editName.setText(name);
                    editAge.setText(age + "");
                    editHeight.setText(height + "");
                    editWeight.setText(weight + "");
                    
                }
      
    }


	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		myDb = new ProfileDatabase(getActivity());
		
		editName = (TextView) view.findViewById(R.id.tvProfileName);
		editAge = (TextView) view.findViewById(R.id.tvProfileAge);
		editHeight = (TextView) view.findViewById(R.id.tvProfileHeight);
		editWeight = (TextView) view.findViewById(R.id.tvProfileWeight);
		btnDeleteData = (Button) view.findViewById(R.id.btnTestingDeleteData);

		//Get data from database and display on edit texts (CHANGE TO TEXTVIEWS)
		getData();
		
		
		///FOR TESTING//// DELETE TABLE//// 
		btnDeleteData.setOnClickListener(new View.OnClickListener() {	
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//DELETE TABLE FROM PROFILE DATABASE
				myDb.deleteTable();
				Toast.makeText(getActivity(), "RESET APP COMPLELETY", Toast.LENGTH_LONG).show();
			}
		});
		
		

	}
	
	

}