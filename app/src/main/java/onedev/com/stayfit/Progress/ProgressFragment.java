package onedev.com.stayfit.Progress;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

import onedev.com.stayfit.R;
import onedev.com.stayfit.RunningData.RouteData;

public class ProgressFragment extends Fragment {
  SectionsPagerAdapter sectionsPagerAdapter;
    ViewPager viewPager;
    RouteData routeData;
    ArrayList<String>routeDistanceList;
    ArrayList<String>routeIdList;
    ArrayList<String>routeDatesList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.progress_fragment_layout, container, false);
        sectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        viewPager = (ViewPager) view.findViewById(R.id.statsProgressPager);
        viewPager.setAdapter(sectionsPagerAdapter);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        //Data Tabel init.
        routeData = new RouteData(getActivity());
        //ArrayList init.
        routeDatesList = new ArrayList<>();
        routeIdList = new ArrayList<>();
        routeDistanceList = new ArrayList<>();

        //////////////////////////////TESTING DATA RETRIEVAL FOR ROUTE DATA TABLE//...
        getRouteData();
        for(int i =0; i < routeIdList.size(); i++){
            Log.i("TestingInProgressFr", routeIdList.get(i) + "");
        }


    }



    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        // Position of tabs  and Fragment invoking
        @Override
        public Fragment getItem(int position) {
            Fragment fragment = new TabbedContentFragment();
            Bundle args = new Bundle();
            args.putInt(TabbedContentFragment.SECTION_NUMBER, position + 1);

            switch (position) {
                case 0:
                    fragment = new WorkoutStats();   //Load the WorkoutStats fragment
                    break;
                case 1:
                    fragment = new RunStats();      //Load the RunStats fragment
                    break;
            }
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }
        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return "Completed Workouts";
                case 1:
                    return "Completed Runs";
            }
            return null;
        }
    }

    public static class TabbedContentFragment extends Fragment {
        public static final String SECTION_NUMBER = "section_number";

        public TabbedContentFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.tabbed_content_stats, container, false);
            return rootView;
        }
    }

    //Retrieve RouteData
    //Retrieve data from Routes, and Route Stats
    public void getRouteData() {
        Cursor cursor;
        try {
            cursor = routeData.getAllData();
            // Check for data
            if (cursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();

            } else {
                while (cursor.moveToNext()) {
                    //Add routeData
                    routeIdList.add(cursor.getString(0));
                    routeDatesList.add(cursor.getString(3));

                    //Distances
                    routeDistanceList.add(cursor.getString(2));
                    Log.i("RouteDistanceString", cursor.getString(2));
                    Log.i("RouteCordinatesLoad", cursor.getString(1));
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "No Data Loaded", Toast.LENGTH_SHORT).show();
        }
    }



}

