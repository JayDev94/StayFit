package onedev.com.stayfit.Progress;

/**
 * Created by Juan on 12/27/2016.
 */

public class StatsRowItem {
    private String title;
    private int icon;

    public StatsRowItem(String title, int icon) {
        this.title = title;
        this.icon = icon;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



}
