package onedev.com.stayfit.Progress;

/**
 * Created by Juan on 12/1/2016.
 */

import android.app.Activity;
import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import onedev.com.stayfit.R;
import onedev.com.stayfit.Run.MyTools;
import onedev.com.stayfit.Run.RouteImageLoad;

public class ProgressAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<String> distance;
    private ArrayList<String> time;
    private ArrayList<String> date;
    private static LayoutInflater inflater=null;
    public RouteImageLoad imageLoader;


    public ProgressAdapter(Activity a, ArrayList<String> distance, ArrayList<String> time, ArrayList<String>date) {
        activity = a;
        this.distance =distance;
        this.time = time;
        this.date = date;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader=new RouteImageLoad();
    }

    public int getCount() {
        return distance.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.progress_item_layout, null);

        TextView text=(TextView)vi.findViewById(R.id.progressTV);
        TextView textDate = (TextView) vi.findViewById(R.id.tvRouteProgressDate);
        TextView textTime = (TextView) vi.findViewById(R.id.tvRouteProgressTime);
        ImageView image = (ImageView)vi.findViewById(R.id.progressListImg);

        MyTools myTools = new MyTools();

        //Set the textviews to the corresponding position in the STR ArrayList of routes and dates
        //Convert string to double meters then to miles
        double dblMiles = 0;
        String holdDistance = distance.get(position);
        String holdTime = "";
        String holdDate = "";
       // dblMiles = myTools.convertStringToMiles(holdDistance);

        Log.i("HoldDistance", holdDistance);

        holdTime = time.get(position);
        holdDate = date.get(position);

        String showDate = holdDate;
        Date dtDate = new Date(Long.parseLong(showDate));
        DateFormat format = new SimpleDateFormat().getDateInstance();
        showDate = format.format(dtDate);

        text.setText(holdDistance + "mi");
        textDate.setText(showDate);
        textTime.setText(holdTime);
        //Show the image which corresponds to the route of that position


        File myDirectory = new File(Environment.getExternalStorageDirectory(), "StayFitSnapshots");
        String mPath = holdDate;


        //Trim the string from any spaces
        String trimmedString = mPath;
        trimmedString  = trimmedString.replace(" ", "");
        Log.i("TestingSeperator", "0000"+trimmedString+"0000");

        //geed the image loader the trimmed string containing the date and the imageview
        imageLoader.showImage(activity, trimmedString+".png", image);
        return vi;
    }
}
