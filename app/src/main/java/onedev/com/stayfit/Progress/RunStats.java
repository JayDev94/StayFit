package onedev.com.stayfit.Progress;

import java.util.ArrayList;

import onedev.com.stayfit.R;
import onedev.com.stayfit.Run.MyTools;
import onedev.com.stayfit.RunningData.RouteData;
import onedev.com.stayfit.RunningData.RouteStatsData;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Created by Juan on 12/27/2016.
 */

public class RunStats extends Fragment {


    //My tools
    MyTools myTools;
    ListView listView;
    ArrayList<String> workoutList;
    ArrayAdapter<String> adapter;
    ProgressAdapter progressAdapter;
    //Database classes
    RouteData routeData;
    RouteStatsData routeStatsData;
    //Route data
    ArrayList<Integer> routePositionList;
    ArrayList<String> routeDistanceList;
    ArrayList<String> routeDataDatesList;
    //Route stats data
    ArrayList<String> routesStatsList;
    ArrayList<String> listViewArrayList;
    ArrayList<String> newDistanceArrayList;
    ArrayList<String> newTimesList;
    ArrayList<String> newDatesList;
    Fragment fragment;

    //Data sent to RouteView
    String dataString = "";
    Bundle bundle;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        return inflater.inflate(R.layout.stats_run_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        myTools = new MyTools();
        //Initialize data classes
        routeData = new RouteData(getActivity());
        routeStatsData = new RouteStatsData(getActivity());

        routePositionList = new ArrayList<>();
        routeDistanceList = new ArrayList<>();
        routesStatsList = new ArrayList<>();
        routeDataDatesList = new ArrayList<>();



        listViewArrayList = new ArrayList<>();
        newDistanceArrayList = new ArrayList<>();
        newTimesList = new ArrayList<>();
        newDatesList = new ArrayList<>();

        workoutList = new ArrayList<>();


        //Retrieve route and stats data
        getRouteStatsData();
        getRouteData();

        //Combine the arraylists in the adapter if a date exists for the route to display
        if (!(routesStatsList.isEmpty())) {
            for (int i = 0; i < routesStatsList.size(); i++) {
               // newDistanceArrayList.add(routeDistanceList.get(routePositionList.get(i)) + "");
                newDistanceArrayList.add(routeDistanceList.get(i));
                newTimesList.add(routesStatsList.get(i));
                newDatesList.add(routeDataDatesList.get(i));
            }
        }else{
            Log.i("RouteStatsIsEmpty", "Route stats is empty" );
        }

        for(int i =0 ; i < newDistanceArrayList.size(); i++){
            Log.i("RouteDistanceList", newDistanceArrayList.get(i) + "");
        }
        for (int i =0 ; i < routeDistanceList.size(); i++){
            Log.i("DataRoutesDist", routeDistanceList.get(i) + "");
        }

        listView = (ListView) view.findViewById(R.id.listViewProgress);
        progressAdapter = new ProgressAdapter(getActivity(), newDistanceArrayList, newTimesList, newDatesList);
        listView.setAdapter(progressAdapter);

        Log.i("TestingRouteSize", " " + newDistanceArrayList.size() + " dates size " + newTimesList.size());


        //On item pressed  ----send bundle to route view and add
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                fragment = new RouteViewer();
                bundle = new Bundle();
                dataString = position + "";
                bundle.putString("RoutePosition", dataString);

                FragmentTransaction fT = getFragmentManager().beginTransaction();
                fT.add(R.id.fragmentProgressViewRoute, fragment);
                fT.addToBackStack(null);
                fragment.setArguments(bundle);
                fT.commit();

            }
        });
    }


    //Retrieve data from Routes, and Route Stats
    public void getRouteData() {
        Cursor cursor;
        try {
            cursor = routeData.getAllData();
            // Check for data
            if (cursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();

            } else {
                while (cursor.moveToNext()) {
                    //Distances
                    routeDistanceList.add(cursor.getString(2));

                    Log.i("RouteDistanceString", cursor.getString(2));
                    Log.i("RouteCordinatesLoad", cursor.getString(1));
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "No Data Loaded", Toast.LENGTH_SHORT).show();
        }
    }

    public void getRouteStatsData() {
        Cursor cursor;
        try {
            cursor = routeStatsData.getAllData();
            // Check for data
            if (cursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();
                Log.i("NoData", "No data was present during getRouteData()");
            } else {
                while (cursor.moveToNext()) {
                    Log.i("DataExists", "No data was present during getRouteData()");
                    //Route Positions
                    routePositionList.add(Integer.parseInt(cursor.getString(1)));
                    Log.i("RoutePosition", cursor.getString(1));

                    //Route Times
                    routesStatsList.add(cursor.getString(2));
                    Log.i("RouteTime", cursor.getString(2));

                    //Route Dates
                    routeDataDatesList.add(cursor.getString(6));


                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "No Data Loaded", Toast.LENGTH_SHORT).show();

        }
    }


}


