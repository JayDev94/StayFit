package onedev.com.stayfit.Progress;

import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import onedev.com.stayfit.R;
import onedev.com.stayfit.WorkoutDatabase.WorkoutData;
import onedev.com.stayfit.WorkoutDatabase.WorkoutStatsData;

/**
 * Created by Juan on 12/27/2016.
 */

public class WorkoutStats extends Fragment {
    //Data classes
    WorkoutData workoutData;
    WorkoutStatsData workoutStatsData;
    //Get date in this format
    SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
    //UI var
    ListView workoutsListView;  //holds the workouts and their dates
    //ArrayLists
    ArrayList<String> mainListviewList; //ArrayList for workoutsListView
    ArrayList<String> dataWorkoutIdList;
    ArrayList<String> dataWorkoutNameList;
    ArrayList<String> dataStatsIdList;
    ArrayList<String> dataStatsDateList;
    ArrayList<Integer> dataStatsWorkoutIdList;
    //Adapters
    ArrayAdapter<String> mainListViewAdapter; //ArrayAdapter for the workoutsListView

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        return inflater.inflate(R.layout.stats_workout_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        workoutData = new WorkoutData(getActivity());
        workoutStatsData = new WorkoutStatsData(getActivity());


        workoutsListView = (ListView) view.findViewById(R.id.statsWorkoutListview);

        //ArrayList init.
        dataWorkoutIdList = new ArrayList<>();
        dataWorkoutNameList = new ArrayList<>();
        dataStatsIdList = new ArrayList<>();
        dataStatsDateList = new ArrayList<>();
        dataStatsWorkoutIdList = new ArrayList<>();

        mainListviewList = new ArrayList<>();
        mainListViewAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, mainListviewList);
        workoutsListView.setAdapter(mainListViewAdapter);

        //Retrieve data
        retrieveWorkouts();
        retrieveStats();

        //Add the workouts saved in the dataWorkouts list using the setsWorkoutId key
        for (int x = 0; x < dataStatsWorkoutIdList.size(); x++) {
            for (int y = 0; y < dataWorkoutIdList.size(); y++) {
                if(dataStatsWorkoutIdList.get(x) == Integer.parseInt(dataWorkoutIdList.get(y))){
                    mainListviewList.add(dataWorkoutNameList.get(y) + "        " + dataStatsDateList.get(x));
                }
            }
        }
        mainListViewAdapter.notifyDataSetChanged();


    }


    ///////=========================RETRIEVE DATA==============================================
    public void retrieveWorkouts() {
        Cursor cursor;
        try {
            cursor = workoutData.getAllData();
            // Check for data
            if (cursor.getCount() == 0) {
                //No data
            } else {
                while (cursor.moveToNext()) {
                    Log.i("CursorWorkoutName", cursor.getString(1));

                    //Get Id
                    dataWorkoutIdList.add(cursor.getString(0));
                    //Get Name
                    dataWorkoutNameList.add(cursor.getString(1));
                }
            }
        } catch (Exception e) {
            Log.i("NoDateFound", "");
        }
    }

    public void retrieveStats() {
        Cursor cursor;
        try {
            cursor = workoutStatsData.getAllData();
            // Check for data
            if (cursor.getCount() == 0) {
                //No data
            } else {
                while (cursor.moveToNext()) {
                    Log.i("CursorStsWrkId", cursor.getString(1));
                    //Get Id
                    dataStatsIdList.add(cursor.getString(0));
                    //Get Date
                    dataStatsDateList.add(cursor.getString(1));
                    //Get Workout Id
                    dataStatsWorkoutIdList.add(Integer.parseInt(cursor.getString(2)));
                }
            }
        } catch (Exception e) {
            Log.i("NoDateFound", "");
        }
    }


}
