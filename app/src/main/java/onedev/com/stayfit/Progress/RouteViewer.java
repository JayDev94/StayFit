package onedev.com.stayfit.Progress;

/**
 * Created by Juan on 12/1/2016.
 */

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import onedev.com.stayfit.R;
import onedev.com.stayfit.Run.MapsActivity;
import onedev.com.stayfit.Run.MyTools;
import onedev.com.stayfit.RunningData.RouteData;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.w3c.dom.Text;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import onedev.com.stayfit.RunningData.RouteData;
import onedev.com.stayfit.RunningData.RouteStatsData;

/**
 * Created by Juan on 7/14/2016.
 */
public class RouteViewer extends Fragment implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener,
        GoogleMap.OnCameraChangeListener, GoogleMap.OnMyLocationButtonClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationSource.OnLocationChangedListener {

    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    // Keys for storing activity state in the Bundle.
    protected final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting-location-updates-key";
    protected final static String LOCATION_KEY = "location-key";
    protected final static String LAST_UPDATED_TIME_STRING_KEY = "last-updated-time-string-key";
    /**
     * Request code for location permission request.
     *
     * @see #onRequestPermissionsResult(int, String[], int[])
     */
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    /**
     * Flag indicating whether a requested permission has been denied after returning in
     * {@link #onRequestPermissionsResult(int, String[], int[])}.
     */

    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    //current location;
    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    public static final String TAG = MapsActivity.class.getSimpleName();
    //Map Fragment
    SupportMapFragment mapFragment;
    //MyTools
    MyTools myTools;
    //PolyOptions
    PolylineOptions userPolyOptions;
    ArrayList<Polyline> userLineList;
    Polyline userLocationLine;
    int userLineCount = 0;

    //This ArrayList will hold the stats data retrieved from a bundle (0Route-1Time-2Distance)
    String distance;
    ArrayList<String> allMilesList;
    ArrayList<String> mileTimesList;
    ArrayList<String> milesAndTimesList;


    //Conversion to miles or km
    boolean measureConvert = true;
    //RouteData and RouteStats tables
    RouteData routeData;
    RouteStatsData routeStatsData;
    //Bundle contents
    String bundleConents = "";

    //Route arraylists --------------------------------------------------
    ArrayList<String> routeStringList;
    ArrayList<String> routeDistanceList;
    ArrayList<String> routeDateList;
    ArrayList<LatLng> latLngArrayList;
    ArrayList<String> routeTimesList;
    ArrayList<String> routeStatsPositions;
    ArrayList<String> milesList;
    ArrayList<String> milesTimesList;
    ArrayList<String> milesListFromDB;
    ArrayList<String> milesTimesListFromDB;
    ArrayList<String> bottomSheetList;

    //RouteStatsData
    ArrayList<String> dataRouteStartTime;
    ArrayList<String> dataRouteEndTime;
    ArrayList<String> dataRouteCalories;
    ArrayList<String> dataRouteDates;


    //ArrayAdapters ------------------------------------------------------
    ArrayAdapter<String> bottomSheetAdapter;


    //-----------STATISTICS VARIABLES------------
    String strStartTime = "", strEndTIme = "", strDistance = "", strDuration = "", strAvPace = "", strAvgSpeed = "",
            strCalories = "", strBestMile = "", strBestMiletime = "",  strDate = "";
    String strPaceIndex = "";
    ArrayList<String> paceList;
    TextView tvStart, tvEnd, tvDistance, tvDuration, tvAvgPace, tvAvgSpeed, tvCalories, tvBestTime,
            tvPaceIndex, tvCurrentPace, tvDate;
    TextView btnDistance;
    Button btnClose;
    Button btnPrev;
    Button btnNext;
    Button btnConvert;
    Button btnSplitPace;

    ListView lvBottomSheet;

    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    private BottomSheetBehavior bottomSheetBehavior;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        routeData = new RouteData(getActivity());
        routeStatsData = new RouteStatsData(getActivity());
        //Get bundle content which is the route position in the ListView
        bundleConents = getArguments().getString("RoutePosition");


        return inflater.inflate(R.layout.progress_route_viewer_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.progressMapFragment);
        mapFragment.getMapAsync(this);
        //Start gps
        mGoogleApiClient.connect();
        myTools = new MyTools();
        //Ui init
        btnClose = (Button) view.findViewById(R.id.btnProgressRouteViewClose);
        btnSplitPace = (Button) view.findViewById(R.id.btnSplitPace);
        btnConvert = (Button) view.findViewById(R.id.btnRVUnitConvert);

        tvStart = (TextView) view.findViewById(R.id.tvRVStart);
        tvEnd = (TextView) view.findViewById(R.id.tvRVEnd);
        tvDistance = (TextView) view.findViewById(R.id.tvRVDistance);
        tvDuration = (TextView) view.findViewById(R.id.tvRVDuration);
        tvAvgPace = (TextView) view.findViewById(R.id.tvRVAvgPace);
        tvAvgSpeed = (TextView) view.findViewById(R.id.tvRVAvgSpeed);
        tvCalories = (TextView) view.findViewById(R.id.tvRVCalories);
        tvBestTime = (TextView) view.findViewById(R.id.tvRVBestTime);
        tvDate = (TextView) view.findViewById(R.id.tvRVDate);

        lvBottomSheet = (ListView) view.findViewById(R.id.lvBottomSheet);


        //ArrayList initialization
        routeStringList = new ArrayList<>();
        milesList = new ArrayList<>();
        latLngArrayList = new ArrayList<>();
        userLineList = new ArrayList<>();
        paceList = new ArrayList<>();
        routeDistanceList = new ArrayList<>();
        routeDateList = new ArrayList<>();
        routeTimesList = new ArrayList<>();
        routeStatsPositions = new ArrayList<>();
        milesList = new ArrayList<>();
        milesTimesList = new ArrayList<>();
        milesListFromDB = new ArrayList<>();
        milesTimesListFromDB = new ArrayList<>();
        bottomSheetList = new ArrayList<>();
        //Route stats data arraylist init.
        dataRouteStartTime = new ArrayList<>();
        dataRouteEndTime = new ArrayList<>();
        dataRouteCalories = new ArrayList<>();
        dataRouteDates = new ArrayList<>();


        //Retrieve data
        retrieveData();
        retrieveStatsData();

        //The routes position retrieved from bundle
        int routePosition = Integer.parseInt(bundleConents);

        //set the route
        setRoute(routePosition);
        //Use the current route to get the split pace miles
        splitPaceSettings(routePosition);
        //Get stats contents then set GUI components
        getSetStats(routePosition);


        //------------------------------------------------------------------------Bottom sheet setup
        final View bottomSheet = view.findViewById(R.id.bottom_sheet);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
        params.setBehavior(bottomSheetBehavior);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        for (int c = 0; c < bottomSheetList.size(); c++) {
            Log.i("BottomList ", bottomSheetList.get(c) + "");
        }


        //Bottom Sheet list view setup
        bottomSheetAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, bottomSheetList);
        lvBottomSheet.setAdapter(bottomSheetAdapter);

        bottomSheetAdapter.notifyDataSetChanged();

        //Method handles all button's logic
        buttonsPressed();


    }


    public void calculationsLoaded() {
        //Will holds the camulative distances until it equals 10 then will reset
        double distanceCounter = 0;
        //Make sure the map is loaded and ready
        if (mMap == null) {

        } else {
            //Create the new loaded array list line
            userPolyOptions = new PolylineOptions();
            userPolyOptions.addAll(latLngArrayList);
            userPolyOptions.color(Color.BLUE);
            userLocationLine = mMap.addPolyline(userPolyOptions);
            //Add the line to the polylinelist
            userLineList.add(userLocationLine);
            userLineCount++;

            //Set the camera at the starting point
            //Find the center point ////JUST ADDING A VALUE/////NEEDS TO BE FIXED
            int center = latLngArrayList.size() / 2;

            //if the LatLngArrayList is empty, do not copy the data
            // if(!(latLngArrayList))
            LatLng myCoordinates = new LatLng(latLngArrayList.get(center).latitude, latLngArrayList.get(center).longitude);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(myCoordinates, 15);
            mMap.animateCamera(cameraUpdate);
        }
    }


    //String to latlang conversions
    public void convertStringToLatLong(String routeString) {
        String strSp = routeString;
        //Split by space
        String[] splited = strSp.split("\\s+");
        //TESTING SPLITTED ARRAY
        for (int x = 0; x < splited.length; x++) {
            Log.i("SplitedArray", splited[x]);
        }
        //TESTING SPLITTED ARRAY BY COMMA
        String strTest1 = splited[1];
        String[] strTest = strTest1.split(",");
        String show = strTest[0];
        Log.i("SplittComma", " SPLIT" + show);
        // List<String> splitedList = new ArrayList<String>();
        //Clear the latlonglist and drawing lists for use
        //latLngArrayList.clear();
        //manualLineList.clear();
        for (int i = 1; i < splited.length; i++) {
            String[] strLatLangs = splited[i].split(",");
            Double lat = Double.parseDouble(strLatLangs[0]);
            Double lo = Double.parseDouble(strLatLangs[1]);
            LatLng location = new LatLng(lat, lo);
            latLngArrayList.add(location);
        }
        for (int t = 0; t < latLngArrayList.size(); t++) {
            Log.i("LatLngConverted", latLngArrayList.get(t).latitude + ", " + latLngArrayList.get(t).longitude);
        }
    }

    //Miles to Kilometers conversion
    public void milesOrKilometers() {
        //Miles to km
        if (!(measureConvert == true)) {
            for (int i = 0; i < allMilesList.size(); i++) {
                double newVal = 0;
                newVal = myTools.convertMiToKm(Double.parseDouble(allMilesList.get(i)));
                //Replace current val with km newVal
                allMilesList.set(i, newVal + "");
            }
            milesAndTimesList.clear();
            for (int i = 0; i < allMilesList.size(); i++) {
                milesAndTimesList.add(allMilesList.get(i) + " km                      " + mileTimesList.get(i));
            }
            //Change the distance lable to the corresponding measure
            double dblKm = 1.60934 * Double.parseDouble(distance);
            dblKm = Math.round(dblKm * 100.0) / 100.0;
            btnDistance.setText(dblKm + " km");
        } else {
            //km to miles
            for (int i = 0; i < allMilesList.size(); i++) {
                double newVal = 0;
                newVal = myTools.convertKmToMi(Double.parseDouble(allMilesList.get(i)));
                //Replace current val with miles newVal
                allMilesList.set(i, newVal + "");
            }
            milesAndTimesList.clear();
            for (int i = 0; i < allMilesList.size(); i++) {
                milesAndTimesList.add(allMilesList.get(i) + " mi                      " + mileTimesList.get(i));
            }
            //Change the distance lable to the corresponding measure
            double dblMi = 0.621371 * Double.parseDouble(distance);
            dblMi = Math.round(dblMi * 100.0) / 100.0;
            btnDistance.setText(dblMi + " mi");

        }
    }


    //Retrieves statistical info from both route tables in database
    public void getSetStats(int position){
        //strDate = routeDateList.get(position);
        Date date = new Date(Long.parseLong(dataRouteDates.get(position)));
        strDate = formatter.format(date);
        Log.i("DateFormatTest", strDate + "");


        Log.i("DateFormatTest1", dataRouteDates.get(position)+ "");

        strStartTime = dataRouteStartTime.get(position);
        strEndTIme = dataRouteEndTime.get(position);
        strDistance = "" + routeDistanceList.get(position) + "mi";
        strDuration = routeTimesList.get(position);
        strBestMile = routeTimesList.get(position);
        strCalories = dataRouteCalories.get(position);

        /////////////////////////////---------------Date setup
        //long milliSeconds= Long.parseLong(strDate);
        //String dateString = formatter.format(new Date(milliSeconds));
        /////////////////////////////---------------Date setup


        //UI setup-----------------------///////////////////////------------------
        // tvDate.setText(dateString);
        tvStart.setText(strStartTime);
        tvEnd.setText(strEndTIme);
        tvDate.setText(strDate);
        tvDistance.setText(strDistance);
        tvDuration.setText(strDuration);
        tvCalories.setText(strCalories);
    }

    //Split Pace settings
    public void splitPaceSettings(int position) {
        //Split miles and times by space
        String[] splittedMiles = milesListFromDB.get(position).trim().split("\\s+");
        String[] splittedTimes = milesTimesListFromDB.get(position).trim().split("\\s+");

        Log.i("CheckingMiles", "MilesList::::" + milesListFromDB.get(position) + "\n    TimesList::::" + milesTimesListFromDB.get(position));

        //TESTING SPLITTED ARRAY
        for (int x = 0; x < splittedMiles.length; x++) {
            milesList.add(splittedMiles[x]);
        }
        for (int x = 0; x < splittedTimes.length; x++) {
            milesTimesList.add(splittedTimes[x]);
        }

        Log.i("SizeOfMiles", milesList.size() + "");
        //Combine both lists into one to use for the Bottom Sheet Adapter
        for (int x = 0; x < milesList.size(); x++) {
            bottomSheetList.add("    " + milesList.get(x) + "mi                             " + milesTimesList.get(x));
        }

        Log.i("MilesListSize", milesList.size() + "");

        //Ge the best mile times.   ---find the smallest value after Double conversion from modification of mile time string---
        double topMileTime = 0;
        String modifiedMileTimes = milesTimesList.get(0).replaceAll(":", "");
        topMileTime = Double.parseDouble(modifiedMileTimes);
        strBestMiletime = milesTimesList.get(0);
        Log.i("ModifiedTextTest", modifiedMileTimes );
        for(int x = 0; x < milesTimesList.size(); x++){
            if(Double.parseDouble(milesTimesList.get(x).replace(":", "")) < topMileTime){
                topMileTime = Double.parseDouble(milesTimesList.get(x).replace(":", ""));
                strBestMiletime = milesTimesList.get(x);
            }
        }
        Log.i("BestMileTimeFinal", strBestMiletime );
        //Set the TextView with the best mile time
        tvBestTime.setText(strBestMiletime);
        Log.i("ModTopMileTime", topMileTime + "");


    }


    //Set the route settinsx
    public void setRoute(int position) {
        //The position is first passed on to the routStatsPosition list then onto the routeList to retrieve the ran route
        String strRouteRan = routeStringList.get(position);
        //Convert the string of
        convertStringToLatLong(strRouteRan);

    }


    //Data base retrieval
    // Retrieve data from ROUTES_TABLE   --------------RETRIEVE ROUTE, DATE, AND DISTANCE---------
    public void retrieveData() {
        Cursor cursor;
        try {
            cursor = routeData.getAllData();
            // Check for data
            if (cursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();

            } else {
                while (cursor.moveToNext()) {
                    // Get data from index
                    //Add to list
                    //Get the routes
                    routeStringList.add(cursor.getString(1));
                    //Get the dates of the route
                    routeDateList.add(cursor.getString(3));
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "No Data Loaded", Toast.LENGTH_SHORT).show();
        }
    }

    //////Route stats ----------RETRIEVE TIMES & ROUTE POSITION -----------
    public void retrieveStatsData() {
        Cursor cursor;
        try {
            cursor = routeStatsData.getAllData();
            // Check for data
            if (cursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();

            } else {
                while (cursor.moveToNext()) {
                    // Get data from index
                    //Retrieve the route times
                    routeTimesList.add(cursor.getString(2));
                    //Retrieve the route positions
                    routeStatsPositions.add(cursor.getString(1));
                    //Retrieve the distances
                    routeDistanceList.add(cursor.getString(3));
                    //Retrieve the miles
                    milesListFromDB.add((cursor.getString(4)));
                    //Retrieve the milesTimes
                    milesTimesListFromDB.add(cursor.getString(5));
                    //Retrieve the routeDates
                    dataRouteDates.add(cursor.getString(6));
                    //Retrieve the startTimes
                    dataRouteStartTime.add(cursor.getString(7));
                    //Retrieve the endTimes
                    dataRouteEndTime.add(cursor.getString(8));
                    //Retrieve the calories burned
                    dataRouteCalories.add(cursor.getString(9));

                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "No Data Loaded", Toast.LENGTH_SHORT).show();
        }
    }


    /******************************************************************************************
     * ------------------------------------GUI COMPONENT EVENT LISTENERS
     ****************************************************************************************/
    //Buttons
    public void buttonsPressed() {
        //On close button pressed
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Go back to the previous fragment
                getFragmentManager().popBackStackImmediate();
            }
        });
        //On SplitPace button pressed ------Expands bottom sheet
        btnSplitPace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
    }


    ////////////////////////////////////////////////////GOOGLE MAP OVERRIDE SETTINGS//////////////////////////////////////

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {

    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapClickListener(this);
        mMap.setOnMapLongClickListener(this);
        mMap.setOnCameraChangeListener(this);
        mMap.setOnMyLocationButtonClickListener(this);
        //enableMyLocation();

        //Connect google API
        mGoogleApiClient.connect();

        //Load polyline calculations once connected
        calculationsLoaded();


    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "Location services connected.");
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Location services suspended. Please reconnect.");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResume() {
        super.onResume();
        //Connect the google api
        mGoogleApiClient.connect();

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

}

