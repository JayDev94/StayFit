package onedev.com.stayfit.RunningData;

/*      StartTime, EndTime, & CaloriesBurned, have been added to RouteData    Jan 11, 2017 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class RouteData extends SQLiteOpenHelper {

    // Database name for the entire workout portion of the application
    public static final String DATABASE_NAME = "FitnessDatabase.db";

    // Exercise Table--------------------------------AND CONTENTS------
    public static final String ROUTES_TABLE = "routes_table";
    public static final String COL_1ID = "ID";
    public static final String COL_2LATLONG = "ROUTE";
    public static final String COL_3DISTANCE = "DISTANCE";
    public static final String COL_4DATETIME = "DATE";

    //public static final String COL_3DATE = "DATE";

    public RouteData(Context context) {
        super(context, ROUTES_TABLE, null, 1);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        // CReate the table
        db.execSQL("create table " + ROUTES_TABLE + " (" + COL_1ID + " INTEGER PRIMARY KEY, " + COL_2LATLONG +
                " TEXT, " + COL_3DISTANCE + " TEXT, " + COL_4DATETIME + " TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        // If table exists, drop
        db.execSQL("DROP TABLE IF EXISTS " + ROUTES_TABLE);
        // Pass this db instance
        onCreate(db);

    }

    // Insert data
    public boolean insertData(String routes, String distance, String date) {
        // Create database to write
        SQLiteDatabase db = this.getWritableDatabase();

        // To hold the content
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2LATLONG, routes);
        contentValues.put(COL_3DISTANCE, distance);
        contentValues.put(COL_4DATETIME, date);



        // Insert data into result to check for existence
        long result = db.insert(ROUTES_TABLE, null, contentValues);
        return result != -1;
    }

    // Get data
    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + ROUTES_TABLE, null);

        return cursor;
    }

    // Update data
    public boolean updateData(String id, String route, String distance, String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_1ID, id);
        contentValues.put(COL_2LATLONG, route);
        contentValues.put(COL_3DISTANCE, distance);
        contentValues.put(COL_4DATETIME, date);

        // Update data on the basis of id
        db.update(ROUTES_TABLE, contentValues, "ID = ?", new String[] { id });
        return true;
    }

    // Delete specific data
    public Integer deleteData(String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(ROUTES_TABLE, "ID = ?", new String[] { id });
    }

    public void deleteTable() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("delete from " + ROUTES_TABLE);

    }
}