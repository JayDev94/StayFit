package onedev.com.stayfit.RunningData;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class RouteStatsData extends SQLiteOpenHelper {

    // Database name for the entire workout portion of the application
    public static final String DATABASE_NAME = "FitnessDatabase.db";

    // Exercise Table--------------------------------AND CONTENTS------
    public static final String STATS_TABLE = "route_stats_table";
    public static final String COL_1ID = "ID";
    public static final String COL_2ROUTE_POSITION = "ROUTE";
    public static final String COL_3TIME = "TIME";
    public static final String COL_4DISTANCE = "DISTANCE";
    public static final String COL_5MILES = "MILES";
    public static final String COL_6MILE_TIMES = "MILES_TIMES";
    public static final String COL_7ROUTE_DATES = "ROUTE_DATES";
    public static final String COL_8STARTTIME = "START_TIME";
    public static final String COL_9ENDTIME= "END_TIME";
    public static final String COL_10CALORIES_BURNED = "CALORIES_BURNED";

    //public static final String COL_3DATE = "DATE";

    public RouteStatsData(Context context) {
        super(context, STATS_TABLE, null, 1);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        // CReate the table
        db.execSQL("create table " + STATS_TABLE + " ("
                + COL_1ID + " INTEGER PRIMARY KEY, "
                + COL_2ROUTE_POSITION + " INTEGER, "
                + COL_3TIME + " TEXT, "
                + COL_4DISTANCE + " TEXT, "
                + COL_5MILES + " TEXT, "
                + COL_6MILE_TIMES + " TEXT, "
                + COL_7ROUTE_DATES + " TEXT, "
                + COL_8STARTTIME + " TEXT, "
                + COL_9ENDTIME + " TEXT, "
                + COL_10CALORIES_BURNED + " TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        // If table exists, drop
        db.execSQL("DROP TABLE IF EXISTS " + STATS_TABLE);
        // Pass this db instance
        onCreate(db);

    }

    // Insert data
    public boolean insertData(int route, String time, String distance, String miles, String miletimes, String routeDates, String startTime, String endTime, String calories) {
        // Create database to write
        SQLiteDatabase db = this.getWritableDatabase();

        // To hold the content
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2ROUTE_POSITION, route);
        contentValues.put(COL_3TIME, time);
        contentValues.put(COL_4DISTANCE, distance);
        contentValues.put(COL_5MILES, miles);
        contentValues.put(COL_6MILE_TIMES, miletimes);
        contentValues.put(COL_7ROUTE_DATES, routeDates);
        contentValues.put(COL_8STARTTIME, startTime );
        contentValues.put(COL_9ENDTIME, endTime );
        contentValues.put(COL_10CALORIES_BURNED, calories );




        // Insert data into result to check for existence
        long result = db.insert(STATS_TABLE, null, contentValues);
        return result != -1;
    }

    // Get data
    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + STATS_TABLE, null);

        return cursor;
    }

    // Update data
    public boolean updateData(String id, int route, String distance, String miles, String miletimes, String routeDates,
                              String startTime, String endTime, String calories) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_1ID, id);
        contentValues.put(COL_2ROUTE_POSITION, route);
        contentValues.put(COL_3TIME, System.currentTimeMillis());
        contentValues.put(COL_4DISTANCE, distance);
        contentValues.put(COL_5MILES, miles);
        contentValues.put(COL_6MILE_TIMES, miletimes);
        contentValues.put(COL_7ROUTE_DATES, routeDates);
        contentValues.put(COL_8STARTTIME, startTime );
        contentValues.put(COL_9ENDTIME, endTime );
        contentValues.put(COL_10CALORIES_BURNED, calories );



        // Update data on the basis of id
        db.update(STATS_TABLE, contentValues, "ID = ?", new String[] { id });
        return true;
    }

    // Delete specific data
    public Integer deleteData(String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(STATS_TABLE, "ID = ?", new String[] { id });
    }

    public void deleteTable() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("delete from " + STATS_TABLE);

    }

    public void DELETEEVERYTHING(){
        SQLiteDatabase db  = this.getWritableDatabase();

    }
}