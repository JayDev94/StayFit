package onedev.com.stayfit.Settings;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;

import onedev.com.stayfit.R;
import onedev.com.stayfit.RunningData.RouteData;
import onedev.com.stayfit.RunningData.RouteStatsData;

public class SettingsFragment extends Fragment {
    RouteData routeData;
    RouteStatsData routeStatsData;

    //RouteData
    ArrayList<String> dataRoutesId;
    ArrayList<String> dataRoutesList;
    ArrayList<String> dataRoutesDistances;
    ArrayList<String> dataRoutesDates;

    //RouteStatsData
    ArrayList<Integer> dataStatsRoutePositions;
    ArrayList<String> dataStatsRouteTime;
    ArrayList<String> dataStatsRouteDistance;
    ArrayList<String> dataStatsRouteMiles;
    ArrayList<String> dataStatsRouteMileTime;
    ArrayList<String> dataStatsRouteDates;
    ArrayList<String> dataStatsRouteStarts;
    ArrayList<String> dataStatsRouteEnds;
    ArrayList<String> dataStatsRouteCalories;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.settings_layout, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        routeData = new RouteData(getActivity());
        routeStatsData = new RouteStatsData(getActivity());

        //Route data
        dataRoutesId = new ArrayList<>();
        dataRoutesList = new ArrayList<>();
        dataRoutesDistances = new ArrayList<>();
        dataRoutesDates = new ArrayList<>();

        //Route stats data
        dataStatsRoutePositions = new ArrayList<>();
        dataStatsRouteTime = new ArrayList<>();
        dataStatsRouteDistance = new ArrayList<>();
        dataStatsRouteMiles = new ArrayList<>();
        dataStatsRouteMileTime = new ArrayList<>();
        dataStatsRouteDates = new ArrayList<>();
        dataStatsRouteStarts = new ArrayList<>();
        dataStatsRouteEnds = new ArrayList<>();
        dataStatsRouteCalories = new ArrayList<>();

        retrieveData();
        retrieveStatsData();

        for (int i = 0; i < dataRoutesId.size(); i++) {
            Log.i("InTestingRouteId", dataRoutesId.get(i) + "");
        }
        for (int i = 0; i < dataRoutesList.size(); i++) {
            Log.i("InTestingRouteCord", dataRoutesList.get(i) + "");
        }
        for (int i = 0; i < dataRoutesDistances.size(); i++) {
            Log.i("InTestingRouteDistances", dataRoutesDistances.get(i) + "");
        }
        for (int i = 0; i < dataRoutesDates.size(); i++) {
            Log.i("InTestingRouteDates", dataRoutesDates.get(i) + "");
        }

        for(int i =0; i < dataStatsRoutePositions.size(); i++){
            Log.i("SettingsTestStats", "   \nPosition: " + dataStatsRoutePositions.get(i) +"   \nTime: " +  dataStatsRouteTime.get(i)+
                    "   \nDistance: " + dataStatsRouteDistance.get(i)+ "   \nCalories: " +   dataStatsRouteCalories.get(i) + "   \nMiles: " +  dataStatsRouteMiles.get(i) +
                    "   \nMileTimes: " +   dataStatsRouteMileTime.get(i) +
                    "   \nDates: " + dataStatsRouteDates.get(i) + "   \nStarts: " + dataStatsRouteStarts.get(i) + "   \nEnds: " + dataStatsRouteCalories.get(i));
        }


    }


    // Retrieve data from ROUTES_TABLE into a string
    public void retrieveData() {
        Cursor cursor;
        try {
            cursor = routeData.getAllData();
            // Check for data
            if (cursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();
            } else {
                while (cursor.moveToNext()) {
                    // Get data from index
                    dataRoutesId.add(cursor.getString(0));
                    dataRoutesList.add(cursor.getString(1));
                    dataRoutesDistances.add(cursor.getString(2));
                    dataRoutesDates.add(cursor.getString(3));
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "No Data Loaded", Toast.LENGTH_SHORT).show();
        }
    }

    public void retrieveStatsData() {
        Cursor statsCursor;
        try {
            statsCursor = routeStatsData.getAllData();
            // Check for data
            if (statsCursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();
                Log.i("NoDataIsLoaded", "No data");
            } else {
                while (statsCursor.moveToNext()) {
                    // Get data
                    dataStatsRoutePositions.add(Integer.parseInt(statsCursor.getString(1)));
                    dataStatsRouteTime.add(statsCursor.getString(2));
                    dataStatsRouteDistance.add(statsCursor.getString(3));
                    dataStatsRouteMiles.add(statsCursor.getString(4));
                    dataStatsRouteMileTime.add(statsCursor.getString(5));
                    dataStatsRouteDates.add(statsCursor.getString(6));
                    dataStatsRouteStarts.add(statsCursor.getString(7));
                    dataStatsRouteEnds.add(statsCursor.getString(8));
                    dataStatsRouteCalories.add(statsCursor.getString(9));
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "No Data Loaded", Toast.LENGTH_SHORT).show();
        }
    }
}
