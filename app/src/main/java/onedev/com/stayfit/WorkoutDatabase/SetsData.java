package onedev.com.stayfit.WorkoutDatabase;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;

public class SetsData extends SQLiteOpenHelper {

    //Database name for the entire workout portion of the application
    public static final String DATABASE_NAME = "FitnessDatabase.db";

    //Sets Table--------------------------------AND CONTENTS------Will hold the foreign id linked to the Exercise Table
    public static final String SETS_TABLE = "sets_table";
    public static final String COL_1ID = "ID";
    public static final String COL_2EXERCISEID = "NAchrME";
    public static final String COL_3REPS = "DATE";
    public static final String COL_4WEIGHT = "WEIGHT";

    //REFERENCE TO THE EXERCISEDATA CLASS
    ExerciseData ExData;
    /////------- ExData.EXERCISE_TABLE


    public SetsData(Context context) {
        super(context, SETS_TABLE, null, 1);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub


        //CReate the table
       //db.execSQL("create table " + SETS_TABLE + " (" + COL_1ID + " INTEGER PRIMARY KEY, " + COL_2EXERCISEID + " TEXT, " + COL_3REPS + " INTEGER, " + COL_4WEIGHT + " INTEGER)");
         db.execSQL("create table " + SETS_TABLE + " ("
                 + COL_1ID + " INTEGER PRIMARY KEY, " + COL_2EXERCISEID + " INTEGER, "
                 + COL_3REPS + " INTEGER, " + COL_4WEIGHT + " INTEGER)");

    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        //If table exists, drop
        db.execSQL("DROP TABLE IF EXISTS " + SETS_TABLE);
        //Pass this db instance
        onCreate(db);


    }

    //Insert data
    public boolean insertData(Integer exerciseId, Integer reps, Integer weight) {
        //Create database to write
        SQLiteDatabase db = this.getWritableDatabase();

        //To hold the content
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2EXERCISEID, exerciseId);
        contentValues.put(COL_3REPS, reps);
        contentValues.put(COL_4WEIGHT, weight);


        //Insert data into result to check for existence
        long result = db.insert(SETS_TABLE, null, contentValues);
        return result != -1;
    }

    //Get data
    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + SETS_TABLE, null);
        return cursor;
    }

    //Update data
    public boolean updateData(String id, String name, Integer reps, Integer weight) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_1ID, id);
        contentValues.put(COL_2EXERCISEID, name);
        contentValues.put(COL_3REPS, reps);
        contentValues.put(COL_4WEIGHT, weight);

        //Update data on the basis of id
       // db.update(SETS_TABLE, contentValues, "ID = ?", new String[]{id});
        db.update(SETS_TABLE, contentValues, COL_1ID + "=" + id, null);

        return true;
    }

    //Delete specific data
    public Integer deleteData(String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(SETS_TABLE, "ID = ?", new String[]{id});
    }


    public void deleteTable() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("delete from " + SETS_TABLE);

    }

}
