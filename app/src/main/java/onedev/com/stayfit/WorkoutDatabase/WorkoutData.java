package onedev.com.stayfit.WorkoutDatabase;

import android.database.sqlite.SQLiteOpenHelper;




/* FINISH THE CLASS.   -----  
 * Will hold the workouts created on tab3.  An arraylist of workouts which will hold
 * exercise ID's.  --- Will reference the exercise ID's in ExerciseData.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class WorkoutData extends SQLiteOpenHelper {
	
	//Database name for the entire workout portion of the application
	public static final String DATABASE_NAME = "FitnessDatabase.db";
	
	//Workout Table--------------------------------AND CONTENTS------
	public static final String WORKOUT_TABLE = "workout_table";
	public static final String COL_1ID = "ID";
	public static final String COL_2NAME = "NAME";
	public static final String COL_3EXERCISE = "EXERCISE";
	//public static final String COL_3DATE = "DATE";
	
	

	public WorkoutData(Context context) {
		super(context, WORKOUT_TABLE, null, 1);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		//CReate the table
        db.execSQL("create table " + WORKOUT_TABLE + " ("+COL_1ID+" INTEGER PRIMARY KEY, "+COL_2NAME+" TEXT, "+COL_3EXERCISE+" INTEGER)");

		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		//If table exists, drop
		db.execSQL("DROP TABLE IF EXISTS " + WORKOUT_TABLE);
		//Pass this db instance
		onCreate(db);
			
	}
	
	//Insert data
    public boolean insertData( String name, Integer exercise) {
        //Create database to write
        SQLiteDatabase db = this.getWritableDatabase();

        //To hold the content
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2NAME, name);
        contentValues.put(COL_3EXERCISE, exercise);
        //contentValues.put(COL_3DATE, date);
     
        
        //Insert data into result to check for existence
        long result = db.insert(WORKOUT_TABLE, null, contentValues);
        return result != -1;
    }
    
    //Get data
    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + WORKOUT_TABLE, null);
        return cursor;
    }

    //Update data
    public boolean updateData(String id, String name, Integer exercise){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_1ID, id);
        contentValues.put(COL_2NAME, name);
        contentValues.put(COL_3EXERCISE, exercise);

        //Update data on the basis of id
        db.update(WORKOUT_TABLE, contentValues, COL_1ID + "=" + id, null);
       // db.update(EXERCISE_TABLE, contentValues, COL_1ID + "=" + id, null);

        return true;
    }

    //Delete specific data
    public Integer deleteData (String id){
        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(WORKOUT_TABLE, "ID = ?", new String[] {id});

    }
    
    public void deleteTable(){
        SQLiteDatabase db = this.getWritableDatabase();
        
        db.execSQL("delete from "+ WORKOUT_TABLE);

    }
}