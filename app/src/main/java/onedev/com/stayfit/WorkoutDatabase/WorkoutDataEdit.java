package onedev.com.stayfit.WorkoutDatabase;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Juan on 12/8/2016.
 */

public class WorkoutDataEdit {
    Context context;
    /*---Tables---*/
    ExerciseData exerciseData;
    SetsData setsData;
    WorkoutData workoutData;
    CalendarData calendarData;
    CalendarColorData calendarColorData;

    /* -----------------------WorkoutTab4 elements */
    private String setsExcID = "";    //the exercise ID from SetsData
    private String excID = "";      //the exercise ID from ExerciseData

    ArrayList<String> exerciseIDList;
    ArrayList<Integer> setsExcIDList;
    ArrayList<Integer> setsIDList;
    /* -----------------------WorkoutTab3 elements */
    ArrayList<Integer> workoutIDlist;
    ArrayList<String> workoutNameList;
    ArrayList<String> workoutTAB3ExcList;
    String compiledExercises = "";

    /* -----------------------WorkoutTab2 elements */
    ArrayList<Integer> calendarDataIDList;
    ArrayList<String> dateList;
    ArrayList<Integer> dateWorkoutKeyList;


    /* -----------------------WorkoutTab1 elements */


    public WorkoutDataEdit(Context context) {
        //WorkoutTab4 --------------
        exerciseIDList = new ArrayList<>();
        setsExcIDList = new ArrayList<>();
        setsIDList = new ArrayList<>();
        workoutIDlist = new ArrayList<>();
        workoutTAB3ExcList = new ArrayList<>();
        workoutNameList = new ArrayList<>();
        calendarDataIDList = new ArrayList<>();
        dateList = new ArrayList<>();
        dateWorkoutKeyList = new ArrayList<>();


        this.context = context;

    }

    //TEsting data method for all tabs
    public void tabsSettings(int position) {
        //Workout tab 4

        //Workout tab 3
        workoutData = new WorkoutData(context);
        getWorkoutData();
        for (int i = 0; i < workoutTAB3ExcList.size(); i++) {
            Log.i("TestingCheckWT3Data", "CHecking for data in tab3: " + workoutTAB3ExcList.get(i));
        }


        //WORKOUT TAB 2

        //WOKROUT TAB 1

    }


    /* -----------------------------WorkoutTab4 Methods--------------------- PRIORITY 1*/
    public void deleteExercise(int exercisePosition) {
        //Get the exercise ID then use it to find the data to delete in SetsData
        //Get the exercise ID then use it to find the data to delete in SetsData
        //WORKOUT TAB 4===============================================
        exerciseData = new ExerciseData(context);
        setsData = new SetsData(context);
        getExerciseData();
        //WORKOUT TAB 3===============================================
        workoutData = new WorkoutData(context);
        getWorkoutData();
        //WORKOUT TAB 2==============================================
        //WORKOUT TAB 1==============================================

        /////------------------------------------DELETE DATA FROM SETS TABLE THAT CORRESPONDS TO THE EXERCISE-----------------------
        int setsId = 0; //Holds the foreign key from SETS TABLE
        String deleteKey = "";  //Holds the exercise to delete
        int intID = Integer.parseInt(exerciseIDList.get(exercisePosition));  //Holds the exercise id from EXERCISE TABLE
        //Get the id in sets table that has the exercise id then delete every instance of it in Sets Table
        setsId = setsExcIDList.indexOf(intID);
        for (int i = 0; i < setsExcIDList.size(); i++) {
            if (setsExcIDList.get(i) == setsId) {
                deleteKey = String.valueOf(setsIDList.get(i));
                setsData.deleteData(deleteKey);
            }
        }
        ////---------------------------------------DELETE THE EXERCISE FROM THE EXERCISE TABLE -----------------------
        String excDeleteKey = "";
        excDeleteKey = exerciseIDList.get(exercisePosition);
        exerciseData.deleteData(excDeleteKey);

        /////////------------------------------------UPDATE THE DATA FROM WORKOUT TAB 3 ----------------
        //CHECK FOR THE EXISTENCE OF THIS WORKOUT IN TAB 3
        //UPDATE THE WORKOUT IF IT EXISTS.
        //Decrement the values by 1 since the saving procedure incremented their values
        String excWorkoutId = "" + (exercisePosition + 1);   //The workout id will be the index +1, since its saved this way in the workouts table
        Log.i("ExerciseWokroutsID", excWorkoutId + " is the exercise id for the selected workout");

        if (!(workoutTAB3ExcList == null || workoutTAB3ExcList.isEmpty() || workoutTAB3ExcList.size() == 0)) {
            ArrayList<String> excUpdateValList = new ArrayList<>();
            ArrayList<String> newUpdatedList = new ArrayList<>();
            for (int x = 0; x < workoutTAB3ExcList.size(); x++) {
                String exValues = "";
                exValues = workoutTAB3ExcList.get(x) + "";
                Log.i("IntExcValues", exValues + "");
                char[] excCharArray = exValues.toCharArray();
                //Add excCharArray to arraylist
                for (char c : excCharArray) {
                    excUpdateValList.add(c + "");
                }
                //Add the non deleted values to a new list
                for (int i = 0; i < excUpdateValList.size(); i++) {
                    Log.i("Before", "" + excUpdateValList.get(i));
                    if (!(excUpdateValList.get(i).equals(excWorkoutId))) {
                        newUpdatedList.add(excUpdateValList.get(i));
                    }
                }
                //Update the workout with the deleted exercise
                //append the new values to a string to save into the DB
                String compiledExercises = "";
                for (int i = 0; i < newUpdatedList.size(); i++) {
                    //Add the exercise for the current workout to a string to save
                    compiledExercises = compiledExercises + newUpdatedList.get(i);
                }
                // String updatedCompiledExc = compiledExercises.replaceAll("\\s","");
                Log.i("NewCompiledString", "" + compiledExercises);

                //update the data in the workoutdata table with the new exercise values
                Log.i("DataToUpdated", (x + "    " + workoutNameList.get(x) + "     " + Integer.parseInt(compiledExercises)));
                workoutData.updateData((x + 1) + "", workoutNameList.get(x), Integer.parseInt(compiledExercises));

                for (int i = 0; i < newUpdatedList.size(); i++) {
                    Log.i("After", "" + newUpdatedList.get(i));
                }
                //clear the updateval list and loop
                newUpdatedList.clear();
                excUpdateValList.clear();
            }
        }
    }

    /* --------------------------------WorkoutTab3 Methods--==========------------------PRIORITY 2---*/
    public void deleteWorkouts(int workoutPosition) {
        int dataPosition = workoutPosition + 1;
        //WORKOUT TAB 3 ==============================================
        workoutData = new WorkoutData(context);
        getWorkoutData();

        //Delete workout position in data
        String workoutIDPos = String.valueOf(workoutIDlist.get(workoutPosition));
        workoutData.deleteData(workoutIDPos);
        // WORKOUT TAB 2 =============================================
        /*Check the existence of the workout in the calendar dates
         */
        calendarData = new CalendarData(context);
        getCalendarData();

        for (int i = 0; i < dateWorkoutKeyList.size(); i++) {
            if (dataPosition == dateWorkoutKeyList.get(i)){
                Log.i("DateWorkoutKey", dateWorkoutKeyList.get(i) + "");
                //Get the position of the workout to delete in calendar table
                int dataIndex = calendarDataIDList.get(i);
                Log.i("indexOfData", dataIndex+"");
                calendarData.deleteData(String.valueOf(dataIndex));
            }
        }

        for(int i = 0; i < dateWorkoutKeyList.size(); i++){
            Log.i("DateWorkoutKeyList", " "+ dateWorkoutKeyList.get(i));
        }
        // WORKOUT TAB 1 =============================================

        //=============================================================

    }

    public void editWorkout(int workoutPosition) {
        //WORKOUT TAB 3 ===============================================
        workoutData = new WorkoutData(context);
        getWorkoutData();
        // workoutData.


    }


    /* --------------------------------WorkoutTab2 Methods--==========------------------PRIORITY 3---*/
    public void deleteCalendarDate(int datePosition) {
        // WORKOUT TAB 2 =============================================
        calendarData = new CalendarData(context);
        getCalendarData();

        Log.i("DatePositionSelected", datePosition + "");
        calendarData.deleteData("" + datePosition);


        // WORKOUT TAB 1 =============================================
    }


    ///////////----------------------------------------------////PULL DATA///////////////------------------
    public void getExerciseData() {
        Cursor excCursor;
        Cursor setsCursor;
        try {
            excCursor = exerciseData.getAllData();
            setsCursor = setsData.getAllData();

            if (excCursor.getCount() == 0) {
                //No data
            } else {
                while (excCursor.moveToNext()) {
                    exerciseIDList.add(excCursor.getString(0));
                }
            }

            if (setsCursor.getCount() == 0) {

            } else {
                while (setsCursor.moveToNext()) {
                    //Get the id and exercise foreign key
                    setsIDList.add(Integer.parseInt(setsCursor.getString(0)));
                    setsExcIDList.add(Integer.parseInt(setsCursor.getString(1)));
                }
            }

        } catch (Exception e) {
            // Log.i("NoData", 'No data loaded for exerciseData');
        }
    }

    public void getWorkoutData() {
        Cursor cursor = workoutData.getAllData();

        if (cursor.getCount() == 0) {
            //No data
        } else {
            while (cursor.moveToNext()) {
                //Retrieve the workout ids
                workoutIDlist.add(Integer.parseInt(cursor.getString(0)));
                //Retrieve the workout name
                workoutNameList.add(cursor.getString(1));
                //Retrieve the exercise lists for the corresponding  workouts
                workoutTAB3ExcList.add(cursor.getString(2));
            }
        }
    }

    // get calendar data--------------------------------
    public void getCalendarData() {
        Cursor cursor;
        try {
            cursor = calendarData.getAllData();
            // Check for data
            if (cursor.getCount() == 0) {
                // Toast.makeText(getActivity(), "NO DATA",
                // Toast.LENGTH_SHORT).show();
            } else {
                // Toast.makeText(getActivity(), "Data is present in
                // TAB3-Popup", Toast.LENGTH_SHORT).show();
                while (cursor.moveToNext()) {
                    // Get date keys
                    calendarDataIDList.add(Integer.parseInt(cursor.getString(0)));
                    // This arraylist captuers the dates
                    dateList.add(cursor.getString(1));
                    // This arraylist captures the workout keys
                    dateWorkoutKeyList.add(Integer.parseInt(cursor.getString(2)));
                    Log.i("TestingCalendarData",
                            "The date: " + cursor.getString(1) + "| The workout KEY: " + cursor.getString(2));
                }
            }
        } catch (Exception e) {
            // Toast.makeText(getActivity(), "No Data Loaded",
            // Toast.LENGTH_SHORT).show();
        }

    }


}
