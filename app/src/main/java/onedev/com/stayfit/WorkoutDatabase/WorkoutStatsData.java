package onedev.com.stayfit.WorkoutDatabase;

/**
 * Created by Juan on 12/27/2016.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;



public class WorkoutStatsData extends SQLiteOpenHelper {

    // Database name for the entire workout portion of the application
    public static final String DATABASE_NAME = "FitnessDatabase.db";

    // Exercise Table--------------------------------AND CONTENTS------
    public static final String WORKOUT_STATS_TABLE = "workout_stats_table";
    public static final String COL_1ID = "ID";
    public static final String COL_2DATE = "DATE";
    public static final String COL_3WORKOUT = "WORKOUT";
    //public static final String COL_3DATE = "DATE";

    public WorkoutStatsData(Context context) {
        super(context, WORKOUT_STATS_TABLE, null, 1);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        // CReate the table
        db.execSQL("create table " + WORKOUT_STATS_TABLE + " (" + COL_1ID + " INTEGER PRIMARY KEY, " + COL_2DATE + " TEXT, " + COL_3WORKOUT + " INTEGER)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        // If table exists, drop
        db.execSQL("DROP TABLE IF EXISTS " + WORKOUT_STATS_TABLE);
        // Pass this db instance
        onCreate(db);

    }

    // Insert data
    public boolean insertData(String date, Integer workout) {
        // Create database to write
        SQLiteDatabase db = this.getWritableDatabase();

        // To hold the content
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2DATE, date);
        contentValues.put(COL_3WORKOUT, workout);

        // Insert data into result to check for existence
        long result = db.insert(WORKOUT_STATS_TABLE, null, contentValues);
        return result != -1;
    }

    // Get data
    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + WORKOUT_STATS_TABLE, null);

        return cursor;
    }

    // Update data
    public boolean updateData(String id, String date, Integer sets) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_1ID, id);
        contentValues.put(COL_2DATE, date);
        contentValues.put(COL_3WORKOUT, sets);

        // Update data on the basis of id
        db.update(WORKOUT_STATS_TABLE, contentValues, "ID = ?", new String[] { id });
        return true;
    }

    // Delete specific data
    public Integer deleteData(String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(WORKOUT_STATS_TABLE, "ID = ?", new String[] { id });
    }

    public void deleteTable() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("delete from " + WORKOUT_STATS_TABLE);

    }
}