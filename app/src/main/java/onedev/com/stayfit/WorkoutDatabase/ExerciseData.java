package onedev.com.stayfit.WorkoutDatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ExerciseData extends SQLiteOpenHelper {

	// Database name for the entire workout portion of the application
	public static final String DATABASE_NAME = "FitnessDatabase.db";

	// Exercise Table--------------------------------AND CONTENTS------
	public static final String EXERCISE_TABLE = "exercise_table";
	public static final String COL_1ID = "ID";
	public static final String COL_2NAME = "NAME";
	public static final String COL_3SETS = "SETS";
	//public static final String COL_3DATE = "DATE";

	public ExerciseData(Context context) {
		super(context, EXERCISE_TABLE, null, 1);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		// CReate the table
		db.execSQL("create table " + EXERCISE_TABLE + " (" + COL_1ID + " INTEGER PRIMARY KEY, " + COL_2NAME + " TEXT, " + COL_3SETS + " INTEGER)");

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		// If table exists, drop
		db.execSQL("DROP TABLE IF EXISTS " + EXERCISE_TABLE);
		// Pass this db instance
		onCreate(db);

	}

	// Insert data
	public boolean insertData(String name, Integer sets) {
		// Create database to write
		SQLiteDatabase db = this.getWritableDatabase();

		// To hold the content
		ContentValues contentValues = new ContentValues();
		contentValues.put(COL_2NAME, name);
		contentValues.put(COL_3SETS, sets);
		//contentValues.put(COL_3DATE, date);

		// Insert data into result to check for existence
		long result = db.insert(EXERCISE_TABLE, null, contentValues);
		return result != -1;
	}

	// Get data
	public Cursor getAllData() {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from " + EXERCISE_TABLE, null);
		
		return cursor;
	}

	// Update data
	public boolean updateData(String id, String name, Integer sets) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();

		contentValues.put(COL_1ID, id);
		contentValues.put(COL_2NAME, name);
		contentValues.put(COL_3SETS, sets);

		// Update data on the basis of id
		//db.update(EXERCISE_TABLE, contentValues, "ID = ?", new String[] { id });

		Log.i("TestingContent", "ID = " + id + "  NAME = "+ name + "  SETS = "+sets);

		db.update(EXERCISE_TABLE, contentValues, COL_1ID + "=" + id, null);


		return true;
	}

	// Delete specific data
	public Integer deleteData(String id) {
		SQLiteDatabase db = this.getWritableDatabase();

		return db.delete(EXERCISE_TABLE, "ID = ?", new String[] { id });
	}

	public void deleteTable() {
		SQLiteDatabase db = this.getWritableDatabase();

		db.execSQL("delete from " + EXERCISE_TABLE);

	}
}