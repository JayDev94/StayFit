package onedev.com.stayfit.WorkoutDatabase;

/**
 * Created by Juan on 8/11/2016.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;



public class CalendarColorData extends SQLiteOpenHelper {

    // Database name for the entire workout portion of the application
    public static final String DATABASE_NAME = "FitnessDatabase.db";

    // Exercise Table--------------------------------AND CONTENTS------
    public static final String CALENDAR_COLOR_TABLE = "calendar_color_table";
    public static final String COL_1ID = "ID";
    public static final String COL_2WEEK = "DATE";
    public static final String COL_4COLOR = "WORKOUT";
    //public static final String COL_3DATE = "DATE";

    public CalendarColorData(Context context) {
        super(context, CALENDAR_COLOR_TABLE, null, 1);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        // CReate the table
        db.execSQL("create table " + CALENDAR_COLOR_TABLE + " (" + COL_1ID + " INTEGER PRIMARY KEY, " + COL_2WEEK + " INTEGER, " + COL_4COLOR + " TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        // If table exists, drop
        db.execSQL("DROP TABLE IF EXISTS " + CALENDAR_COLOR_TABLE);
        // Pass this db instance
        onCreate(db);

    }

    // Insert data
    public boolean insertData(Integer week, String color) {
        // Create database to write
        SQLiteDatabase db = this.getWritableDatabase();

        // To hold the content
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2WEEK, week);
        contentValues.put(COL_4COLOR, color);

        // Insert data into result to check for existence
        long result = db.insert(CALENDAR_COLOR_TABLE, null, contentValues);
        return result != -1;
    }

    // Get data
    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + CALENDAR_COLOR_TABLE, null);
        return cursor;
    }

    // Update data
    public boolean updateData(String id,Integer week, String color) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_1ID, id);
        contentValues.put(COL_2WEEK, week);
        contentValues.put(COL_4COLOR, color);

        // Update data on the basis of id
        db.update(CALENDAR_COLOR_TABLE, contentValues, "ID = ?", new String[] { id });
        return true;
    }

    // Delete specific data
    public Integer deleteData(String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(CALENDAR_COLOR_TABLE, "ID = ?", new String[] { id });
    }

    public void deleteTable() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("delete from " + CALENDAR_COLOR_TABLE);

    }
}