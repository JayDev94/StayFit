package onedev.com.stayfit;

import java.util.Locale;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import onedev.com.stayfit.Workout.PreWorkoutTab2;
import onedev.com.stayfit.Workout.WorkoutTab1;
import onedev.com.stayfit.Workout.WorkoutTab2;
import onedev.com.stayfit.Workout.WorkoutTab3;
import onedev.com.stayfit.Workout.WorkoutTab4;

public class WorkoutFragment extends Fragment {

	public static final String TAG = WorkoutFragment.class.getSimpleName();
	SectionsPagerAdapter mSectionsPagerAdapter;
	ViewPager viewPager;

	public static WorkoutFragment newInstance() {
		return new WorkoutFragment();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.workout_creator, container, false);
		mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());

		viewPager = (ViewPager) v.findViewById(R.id.pager);
		viewPager.setAdapter(mSectionsPagerAdapter);

		return v;
	}

	public class SectionsPagerAdapter extends FragmentPagerAdapter {
		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}
		// Position of tabs  and Fragment invoking
		@Override
		public Fragment getItem(int position) {
			Fragment fragment = new TabbedContentFragment();
			Bundle args = new Bundle();
			args.putInt(TabbedContentFragment.ARG_SECTION_NUMBER, position + 1);

			switch (position) {
			case 0:
				fragment = new WorkoutTab1();
				break;
			case 1:
				fragment = new PreWorkoutTab2();
				break;
			case 2:
				fragment = new WorkoutTab3();
				break;
			case 3:
				fragment = new WorkoutTab4();
				break;
			}

			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			return 4;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			case 3:
				return getString(R.string.title_section4).toUpperCase(l);
			}
			return null;
		}
	}

	public static class TabbedContentFragment extends Fragment {

		public static final String ARG_SECTION_NUMBER = "section_number";

		public TabbedContentFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.tabbed_content, container, false);
			/*
			 * TextView dummyTextView = (TextView) rootView
			 * .findViewById(R.id.section_label);
			 * dummyTextView.setText(Integer.toString(getArguments().getInt(
			 * ARG_SECTION_NUMBER)));
			 */

			return rootView;
		}
	}

}