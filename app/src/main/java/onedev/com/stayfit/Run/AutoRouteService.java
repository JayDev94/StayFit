package onedev.com.stayfit.Run;

import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ListView;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.cast.CastRemoteDisplayLocalService.Callbacks;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import onedev.com.stayfit.MainActivity;
import onedev.com.stayfit.R;
import onedev.com.stayfit.RunningData.RouteData;
import onedev.com.stayfit.RunningData.RouteStatsData;

/**
 * Created by Juan on 1/17/2017.
 */

/*---------------------------------------------------------
    -----------------------------------------------------------
    Service class will handle all logic outside of the forground
    -----------------------------------------------------------
    -----------------------------------------------------------*
     */
public class AutoRouteService extends Service implements LocationSource.OnLocationChangedListener {

    Callbacks activityCallbacks;
    private final IBinder mBinder = new LocalBinder();
    Handler handler = new Handler();

    //Notification
    NotificationCompat.Builder mBuilder;
    NotificationManager mNotificationManager;

    //Test notification
    Notification notification;

    //The lists of latlangs and locations
    ArrayList<LatLng> latlongList;

    //Location change
    boolean booleanTrack;
    double previousLatitude;
    double previousLongitude;
    float[] results;
    float totalResults = 0;
    double roundedDist = 0;

    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */

    PolylineOptions userPolyOptions;
    ArrayList<Polyline> manualLineList;
    ArrayList<Polyline> userLineList;
    Polyline userLocationLine;
    int polylineCount = 0;
    int userLineCount = 0;

    //Location request --TESTING-_
    LocationManager locationManager;

    //Timer vars
    long startTime = 0L;
    long timeMilli = 0L;
    long timeBuffer = 0L;
    long timeUpdate = 0L;
    private Handler timeHandler = new Handler();
    String strTime = "";
    String sec = "";
    String mili = "";

    //Calorie vars
    int age = 21;
    int weight = 79;
    String strSeconds = "";
    String strMinutes = "";
    double myCals = 0;

    boolean pause;


    //Distance counter for each miles completed and miles string holder
    double dblCompletedMile = 0;
    String strMiles = " ";
    String strMilesTimes = " ";
    int mileCounter = 1;

    //Locaiton listener used to handle location updates and tracking calculations
    LocationListener locationListener;

    final String STOP_SERVICE = "STOP_AUTO_ROUTE_SERVICE";


    public AutoRouteService() {

    }


    @Override
    public IBinder onBind(Intent intent) {
        //Start the timer when the services is binded


        return mBinder;
    }

    //This returns an instance of AutoRouteService
    public class LocalBinder extends Binder {
        public AutoRouteService getServiceInstance() {
            return AutoRouteService.this;
        }
    }

    //MainActivity registers to this service as Callbacks client
    public void registerClient(Activity activity) {
        this.activityCallbacks = (Callbacks) activity;
    }


    /*
    On Service started ....Handle all route code here --- THEN transfer using receiver, into AutoRoute
    for GoogleMap utilization
    */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Initialize location manager with location services
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        //Location updates PolyOptions
        userPolyOptions = new PolylineOptions();
        //Tracker boolean initialization
        booleanTrack = false;
        //ArrayList init.
        latlongList = new ArrayList();
        manualLineList = new ArrayList<Polyline>();
        userLineList = new ArrayList<Polyline>();


        //Pending intents for notification actions
        Intent playIntent = new Intent();
        Intent pauseIntent = new Intent();
        playIntent.setAction("PLAY_ACTION");
        final PendingIntent pendingPlayIntent = PendingIntent.getBroadcast(getApplicationContext(),
                12345, playIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        pauseIntent.setAction("PAUSE_ACTION");
        final PendingIntent pendingPauseIntent = PendingIntent.getBroadcast(getApplicationContext(),
                12345, pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        //Retreive location updates
        locationUpdate();
        //
        // startTimer();


        return super.onStartCommand(intent, flags, startId);
    }

    //Custom notification
    public void customNotification(String strText) {
        //Infalte custome_notification layout
        RemoteViews remoteViews = new RemoteViews(getPackageName(),
                R.layout.custom_notification);
        //Set notification title
        //Build notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(android.R.drawable.ic_media_play)
                .setTicker("TestTicker")
                .setAutoCancel(true)
                .setContent(remoteViews)
                .setUsesChronometer(true);

        remoteViews.setTextViewText(R.id.title, "Something");
        remoteViews.setTextViewText(R.id.text, strText);
        ///////////////////////YOU ARE HERE................CONTINUE WORKING ON CUSTOM NOTIFICATION,  ADD ACTION PENDING INTENTS
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, builder.build());
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    /*
        * -------------------------------------------------------------------Location Listener
         */
    /*Location updates should be limited to a radius of 10 meters minimum (accuracy)*/
    public void locationUpdate() {
        locationListener = new LocationListener() {
            //Updating location
            @Override
            public void onLocationChanged(Location location) {
                //Toast.makeText(getApplicationContext(), "-TESTING-" + location.getLatitude() + ", " + location.getLongitude(), Toast.LENGTH_SHORT).show();
                Log.i("NewLocationRequest", "LatLang: " + location.getLatitude() + ", " + location.getLongitude() + " Accuracy: " + location.getAccuracy());

                if (location == null) {
                    // Blank for a moment..
                    Log.i("LocationNull.", "NOT WORKING");
                } else {
                    if (location.getAccuracy() > 1000) {
                        //  Log.i("BadAccuracy", "The accuracy is is low");
                        Toast.makeText(getApplicationContext(), "Bad Accuracy " + location.getAccuracy(), Toast.LENGTH_SHORT).show();
                        Log.i("BadAccuracy", "bad accuracy");
                    } else {
                        if (booleanTrack == false) {
                            Log.i("LocationOFF", "booelanTrack is false");
                        } else if (booleanTrack == true) {

                            Log.i("LocationStatus", "WORKING" + "Lat and Long: " + location.getLatitude() + ", " + location.getLongitude());
                            /**Get the locations lat and long
                             *strLong = location.getLong itude() + "";
                             *strLat = location.getLatitude() + "";
                             *dblLong = Double.parseDouble(strLong);
                             dblLat = Double.parseDouble(strLat);
                             Round the values to the nearest 100,000th for a more efficient reading of the arraylist
                             * This will also reduce the amount of data that will be loaded into the array.
                             * The camera movement will be more stable as a result.
                             *double lg = Math.round(dblLong * 10000.0) / 10000.0;
                             *double lt = Math.round(dblLat * 10000.0) / 10000.0;
                             *LatLng updatedLocation = new LatLng(lt, lg);
                             *Add the updated location with the LatLang to the array list
                             *latlongList.add(updatedLocation);
                             */

                            //Calculate the distance
                            //Add the first point the the list
                            LatLng LL = new LatLng(location.getLatitude(), location.getLongitude());
                            latlongList.add(LL);


                            //Caclulate the distance
                            if (!(latlongList.isEmpty())) {
                                if (latlongList.size() < 2) {
                                    //The arraylist only holds one value, use that for the first value
                                    previousLatitude = latlongList.get(0).latitude;
                                    previousLongitude = latlongList.get(0).longitude;
                                } else {
                                    for (int i = 0; i < latlongList.size(); ++i) {
                                        //Initialize the results float to retrieve the distance between points
                                        results = new float[1];

                                        //Calculate the distance between the previous point and the current
                                        Location.distanceBetween(previousLatitude, previousLongitude, location.getLatitude(), location.getLongitude(), results);
                                        //Update the total distance
                                        totalResults = totalResults + results[0];
                                        // Modify the previous ones with the ones just used
                                        previousLatitude = location.getLatitude();
                                        previousLongitude = location.getLongitude();

                                        //set the TextView with the total distance
                                        //After conversion to miles
                                        double dist = 0;

                                        dist = (double) totalResults * 0.00062137;
                                        roundedDist = Math.round(dist * 100.0) / 100.0;
                                        //set the textview with the total distance


                                        //Everytime a mile is completed the distance is added to mileslist and the counter is reset
                                        dblCompletedMile = roundedDist;
                                        if (dblCompletedMile > mileCounter) {
                                            //String will hold all the miles seperated by " "
                                            strMiles = strMiles + " " + dblCompletedMile + " ";
                                            dblCompletedMile = 0;

                                            //Stamp the current time
                                            strMilesTimes = " " + strMilesTimes + " " + strTime + " ";

                                            Log.i("StringMilesTimesTest", strMiles);
                                            //Increment the counter for the next mile
                                            mileCounter++;
                                        }


                                        /////////THIS IS CORRUPT
                                        ///This essentially need to be able the get the remainder of the last mile
                                        /*double value = 0;
                                        if (dblCompletedMile < 1) {
                                            value = dblCompletedMile;
                                            //strMiles =  strMiles + value + "";
                                        } else {
                                            value = dblCompletedMile - (mileCounter - 1);
                                            value = 1 - value;
                                            value = Math.round(value * 100.00) / 100.00;
                                            strMiles = strMiles + value + " ";

                                        }
                                        */


                                        Log.i("MilesAndTimes", "Miles: " + strMiles + " \n Times: " + strMilesTimes);


                                        //Calorie Settings (TIME CONVERSTION TO MINUTES)
                                        double timeCals = 0;
                                        double dblSec = 0;
                                        double dblMin = 0;
                                        dblSec = Double.parseDouble(strSeconds);
                                        dblMin = Double.parseDouble(strMinutes);
                                        timeCals = ((dblMin * 60) + dblSec);
                                        Log.i("totalTimeCals", "" + timeCals);


                                        myCals = calculateCalories(timeCals, totalResults);
                                        myCals = Math.round(100.0 * myCals) / 100.0;
                                        Log.i("TestingClaories", "Calories: " + myCals);


                                        //---------------------------------------[Broadcast route physical information]
                                        /*List item 0 = roundedDist
                                               item 1 = calories
                                               item 2 = miles
                                               item 3 = mileTimes
                                         */
                                        //////////TESST;; get strmiles string
                                        Log.i("StrMilesString", strMiles + "");


                                        Intent routeDataBroadcast = new Intent();
                                        routeDataBroadcast.setAction("RouteInfoBroadcast");
                                        Bundle args1 = new Bundle();
                                        ArrayList<String> routeDataList = new ArrayList<>();
                                        routeDataList.add(roundedDist + "");
                                        routeDataList.add(myCals + "");
                                        routeDataList.add(strMiles);
                                        routeDataList.add(strMilesTimes);

                                        args1.putStringArrayList("RouteInfoArg", routeDataList);
                                        routeDataBroadcast.putExtra("RouteInfoBundle", args1);


                                        sendBroadcast(routeDataBroadcast);
                                    }
                                }
                            }

                            //Update AutoRoute data
                            ////////===================TESTING BROADCAST COMMUNICATION WITH ACTIVITY/FRAGMENT(AUTOROUTE)====== "IT WORKED"
                            ////Test has been successfull, callback method will be ignored and broadcast communication will be used
                                        /*Broadcast communication using a bundle is used (LatLng is stored) */
                            Intent routeLatLngBroadcast = new Intent();
                            routeLatLngBroadcast.setAction("RouteLatLngBroadcast");
                            Bundle args2 = new Bundle();
                            args2.putParcelableArrayList("latlng", latlongList);
                            routeLatLngBroadcast.putExtra("RouteLatLngBroadcast", args2);
                            sendBroadcast(routeLatLngBroadcast);

                            /* //Create the polyline from latlongList arraylist and add it to manualLine for edit
                            Log.i("TestingDistance", "Distance: " + totalResults);
                            userPolyOptions = new PolylineOptions();
                            userPolyOptions.addAll(latlongList);
                            userPolyOptions.color(Color.GREEN);
                            userLocationLine = mMap.addPolyline(userPolyOptions);
                            //Add the line to the polylinelist
                            userLineList.add(userLocationLine);
                            userLineCount++; */
                        } else {
                            Toast.makeText(getApplicationContext(), "Tracker OFF", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }

        };
    }


    ///------------------------------------------------------------------ COMPLETE ROUTE ---
    public void completeRoute() {
        Log.i("RecievedBroad", "hello world");
        //Add the remaining distance to the miles and milesTimes string
        //Get the remainder by subtracting the whole number then overwrite that value by 1-value then round to .00
        //If it is less than 1, use the current distance ('which dblCompletedMile has')


        ////THIS OBTAINS THE REMAINDER OF THE LAST MILE COUNT BEING < 1 MILE AS WELL AS THE REMAINDER MILE TIME
        double value = 0;
        if (dblCompletedMile < 1) {
            value = dblCompletedMile;
            strMiles = strMiles + value + " ";
            strMilesTimes = strTime + " ";
        }


        //strMilesTimes = strMilesTimes + strTime;
        Log.i("ChekingMilesAndtimes", "MilesList: " + strMiles + "   \n TimesList: " + strMilesTimes);

        //Save data
        String strRoute = "";
        for (int i = 0; i < latlongList.size(); i++) {
            strRoute = strRoute + " " + latlongList.get(i).latitude + "," + latlongList.get(i).longitude + " ";
        }
        //JUST FOR TESTING PURPOSES------------------Add blank info if distance < 1
        //if(roundedDist < 1){
        //  strRoute = " 41.8781,-87.6298 " ;
        //}
        //--------------------------------------------

        String totalDist = " " + roundedDist + " ";
                                /*
                                routeData.insertData(strRoute, totalDist);
                                int routeDataInput = routeDataList.size();
                                //Save the miles, and mile times as well
                               routeStatsData.insertData(routeDataInput, strTime, totalDist, strMiles, strMilesTimes);
                                */
        DateFormat formatter = SimpleDateFormat.getDateInstance();
        Date saveDate = new Date();
        //String strDate = formatter.format(saveDate);
        String strDate = String.valueOf(System.currentTimeMillis());

        //Start time, End time, Calories burned//
        String startTime = "", endTime = "";
        // startTime = strStartTime;
        //endTime = strEndTime;
        Log.i("DataSentToStats", "   \nstrRoute: " + strRoute + "   \nstrTime: " + strTime +
                "   \ntotalDist: " + totalDist + "   \nmyCals: " + myCals + "   \nstrMiles: " + strMiles + "   \nstrMileTimes: " + strMilesTimes +
                "   \nstrDate: " + strDate + "   \nstartTime: " + startTime + "   \nendTime: " + endTime);

        Intent routeCompletionBroadcast = new Intent();
        routeCompletionBroadcast.setAction("RouteCompletionBroadcast");
        Bundle args1 = new Bundle();
        ArrayList<String> routeDataList = new ArrayList<>();
        routeDataList.add(strRoute + "");
        routeDataList.add(strTime + "");
        routeDataList.add(totalDist);
        routeDataList.add(myCals + "");
        routeDataList.add(strMiles);
        routeDataList.add(strMilesTimes);     ///CAHNGED FROM strMilesTimes
        routeDataList.add(strDate);
        args1.putStringArrayList("RouteCompletionList", routeDataList);
        routeCompletionBroadcast.putExtra("RouteCompletionBundle", args1);


        latlongList.clear();


        sendBroadcast(routeCompletionBroadcast);

    }


    /**
     * ------------------------------------------------------------------- Requests location updates
     */

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        Toast.makeText(getApplicationContext().getApplicationContext(), "Starting Location Tracker", Toast.LENGTH_SHORT).show();
        //--TESTING-- LOCATION REQUESTS    time, meters

        /*Final setup should include both network and gps location listeners*/
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 3, locationListener);


        // locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        //Original location requestion
        /*LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);/*/
    }

    /**
     * ------------------------------------------------------------------Stop location updates
     */
    public void stopLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            // return;
        }
        Toast.makeText(getApplicationContext(), "Location Tracker OFF", Toast.LENGTH_SHORT).show();
        locationManager.removeUpdates(locationListener);
    }

    //------------------------------------------------------------Reset the tracker vars and UI elements
    public void resetTracker() {
        //set the distance text to empty string
        //distanceText.setText("");
        //delete the list of locations
        latlongList.clear();
        //stop the tracker
        booleanTrack = false;

        //remove polylines
        if (!(manualLineList.isEmpty())) {
            for (int i = 0; i < manualLineList.size(); i++) {
                manualLineList.get(i).remove();
            }
        }

        //Clear the polyline list
        manualLineList.clear();
        //Set the polyline count to 0
        polylineCount = 0;
        if (!(userLineList.isEmpty())) {
            for (int i = 0; i < userLineList.size(); i++) {
                userLineList.get(i).remove();
            }
        }
        //Clear the polyline list
        userLineList.clear();
        //Set the polyline count to 0
        userLineCount = 0;
        //Reset the total distance
        totalResults = 0;
        //Reset calories text
        //caloriesText.setText("Cals: ");
        //Show the start button
        //btnStart.setVisibility(View.VISIBLE);
        //Continue button shows next time
        //btnContinuePause.setText("Pause");
        //btnContinuePause.setTextColor(Color.parseColor("#ff5100"));
        pause = false;

    }


    /**
     * ----------------------------------Calorie calculations are done using the ACSM formula for running
     * ---[VO2 = (.2*metersMin) + 3.5]---
     * the kcals are calculated using this formula
     * ---[kcal/min = 5((VO2*BMkg)/1000]---
     */
    /*Calorie calculations need to incorporate a walking speed, Currently only calculating for > 5mph.
    *Function must switch dynamically when speed passes > or < 5mph.*/
    public double calculateCalories(double time, float distance) {
        double vO2 = 0;
        double metersMin = 0;
        double kCal = 0;
        double totalKCal = 0;
        double timeVar = 0;
        double kcalVar = 0;

        //Conversions
        timeVar = time / 60;
        metersMin = distance / timeVar;
        //VO2 calculations
        vO2 = (.2 * metersMin) + 3.5;
        //Kcal calculations
        kcalVar = (vO2 * weight) / 1000;
        kCal = 5 * kcalVar;
        totalKCal = timeVar * kCal;

        return totalKCal;
    }


    //----------------------------------------------------------Timer runnable
    private Runnable updateTimer = new Runnable() {
        @Override
        public void run() {
            timeMilli = SystemClock.uptimeMillis() - startTime;
            timeUpdate = timeBuffer + timeMilli;
            int seconds = (int) (timeUpdate / 1000);
            int minutes = seconds / 60;
            int hours = minutes / 60;
            seconds = seconds % 60;
            int milliseconds = (int) (timeUpdate % 1000);
            //Format time
            sec = String.format("%02d", seconds);
            mili = String.format("%02d", milliseconds);
            //Set strTime with the time to display
            strTime = hours + ":" + minutes + ":" + sec;

            strSeconds = sec;
            strMinutes = "" + minutes;


            customNotification(strSeconds);

            /*//Update notification
            mBuilder.setContentTitle(strSeconds);
            mNotificationManager.notify(1, mBuilder.build());*/


            //Send a broadcast of elapsed time
            Intent routeTimeBroadcast = new Intent();
            routeTimeBroadcast.setAction("RouteTimeBroadcast");
            Bundle args1 = new Bundle();
            String routeTime = "";
            routeTime = strTime;
            args1.putString("RouteTimeArg", routeTime);
            routeTimeBroadcast.putExtra("RouteTimeBundle", args1);
            sendBroadcast(routeTimeBroadcast);


            timeHandler.postDelayed(this, 0);
        }
    };

    //Start timer
    public void startTimer() {
        startTime = SystemClock.uptimeMillis();
        timeHandler.postDelayed(updateTimer, 0);
        booleanTrack = true;
    }

    //Pause timer
    public void pauseTimer() {
        timeBuffer += timeMilli;
        timeHandler.removeCallbacks(updateTimer);
    }

    //Reset timer
    public void resetTimer() {
        //Timer vars
        startTime = 0L;
        timeMilli = 0L;
        timeBuffer = 0L;
        timeUpdate = 0L;
        strTime = "";
        sec = "";
        mili = "";
    }

    //Destroy service here
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("ServiceIsDestroyed", "Service has been destroyed");
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    //interface for communication
    public interface Callbacks {
        public void updateAutoRouteTracksData(double distance, double calories, String strMiles,
                                              ArrayList<LatLng> latLongList);
    }

    public void unbindedAndStop() {
      /*  String BROADCAST_STOP_SERVICE = "StopRouteService";
        Intent routeCompletionBroadcast = new Intent();
        routeCompletionBroadcast.setAction(BROADCAST_STOP_SERVICE);
        sendBroadcast(routeCompletionBroadcast);*/
        stopSelf();

    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }
}