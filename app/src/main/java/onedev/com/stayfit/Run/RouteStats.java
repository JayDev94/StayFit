package onedev.com.stayfit.Run;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import onedev.com.stayfit.R;
import onedev.com.stayfit.RunningData.RouteData;
import onedev.com.stayfit.RunningData.RouteStatsData;

/**
 * Created by Juan on 7/14/2016.
 */
public class RouteStats extends Fragment implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener,
        GoogleMap.OnCameraChangeListener, GoogleMap.OnMyLocationButtonClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationSource.OnLocationChangedListener, GoogleMap.SnapshotReadyCallback {

    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    // Keys for storing activity state in the Bundle.
    protected final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting-location-updates-key";
    protected final static String LOCATION_KEY = "location-key";
    protected final static String LAST_UPDATED_TIME_STRING_KEY = "last-updated-time-string-key";
    /**
     * Request code for location permission request.
     *
     * @see #onRequestPermissionsResult(int, String[], int[])
     */
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    /**
     * Flag indicating whether a requested permission has been denied after returning in
     * {@link #onRequestPermissionsResult(int, String[], int[])}.
     */

    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    protected Boolean mRequestingLocationUpdates;

    //current location;
    Location myLocation;
    LatLng currentLocation;

    private GoogleMap mMap;

    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    public static final String TAG = MapsActivity.class.getSimpleName();

    //Map Fragment
    SupportMapFragment mapFragment;

    //MyTools
    MyTools myTools;

    //PolyOptions
    PolylineOptions userPolyOptions;
    ArrayList<Polyline> manualLineList;
    ArrayList<Polyline> userLineList;
    Polyline userLocationLine;
    int polylineCount = 0;
    int userLineCount = 0;

    //Route arraylists and distance
    ArrayList<String> routeStringList;
    ArrayList<String> routeDistanceList;
    ArrayList<LatLng> latLngArrayList;

    //Interpolation var
    //ArrayList holds the interpolated latlngs
    ArrayList<LatLng> interpolatLoadedArrayList;
    double counterDistance = 0;

    //This ArrayList will hold the stats data retrieved from a bundle (0Route-1Time-2Distance)
    ArrayList<String> statsList;
    String strRoute;
    String time;
    String distance;
    String calories;
    String miles;
    String mileTimes;
    String dateRan;
    String startTime;
    String endTime;
    ArrayList<String> allMilesList;
    ArrayList<String> mileTimesList;
    ArrayList<String> milesAndTimesList;
    ArrayList<Integer> routeTimesList;
    double dblDistance;

    //UI var
    TextView viewTime;
    TextView btnDistance;
    TextView viewCalories;
    TextView viewAvgPace;
    ListView lvMiles;

    Button btnDone;

    //Interface callback
    StatsFinishedInterface callBack;

    //List holds the miles and their corresponding times
    ArrayList<String> milesList;
    ArrayAdapter<String> milesAdapter;

    //Conversion to miles or km
    boolean measureConvert = true;


    RouteData routeData;
    RouteStatsData routeStatsData;
    ArrayList<String>routeDataList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        routeData = new RouteData(getActivity());
        routeStatsData = new RouteStatsData(getActivity());

        statsList = new ArrayList<>();
        allMilesList = new ArrayList<>();
        mileTimesList = new ArrayList<>();
        milesAndTimesList = new ArrayList<>();
        routeTimesList = new ArrayList<>();
        //Retrieve data from bundle
        statsList = getArguments().getStringArrayList("RouteTimeDistanceBundle");
        //Set with their corresponding statsList values
        strRoute = statsList.get(0);
        time = statsList.get(1);
        distance = statsList.get(2);
        calories = statsList.get(3);
        miles = statsList.get(4);
        mileTimes = statsList.get(5);
        dateRan = statsList.get(6);
        startTime = statsList.get(7);
        endTime= statsList.get(8);


        Log.i("DateRanSats", dateRan);


        //Split miles and times by space
        String[] splittedMiles = miles.trim().split("\\s+");
        String[] splittedTimes = mileTimes.trim().split("\\s+");
        //TESTING SPLITTED ARRAY
        for (int x = 0; x < splittedMiles.length; x++) {
            allMilesList.add(splittedMiles[x]);
        }
        for (int x = 0; x < splittedTimes.length; x++) {
            mileTimesList.add(splittedTimes[x]);
        }

        for (int i = 0; i < statsList.size(); i++) {
            Log.i("StatsArrayList", i + "  " + statsList.get(i));
        }

        return inflater.inflate(R.layout.route_stats_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        myTools = new MyTools();
        milesList = new ArrayList<>();

        lvMiles = (ListView) view.findViewById(R.id.listStatsMiles);
        viewTime = (TextView) view.findViewById(R.id.txtStatsTime);
        btnDistance = (TextView) view.findViewById(R.id.txtStatsDistance);
        viewCalories = (TextView) view.findViewById(R.id.txtStatsCalories);
        btnDone = (Button) view.findViewById(R.id.btnStatsDone);


        //ListView settings
        milesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, milesAndTimesList);
        lvMiles.setAdapter(milesAdapter);

        //Combine the contents from both lists to display on the listview in one list
        Log.i("SizeOfLists", allMilesList.size() + "miles  ------  times" + mileTimesList.size());

        //Testing allmileslist and miles list
        Log.i("MilesListsCheck", "\n " + allMilesList.size() + "\n" + milesList.size());

        /*ORIGGINAL ---------*/
        for (int i = 0; i < allMilesList.size(); i++) {
            milesAndTimesList.add(allMilesList.get(i) + " mi                      " + mileTimesList.get(i));
        }

        /*COPY -----------*/
        /*
        for (int i = 0; i < allMilesList.size(); i ++){
            if(i < milesAndTimesList.size()) {
                milesAndTimesList.add(allMilesList.get(i) + " mi ");
            }
        }
        */
        milesAdapter.notifyDataSetChanged();


        //Initialize ArrayLists
        latLngArrayList = new ArrayList<>();
        userLineList = new ArrayList<>();


        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.route_stats_map);
        mapFragment.getMapAsync(this);

        //Start gps
        mGoogleApiClient.connect();

        //Set the UI with the stats data
        viewTime.setText(time);

        //Set distance after conversion
        dblDistance = Double.parseDouble(distance);
        MyTools myTools = new MyTools();
        dblDistance = myTools.convertStringToMiles(distance);
        btnDistance.setText(distance + " mi");
        //Set calories
        viewCalories.setText(calories);

        //Convert strint to list ---Calculations are loaded in onMapReady
        convertStringToLatLong(strRoute);

        //On btnDistance clicked, convert measure
        btnDistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                measureConvert = measureConvert != true;
                milesOrKilometers();
            }
        });


        //Distance list and Retrieve data
        routeDistanceList = new ArrayList<>();
        routeDataList = new ArrayList<>();

        retrieveData();
        //On btnDone button clicked
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Take a screenshot of the map
                //Save a snapshot of the route
                GoogleMap.SnapshotReadyCallback callbackSnap = new GoogleMap.SnapshotReadyCallback() {
                    Bitmap bitmap;

                    @Override
                    public void onSnapshotReady(Bitmap snapshot) {
                        bitmap = snapshot;
                        try {
                            //String filePath = Environment.DIRECTORY_DCIM;
                            //File dir=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                            //File file = new File(dir, "mapTestImage1.png");
                            //file.createNewFile();

                            File myDirectory = new File(Environment.getExternalStorageDirectory(), "StayFitSnapshots");

                            if(!myDirectory.exists()) {
                                myDirectory.mkdirs();
                            }

                            String trimmedName = dateRan +"";
                            trimmedName = trimmedName.replace(" ", "");

                            String mPath = myDirectory+ "/" +trimmedName+ ".png";

                            Log.i("mpath", mPath);
                            File myFile = new File(mPath);


                            //Save image to internal storage
                            FileOutputStream out = new FileOutputStream(myFile);



                            //Scale down the bitmap size before compressing and saving
                            Bitmap scaledBitmap = Bitmap.createScaledBitmap(snapshot, 250, 250, false);
                            scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                            out.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.i("SnapshotError", "Cannot save snapshot");
                        }
                    }
                };
                mMap.snapshot(callbackSnap);

                     /* totalDist = " " + roundedDist + " ";
                                routeData.insertData(strRoute, totalDist);
                                int routeDataInput = routeDataList.size();
                                //Save the miles, and mile times as well
                               routeStatsData.insertData(routeDataInput, strTime, totalDist, strMiles, strMilesTimes);
                                */




                Log.i("RouteDataToInsert", "ROUTE: " + strRoute + "\n DISTANCE: " + distance + "\n DATERAN: " + dateRan);
                //Save the data into RouteData and RouteStats
                routeData.insertData(strRoute, distance, dateRan);
                retrieveData();


                Log.i("RoutePositionsList", routeDataList.size()+"");

                Log.i("StatsToBeSaved", "\nRoutePosition" + routeDataList.size() + "\nstrTime: " + time+
                        "   \ndistance: " +distance+  "   \nmiles: " + miles + "   \nMileTimes: " +  mileTimes +
                        "   \nDate: " +dateRan + "   \nStart: " +startTime + "   \nendTime: " + endTime + "\nCalories" + calories);

                routeStatsData.insertData(routeDataList.size(), time, distance, miles, mileTimes, dateRan, startTime, endTime, calories);



                //Listener  Finishes Stats Fragment
                callBack.onStatsComplete();
            }
        });

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapClickListener(this);
        mMap.setOnMapLongClickListener(this);
        mMap.setOnCameraChangeListener(this);
        mMap.setOnMyLocationButtonClickListener(this);

        //Connect google API
        mGoogleApiClient.connect();
        //Load calculations for polyline
        calculationsLoaded();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onSnapshotReady(Bitmap bitmap) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception

        try {
            callBack = (StatsFinishedInterface) context;

        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement SelectRouteInterfaceListener");

        }
    }


    public interface StatsFinishedInterface {
        void onStatsComplete();

    }

    public void calculationsLoaded() {
        //Will holds the camulative distances until it equals 10 then will reset
        double distanceCounter = 0;
        //Make sure the map is loaded and ready
        if (mMap == null) {

        } else {
            //Create the new loaded array list line
            userPolyOptions = new PolylineOptions();
            userPolyOptions.addAll(latLngArrayList);
            userPolyOptions.color(Color.BLUE);
            userLocationLine = mMap.addPolyline(userPolyOptions);
            //Add the line to the polylinelist
            userLineList.add(userLocationLine);
            userLineCount++;

            //Set the camera at the starting point
            //Find the center point
            int center = statsList.size() / 2;

            //if the LatLngArrayList is empty, do not copy the data
            // if(!(latLngArrayList))
            LatLng myCoordinates = new LatLng(latLngArrayList.get(center).latitude, latLngArrayList.get(center).longitude);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(myCoordinates, 15);
            mMap.animateCamera(cameraUpdate);
        }
    }


    //String to latlang conversions
    public void convertStringToLatLong(String routeString) {

        String strSp = routeString;
        //Split by space
        String[] splited = strSp.split("\\s+");
        //TESTING SPLITTED ARRAY
        for (int x = 0; x < splited.length; x++) {
            Log.i("SplitedArray", splited[x]);
        }
        //TESTING SPLITTED ARRAY BY COMMA
        String strTest1 = splited[1];
        String[] strTest = strTest1.split(",");
        String show = strTest[0];
        Log.i("SplittComma", " SPLIT" + show);

        // List<String> splitedList = new ArrayList<String>();
        //Clear the latlonglist and drawing lists for use
        latLngArrayList.clear();
        //manualLineList.clear();
        for (int i = 1; i < splited.length; i++) {
            String[] strLatLangs = splited[i].split(",");
            Double lat = Double.parseDouble(strLatLangs[0]);
            Double lo = Double.parseDouble(strLatLangs[1]);
            LatLng location = new LatLng(lat, lo);
            latLngArrayList.add(location);
        }
        for (int t = 0; t < latLngArrayList.size(); t++) {
            Log.i("LatLngConverted", latLngArrayList.get(t).latitude + ", " + latLngArrayList.get(t).longitude);
        }
    }

    //Miles to Kilometers conversion
    public void milesOrKilometers() {
        //Miles to km
        if (!(measureConvert == true)) {
            for (int i = 0; i < allMilesList.size(); i++) {
                double newVal = 0;
                newVal = myTools.convertMiToKm(Double.parseDouble(allMilesList.get(i)));
                //Replace current val with km newVal
                allMilesList.set(i, newVal + "");
            }
            milesAndTimesList.clear();
            for (int i = 0; i < allMilesList.size(); i++) {
                milesAndTimesList.add(allMilesList.get(i) + " km                      " + mileTimesList.get(i));
            }
            //Change the distance lable to the corresponding measure
            double dblKm = 1.60934 * Double.parseDouble(distance);
            dblKm = Math.round(dblKm * 100.0) / 100.0;
            btnDistance.setText(dblKm + " km");
            milesAdapter.notifyDataSetChanged();
        } else {
            //km to miles
            for (int i = 0; i < allMilesList.size(); i++) {
                double newVal = 0;
                newVal = myTools.convertKmToMi(Double.parseDouble(allMilesList.get(i)));
                //Replace current val with miles newVal
                allMilesList.set(i, newVal + "");
            }
            milesAndTimesList.clear();
            for (int i = 0; i < allMilesList.size(); i++) {
                milesAndTimesList.add(allMilesList.get(i) + " mi                      " + mileTimesList.get(i));
            }
            //Change the distance lable to the corresponding measure
            double dblMi = 0.621371 * Double.parseDouble(distance);
            dblMi = Math.round(dblMi * 100.0) / 100.0;
            btnDistance.setText(dblMi + " mi");
            milesAdapter.notifyDataSetChanged();

        }
    }


    //Data base retrieval
    // Retrieve data from ROUTES_TABLE into a string
    public void retrieveData() {
        Cursor cursor;
        try {
            cursor = routeData.getAllData();
            // Check for data
            if (cursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();
            } else {
                while (cursor.moveToNext()) {
                    // Get data from index
                    //Add distances to a list
                    routeDataList.add(cursor.getString(1));
                    Log.i("RouteDataListContent", routeDataList.size()+"");
                    routeDistanceList.add(cursor.getString(2));
                    routeTimesList.add(Integer.parseInt(cursor.getString(3)));
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "No Data Loaded", Toast.LENGTH_SHORT).show();
        }
    }
}

