package onedev.com.stayfit.Run;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Juan on 2/20/2017.
 */

public class StopAutoRouteServiceReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        context.stopService(intent);
    }
}
