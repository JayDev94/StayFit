package onedev.com.stayfit.Run;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.maps.android.SphericalUtil;


import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;

import onedev.com.stayfit.R;
import onedev.com.stayfit.RunningData.RouteData;
import onedev.com.stayfit.RunningData.RouteStatsData;

/**
 * Created by Juan on 6/13/2016.
 */
public class RunRoute extends Fragment implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener,
        GoogleMap.OnCameraChangeListener, GoogleMap.OnMyLocationButtonClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationSource.OnLocationChangedListener, SelectRoute.SelectRouteInterfaceListener {
    //THE LOCATION ACCURACY
    int locationAccuracy = 1000;


    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    // Keys for storing activity state in the Bundle.
    protected final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting-location-updates-key";
    protected final static String LOCATION_KEY = "location-key";
    protected final static String LAST_UPDATED_TIME_STRING_KEY = "last-updated-time-string-key";
    /**
     * Request code for location permission request.
     *
     * @see #onRequestPermissionsResult(int, String[], int[])
     */
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    /**
     * Flag indicating whether a requested permission has been denied after returning in
     * {@link #onRequestPermissionsResult(int, String[], int[])}.
     */

    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    protected Boolean mRequestingLocationUpdates;

    //current location;
    Location myLocation;
    LatLng currentLocation;

    //Googlemap api var
    private GoogleMap rMap;

    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    public static final String TAG = MapsActivity.class.getSimpleName();
    //Map frage
    SupportMapFragment mapFragment;
    //Location request --TESTING-_
    LocationManager locationManager;

    //Bundle received from MapActivity
    int routePosition = 0;

    //Route Data
    RouteData routeData;

    //Route arraylists and distance
    ArrayList<String> routeStringList;
    ArrayList<String> routeDistanceList;
    ArrayList<LatLng> latLngArrayList;

    //Location list and settings var (LOADED ROUTE)
    ArrayList<LatLng> latlongList;
    double previousLatitude;
    double previousLongitude;
    float[] results;
    float totalResults = 0;
    PolylineOptions runRoutePolyoptions;
    Polyline runRouteLine;

    //Location list and settings (USER'S ROUTE)
    float[] userResults;
    float totalUserResults = 0;
    double previousLatUser;
    double previousLngUser;

    //ui
    TextView distanceText;
    TextView caloriesText;
    TextView timeText;
    TextView paceText;
    Button btnStart;
    Button btnPause;
    Button btnReset;

    //PolyOptions
    PolylineOptions userPolyOptions;
    ArrayList<Polyline> manualLineList;
    ArrayList<Polyline> userLineList;
    Polyline userLocationLine;
    int polylineCount = 0;
    int userLineCount = 0;

    //Calorie calculations var
    int age = 21;
    int weight = 79;
    double calMinutes = 0.0;
    String strSeconds = "";
    String strMinutes = "";
    //Time var
    //Timer vars
    long startTime = 0L;
    long timeMilli = 0L;
    long timeBuffer = 0L;
    long timeUpdate = 0L;
    private Handler timeHandler = new Handler();
    String strTime = "";
    String sec = "";
    String mili = "";
    TextView textMapTime;


    //Tracker boolean
    boolean booleanTrack = false;


    /////TESTING::::TESTING:::MANUAL OPTIONS
    ArrayList<LatLng> manualList;
    double mPrevLat;
    double mPrevLong;
    float[] mResults;
    float mTotalResults = 0;
    PolylineOptions mUserPolyOptions;
    PolylineOptions mManualPolyOptions;
    ArrayList<Polyline> mManualLineList;
    Polyline mManualLine;
    int mPolyLineCount = 0;

    //Interpolation var
    //ArrayList holds the interpolated latlngs
    ArrayList<LatLng> interpolatLoadedArrayList;
    double counterDistance = 0;
    int counterFinalDistance = 0;

    //InterpolationClass --- CONTAINS GOOGLEMAPS API METHODS
    Circle circ1;
    Circle circ2;

    //Boundary Tracker var
    int boundaryCounter = 0;
    int firstBound = 0;
    int secondBount = 0;
    boolean circleIsDrawn = false;
    CircleOptions circleOptions1;
    CircleOptions circleOptions2;

    //Distance Var
    double routeDistance = 0;
    double fullDistance = 0;
    ArrayList<Double> optimizedDistanceList;

    //My tool box
    MyTools myTools = new MyTools();

    //RouteStatistics class database bar
    RouteStatsData routeStatsData;

    //Decides if the boundary function is to be used
    boolean boundaryDecider = false;

    //Copy of the view
    View myView;

    //Relative layout var
    RelativeLayout relativeLayout;


    //RouteStatsInterface callback
    RunRouteStatsInterface callBack;

    //Route in String
    String strNewRoute = "";

    //Total distance roundedTotalUserResults
    double rTUR = 0;
    //Total calories
    double myCals = 0;


    //Distance counter for each miles completed and miles string holder
    double dblCompletedMile = 0;
    String strMiles = " ";
    String strMilesTimes = " ";
    int mileCounter = 1;
    String totalDist = "";

    double originalMiles = 0;



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //Get bundle arguments
        routePosition = getArguments().getInt("Position");
        return inflater.inflate(R.layout.run_route_fragment, container, false);


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.RunRouteLayout);

        myView = view;

        //Initialize stats and route database classes
        routeStatsData = new RouteStatsData(getActivity());
        routeData = new RouteData(getActivity());

        //Circle Options for Boundary Tracking Function

        circleOptions1 = new CircleOptions();
        circleOptions2 = new CircleOptions();


        //Hide toolbar
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        //Initialize location manager with location services
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);


        //---------------------------TESTING OPTIONS INITIALIZATION------
        manualList = new ArrayList<LatLng>();
        mManualLineList = new ArrayList<>();
        //////////////////////////////////-------------------------------

        //Interpolation latlng arraylist
        interpolatLoadedArrayList = new ArrayList<LatLng>();


        distanceText = (TextView) view.findViewById(R.id.textDistanceRR);
        caloriesText = (TextView) view.findViewById(R.id.textCaloriesRR);
        timeText = (TextView) view.findViewById(R.id.textTimeRR);
        paceText = (TextView) view.findViewById(R.id.textPaceRR);
        btnStart = (Button) view.findViewById(R.id.btnStartRR);
        btnPause = (Button) view.findViewById(R.id.btnPauseRR);
        btnReset = (Button) view.findViewById(R.id.btnResetRR);

        //Route data and arraylist initialization
        routeData = new RouteData(getActivity());
        //User ArrayLists
        latlongList = new ArrayList<>();
        userLineList = new ArrayList<>();

        //Loaded ArrayLists
        routeStringList = new ArrayList<String>();
        routeDistanceList = new ArrayList<String>();
        latLngArrayList = new ArrayList();
        manualLineList = new ArrayList<Polyline>();
        optimizedDistanceList = new ArrayList<Double>();

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.run_route_map);

        mapFragment.getMapAsync(this);
        //Start gps
        mGoogleApiClient.connect();

        //Retrieve route data
        retrieveData();
        //Convert the data retrieved int LatLng using the position retrieved by bundle
        convertStringToLatLong(routePosition);


        //BUTTONS SETTINGS ______________________________________________________________________
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startLocationUpdates();
                startTimer();
                //Turn on boundary fucntion
                boundaryDecider = true;
                //The first 2 circles are drawn
                circ1 = rMap.addCircle(circleOptions1.center(interpolatLoadedArrayList.get(boundaryCounter))
                        .radius(30)
                        .strokeWidth(1)
                        .strokeColor(Color.parseColor("#4D00ABFF"))
                        .fillColor(Color.parseColor("#4D00ABFF")));
                circ2 = rMap.addCircle(circleOptions2.center(interpolatLoadedArrayList.get(boundaryCounter + 1))
                        .radius(30)
                        .strokeWidth(1)
                        .strokeColor(Color.parseColor("#4D00FF7C"))
                        .fillColor(Color.parseColor("#4D00FF7C")));
                //Increment the counter for the boundaries on track to work
                boundaryCounter++;
            }
        });
        btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pauseTimer();
            }
        });
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetTimer();
            }
        });
        //_______________________________________________________________________________________


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        rMap = googleMap;

        rMap.setOnMapClickListener(this);
        rMap.setOnMapLongClickListener(this);
        rMap.setOnCameraChangeListener(this);
        rMap.setOnMyLocationButtonClickListener(this);
        enableMyLocation();

        //Connect google API
        mGoogleApiClient.connect();

        /*Calculations and route display will be made after the map is connected-
        -otherwise a null exception would arise once the polyline draw is attempted.*/
        calculationsLoaded();


        //Set the camera view to show the entire route
        rMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngArrayList.get(0), 16));
    }

    //Enable the the my location
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission((AppCompatActivity) getActivity(), LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (rMap != null) {
            // Access to the location has been granted to the app.
            rMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "Location services connected.");
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    //////////TESTING::::: WILL MIMIC USER"S LOCATION POINT FOR BOUNDARY TEST
    @Override
    public void onMapClick(LatLng point) {
        boundaryFunction(point);

        latlongList.add(point);
        //Add the first point the the list
        LatLng lt = new LatLng(point.latitude, point.longitude);
        manualList.add(lt);

        //TESTING::::::::
        //latlongList.add(point);

        Log.i("ClickedPointMap", point.latitude + " ," + point.longitude);

        //Caclulate the distance
        /*if (!(manualList.isEmpty())) {
            if (manualList.size() < 2) {
                //The arraylist only holds one value, use that for the first value
                mPrevLat = manualList.get(0).latitude;
                mPrevLong = manualList.get(0).longitude;
            } else {
                for (int i = 0; i < manualList.size(); ++i) {
                    //Initialize the results float to retrieve the distance between points
                    mResults = new float[1];
                    //New location object to hold the current location
                    Location location = new Location("");
                    location.setLatitude(point.latitude);
                    location.setLongitude(point.longitude);
                    //Calculate the distance between the previous point and the current
                    location.distanceBetween(mPrevLat, mPrevLong, point.latitude, point.longitude, mResults);

                    //Update the total distance
                    mTotalResults = mTotalResults + mResults[0];
                    // Modify the previous ones with the ones just used
                    mPrevLat = point.latitude;
                    mPrevLong = point.longitude;


                    //TESTING -------------------_TESTING INTERPOLATE ON EVERY 10m_-----------
                    double convertFloatDbl = 0;
                    convertFloatDbl = (double) mTotalResults;
                    double mDistance = 0;
                    mDistance = Math.round(convertFloatDbl * 100.0) / 100.0;
                    ///
                    double interCounter = 0;
                    if (mDistance > 10) {
                        double remainder = mDistance % 10;
                        double newDist = mDistance - remainder;
                        //Get the amount of times that 10 exists in the distance
                        double tenCounter = newDist / 10;
                        Log.i("TenCounter", tenCounter + "");
                        double fraction = 10 / newDist;

                        //The last point
                        LatLng pointLatlng = new LatLng(point.latitude, point.longitude);
                        //Holds the interpolated point to mark
                        LatLng interpolatedLatlng;
                        //ArrayList holds the interpolated latlngs
                        ArrayList<LatLng> interpolatArrayList = new ArrayList<>();
                        //Loops to get the 10m instance
                        for (int t = 0; t < tenCounter; t++) {
                            //Mark the latlang
                            interCounter = interCounter + fraction;
                            interpolatedLatlng = SphericalUtil.interpolate(manualList.get(0), pointLatlng, interCounter);
                            //Add the loops latlang interpolated point to the arraylist
                            interpolatArrayList.add(interpolatedLatlng);
                            //Test draw circles on the new arraylist of latlongs


                            /*Circle circ = rMap.addCircle(new CircleOptions()
                                    .center(interpolatArrayList.get(t))
                                    .radius(10)
                                    .strokeWidth(5)
                                    .strokeColor(Color.BLUE)
                                    .fillColor(Color.parseColor("#26006CFF")));
                            Log.i("InterPolatedCounter", "" + interpolatArrayList.size());*/

        /*
                        }
                        //Calculate the interpolation for the corresponding fraction
                        //interpolatedLatlng = SphericalUtil.interpolate(manualList.get(0), pointLatlng, fraction);
                        //Log.i("TestingNewLatlng", ""+ interpolatedLatlng.latitude + ", " + interpolatedLatlng.longitude);
                    }
                    Log.i("TESTING_MANUAL_DIST", "" + mDistance);
                    //TESTING -------------------_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-__-----------


                    /*double roundedDist;
                    dist = (double) mTotalResults * 0.00062137;
                    roundedDist = Math.round(dist * 100.0) / 100.0;
                    distanceText.setText("" + roundedDist + " mi");



        /*
                }
            }
        }
        //Create the polyline from lineLIst arraylist and add it to manualLine for


        mManualPolyOptions = new PolylineOptions();
        mManualPolyOptions.addAll(manualList);
        mManualPolyOptions.color(Color.parseColor("#4D0970FC"));
        mManualLine = rMap.addPolyline(mManualPolyOptions);
        //Add the line to the polylinelist
        manualLineList.add(mManualLine);
        polylineCount++;

                            */
    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }


    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    //Location listener-  calculations on location updates
    LocationListener locationListener = new LocationListener() {
        //Updating location
        @Override
        public void onLocationChanged(Location location) {
            //Check user's location in boundary
            //LatLng userLocation = new LatLng(location.getLatitude(), location.getLongitude());

            //If the start button is used, initialize the boundary function
            //if (boundaryDecider == true) {
              //  boundaryFunction(userLocation);

            //Toast.makeText(getApplicationContext(), "-TESTING-" + location.getLatitude() + ", " + location.getLongitude(), Toast.LENGTH_SHORT).show();
            Log.i("NewLocationRequest", "LatLang: " + location.getLatitude() + ", " + location.getLongitude() + " Accuracy: " + location.getAccuracy());

            if (location == null) {
                // Blank for a moment..
                Log.i("Location-Null.", "NOT WORKING");
            } else {
                if (location.getAccuracy() > locationAccuracy) {
                    //  Log.i("BadAccuracy", "The accuracy is is low");
                    Toast.makeText(getActivity(), "Bad Accuracy " + location.getAccuracy(), Toast.LENGTH_SHORT).show();
                } else {
                    if (booleanTrack == false) {
                        Log.i("Location-OFF", "booelanTrack is false");
                    } else if (booleanTrack == true) {

                        Log.i("LocationStatus.", "WORKING" + "Lat and Long: " + location.getLatitude() + ", " + location.getLongitude());
                        /*
                         *Get the locations lat and long
                         *strLong = location.getLong itude() + "";
                         *strLat = location.getLatitude() + "";
                         *dblLong = Double.parseDouble(strLong);
                         dblLat = Double.parseDouble(strLat);
                         Round the values to the nearest 100,000th for a more efficient reading of the arraylist
                         * This will also reduce the amount of data that will be loaded into the array.
                         * The camera movement will be more stable as a result.
                         *double lg = Math.round(dblLong * 10000.0) / 10000.0;
                         *double lt = Math.round(dblLat * 10000.0) / 10000.0;
                         *LatLng updatedLocation = new LatLng(lt, lg);
                         *Add the updated location with the LatLang to the array list
                         *latlongList.add(updatedLocation);
                         */

                        //Calculate the distance
                        //Add the first point the the list
                        LatLng LL = new LatLng(location.getLatitude(), location.getLongitude());
                        //latlongList.add(LL);


                        //Caclulate the distance
                        if (!(latlongList.isEmpty())) {
                            if (latlongList.size() < 2) {
                                //The arraylist only holds one value, use that for the first value
                                previousLatUser = latlongList.get(0).latitude;
                                previousLngUser = latlongList.get(0).longitude;
                            } else {
                                for (int i = 0; i < latlongList.size(); ++i) {
                                    //Initialize the results float to retrieve the distance between points
                                    userResults = new float[1];

                                    //Calculate the distance between the previous point and the current
                                    Location.distanceBetween(previousLatUser, previousLngUser, location.getLatitude(), location.getLongitude(), userResults);
                                    //Update the total distance
                                    totalUserResults = totalUserResults + userResults[0];
                                    // Modify the previous ones with the ones just used
                                    previousLatUser = location.getLatitude();
                                    previousLngUser = location.getLongitude();
                                    //set the textview with the total distance (rounded to .10) after miles conversion
                                    rTUR = (double) totalUserResults * 0.00062137;

                                    //Everytime a mile is completed the distance is added to mileslist and the counter is reset
                                    dblCompletedMile = Math.round(rTUR * 100.0)/100.0;
                                    Log.i("TheMilesAre", dblCompletedMile + "");
                                    if(dblCompletedMile > mileCounter){
                                        //String will hold all the miles seperated by " "
                                        strMiles = strMiles + " " + dblCompletedMile + " ";
                                        dblCompletedMile = 0;

                                        //Stamp the current time
                                        strMilesTimes = " " +strMilesTimes + " " + strTime + " ";

                                        Log.i("InfoMilesTimes", strMiles + " -------- " + strMilesTimes);
                                        //Increment the counter for the next mile
                                        mileCounter++;
                                        //Set the result as a string and insert into data
                                    }

                                    //Calorie Settings (TIME CONVERSTION TO MINUTES)
                                    double timeCals = 0;
                                    double dblSec = 0;
                                    double dblMin = 0;
                                    dblSec = Double.parseDouble(strSeconds);
                                    dblMin = Double.parseDouble(strMinutes);
                                    timeCals = ((dblMin * 60) + dblSec);
                                    Log.i("totalTimeCals", "" + timeCals);


                                    myCals = calculateCalories(timeCals, totalUserResults);
                                    myCals = Math.round(myCals * 100.0) / 100.0;
                                    Log.i("TestingClaories", "Calories: " + myCals);
                                    //Set the calories TextView
                                    caloriesText.setText("" + myCals);


                                }
                            }
                        }
                        /*
                        //Create the polyline from latlongList arraylist and add it to manualLine for edit
                        Log.i("TestingDistance", "Distance: " + totalResults);
                        userPolyOptions = new PolylineOptions();
                        userPolyOptions.addAll(latlongList);
                        userPolyOptions.color(Color.GREEN);
                        userLocationLine = rMap.addPolyline(userPolyOptions);
                        //Add the line to the polylinelist
                        userLineList.add(userLocationLine);
                        userLineCount++;

                        //Circle Options
                        Circle circle = rMap.addCircle(new CircleOptions()
                                .center(latlongList.get(0))
                                .radius(100)
                                .strokeWidth(10)
                                .strokeColor(Color.BLUE)
                                .fillColor(Color.argb(128, 255, 0, 0)));

                        */

                        /*//Calorie Settings (TIME CONVERSTION TO MINUTES)
                        double timeCals = 0;
                        double dblSec = 0;
                        double dblMin = 0;
                        dblSec = Double.parseDouble(strSeconds);
                        dblMin = Double.parseDouble(strMinutes);
                        timeCals = ((dblMin *60) + dblSec);
                        Log.i("totalTimeCals", ""+timeCals);*/
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), "Tracker OFF", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }

    };

    //Boundary algorithm
    public boolean inBoundary(Float dist, Location userLoc, Location boundaryLoc) {
        boolean isInside = false;
        float[] totalBound = new float[2];
        totalBound[0] = dist;

        Location.distanceBetween(userLoc.getLatitude(), userLoc.getLongitude(), boundaryLoc.getLatitude(),
                boundaryLoc.getLongitude(), totalBound);

        return isInside;
    }

    /*
     Start location updates
     */
    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        Toast.makeText(getActivity().getApplicationContext(), "Starting Location Tracker", Toast.LENGTH_SHORT).show();
        //--TESTING-- LOCATION REQUESTS    time, meters
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 3, locationListener);
    }

    /*
       Stop location updates.
     */
    public void stopLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            // return;
        }
        Toast.makeText(getActivity(), "Location Tracker OFF", Toast.LENGTH_SHORT).show();
        locationManager.removeUpdates(locationListener);
    }


    //Route change listener methods
    @Override
    public void onSelectRoutePressed(int position) {
        Toast.makeText(getActivity(), "The Route's Position is " + position, Toast.LENGTH_SHORT).show();
        Log.i("TestingPositionCall", position + "");
    }

    @Override
    public void OnCreateRoutePressed() {

    }

    @Override
    public void OnRemoveRoutePressed() {

    }

    @Override
    public void OnEditRoutePressed() {

    }

    /**
     * Calorie calculations are done using the ACSM formula for running
     * ---[VO2 = (.2*metersMin) + 3.5]---
     * the kcals are calculated using this formula
     * ---[kcal/min = 5((VO2*BMkg)/1000]---
     */
    public double calculateCalories(double time, float distance) {
        double vO2 = 0;
        double metersMin = 0;
        double kCal = 0;
        double totalKCal = 0;
        double timeVar = 0;
        double kcalVar = 0;

        //Conversions
        timeVar = time / 60;
        metersMin = distance / timeVar;
        //VO2 calculations
        vO2 = (.2 * metersMin) + 3.5;
        //Kcal calculations
        kcalVar = (vO2 * weight) / 1000;
        kCal = 5 * kcalVar;
        totalKCal = timeVar * kCal;

        return totalKCal;
    }


    //Timer
    private Runnable updateTimer = new Runnable() {
        @Override
        public void run() {
            timeMilli = SystemClock.uptimeMillis() - startTime;
            timeUpdate = timeBuffer + timeMilli;
            int seconds = (int) (timeUpdate / 1000);
            int minutes = seconds / 60;
            seconds = seconds % 60;
            int milliseconds = (int) (timeUpdate % 1000);
            //Format time
            sec = String.format("%02d", seconds);
            mili = String.format("%02d", milliseconds);
            //Set strTime with the time to display
            strTime = "" + minutes + ":" + sec;
            strSeconds = sec;
            strMinutes = "" + minutes;

            timeHandler.postDelayed(this, 0);
            //Set the TextView with the updating time
            timeText.setText(strTime);
        }
    };

    //Start timer
    public void startTimer() {
        startTime = SystemClock.uptimeMillis();
        timeHandler.postDelayed(updateTimer, 0);
    }

    //Pause timer
    public void pauseTimer() {
        timeBuffer += timeMilli;
        timeHandler.removeCallbacks(updateTimer);
    }

    //Reset timer
    public void resetTimer() {
        pauseTimer();
        //Timer vars
        startTime = 0L;
        timeMilli = 0L;
        timeBuffer = 0L;
        timeUpdate = 0L;
        strTime = "";
        sec = "";
        mili = "";
        timeText.setText("00:00");
    }


    //Route calculations loader
    public void calculationsLoaded() {
        //Will holds the camulative distances until it equals 10 then will reset
        double distanceCounter = 0;
        //Make sure the map is loaded and ready
        if (rMap == null) {

        } else {
            //Caclulate the distance
            if (!(latLngArrayList.isEmpty())) {
                //Load the loadedArrayList with the first point from latLngArrayList
                interpolatLoadedArrayList.add(latLngArrayList.get(0));

                //A counter is used here for to attain the first latlong as the first previous
                for (int counter = 1; counter < latLngArrayList.size(); counter++) {
                    if (counter < 2) {
                        //The arraylist only holds one value, use that for the first value
                        previousLatitude = latLngArrayList.get(0).latitude;
                        previousLongitude = latLngArrayList.get(0).longitude;
                        Log.i("CounterLessThan2", "Counter less than 2");
                    } else {

                        for (int i = 0; i < latLngArrayList.size(); ++i) {
                            //Initialize the results float to retrieve the distance between points
                            results = new float[1];
                            //New location object to hold the current location
                            Location location = new Location("");
                            location.setLatitude(latLngArrayList.get(i).latitude);
                            location.setLongitude(latLngArrayList.get(i).longitude);
                            //Calculate the distance between the previous point and the current
                            Location.distanceBetween(previousLatitude, previousLongitude, location.getLatitude(), location.getLongitude(), results);
                            //Update the total distance
                            totalResults = totalResults + results[0];

                            //[=][=][=][=][=][=][=][=][=]--INTERPOLATION--[=][=][=][=][=][=][=][=][=][=][=][=][=][=]
                            /**
                             ************INTERPOLATION ALGORITHM*************
                             */
                            //The current distance in converted to double
                            float cr = results[0];
                            double currentDistance = (double) cr;
                            Log.i("TheCurrentDistance", currentDistance + "");


                            //Counters will hold values until they equal 10
                            counterDistance = counterDistance + currentDistance;
                            Log.i("DistanceForCounter", counterDistance + "");
                            //Hold the total value - the current distance to get the total d of the previous points
                            double ctp = counterDistance - currentDistance;
                            Log.i("CTPforPreviousP", ctp + "");

                            //The STARTING point for the interpolation
                            LatLng firstPoint = new LatLng(previousLatitude, previousLongitude);
                            //The ENDING point for the interpolation
                            LatLng pointLatlng = new LatLng(location.getLatitude(), location.getLongitude());

                            //Will execute only if the distance between the points is > 10 meters and the previous points d is < 10
                            if (counterDistance > 10 && ctp < 10 && ctp > 0) {
                                //Get the remaining to 10 from ctp then find the fraction for the meter point
                                double fr = 10 - ctp;
                                double ff = fr / currentDistance;
                                ff = Math.round(ff * 100.00) / 100.00;
                                counterDistance = currentDistance - fr;
                                Log.i("FractionPoint", "" + ff);
                                Log.i("TestingLatLngs", firstPoint.latitude + "," + firstPoint.longitude + "   " + pointLatlng.latitude + "," + pointLatlng.longitude);


                                //Find the interpolated location for the corresponding fraction
                                LatLng interpolatedLatlng;
                                interpolatedLatlng = interpolation(firstPoint, pointLatlng, ff);
                                //Add the loops latlang interpolated point to the arraylist
                                interpolatLoadedArrayList.add(interpolatedLatlng);
                            }
                            double counterTens = counterDistance;
                            if (counterTens > 10) {//|| ctp > 10 || ctp == 0
                                double limit = 10;
                                //If the 10 values counter is > 10
                                while (counterTens > 10) {
                                    //Get the fraction in which the corresponding 10 value lies on then interpolate it and add to list
                                    double ff = limit / counterDistance;

                                    LatLng newLatlong = interpolation(firstPoint, pointLatlng, ff);
                                    interpolatLoadedArrayList.add(newLatlong);
                                    //counterDistance = counterDistance - 10;
                                    Log.i("Fraction&CurrentDist", "FRACTION: " + ff + "  DIST: " + currentDistance +
                                            "  counterDistance: " + counterDistance);
                                    //Increment the limit by 10 to find the next 10 value's fraction on the next loop
                                    limit = limit + 10;
                                    //Decrement the counter by 10
                                    counterTens = counterTens - 10;
                                    Log.i("TensIterationVal", "" + counterTens);
                                    //Once the counter is < 10, add the remaining value to the counterDistance for next D(px, py) loop
                                    if (counterTens < 10) {
                                        //The remaining value after all 10's iterations
                                        counterDistance = counterTens;
                                    }
                                }
                            }

                            //[=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=]

                            // Modify the previous ones with the ones just used
                            previousLatitude = location.getLatitude();
                            previousLongitude = location.getLongitude();

                        }
                        //[=][=][=][=][=][=][=][=][=]---PART OF INTERPOLATION ALGORITHM---[=][=][=][=][=][=][=][=][=][=][=]
                        //[=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=][=]
                        break;
                    }

                    //Create the polyline from latlongList arraylist and add it to manualLine for edit
                    runRoutePolyoptions = new PolylineOptions();
                    runRoutePolyoptions.addAll(latLngArrayList);
                    Log.i("TestingDistance", "Distance: " + latLngArrayList.size());
                    runRoutePolyoptions.color(Color.MAGENTA);
                    runRouteLine = rMap.addPolyline(runRoutePolyoptions);
                    //Add the line to the polylinelist
                    manualLineList.add(runRouteLine);
                    polylineCount++;


                }
                //Add the last point from latLngArrayList int loadedArrayList
                interpolatLoadedArrayList.add(latLngArrayList.get(latLngArrayList.size() - 1));
                //Create the new loaded array list line
                userPolyOptions = new PolylineOptions();
                userPolyOptions.addAll(interpolatLoadedArrayList);
                userPolyOptions.color(Color.GREEN);
                userLocationLine = rMap.addPolyline(userPolyOptions);
                //Add the line to the polylinelist
                userLineList.add(userLocationLine);
                userLineCount++;

                //Add the distances between new locations
                if (!(interpolatLoadedArrayList.isEmpty())) {
                    LatLng first = null;
                    for (int r = 0; r < interpolatLoadedArrayList.size(); r++) {
                        if (r == 0) {
                            first = interpolatLoadedArrayList.get(0);
                        } else {
                            Location newLocation = new Location("");
                            float[] newFloat;
                            newFloat = new float[1];
                            if (r == 0) {

                            }
                            Location.distanceBetween(first.latitude, first.longitude, interpolatLoadedArrayList.get(r).latitude,
                                    interpolatLoadedArrayList.get(r).longitude, newFloat);
                            //Add the distance to the list
                            optimizedDistanceList.add((double) newFloat[0]);

                            first = new LatLng(interpolatLoadedArrayList.get(r).latitude, interpolatLoadedArrayList.get(r).longitude);
                        }
                    }
                }
            }
        }
    }

    //Boundary Track Function
    public void boundaryFunction(LatLng point) {
        //Creates a circles on every point inside the given arraylist
        if (boundaryCounter < interpolatLoadedArrayList.size()) {
            //Check and see if the clicked point is in the interpolated arraylist for the second boundary
            Double distCirc1 = SphericalUtil.computeDistanceBetween(circ1.getCenter(), point); //<= circ.getRadius();
            Log.i("CircleDist", "" + distCirc1);

            //Check and see if the clicked point is in the interpolated arraylist for the second boundary
            Double distCirc2 = SphericalUtil.computeDistanceBetween(circ2.getCenter(), point); //<= circ.getRadius();
            Log.i("CircleDist", "" + distCirc2);
            boolean inCircle = false;
            if (distCirc1 < circ1.getRadius()) {
                //USER IS INSIDE CIRCLE 1
                //Allow tracking user location tracking
                booleanTrack = true;
                inCircle = true;
            } else {
                inCircle = false;
            }
            if (distCirc2 < circ2.getRadius()) {
                //Toast.makeText(getActivity(), "INSIDE BOUNDARY 2 :::: CLEARED :::::", Toast.LENGTH_SHORT).show();
                circ1.remove();
                circ2.remove();
                inCircle = true;
                if (!(interpolatLoadedArrayList.size() - 1 == boundaryCounter)) {
                    //Allow tracking user location tracking
                    booleanTrack = true;

                    Log.i("DoesContain", "The list contains j + 1");
                    //Replace the second boundary circle with firstBoundary "safe boundary"
                    circ1 = rMap.addCircle(circleOptions1.center(interpolatLoadedArrayList.get(boundaryCounter))
                            .radius(30)
                            .strokeWidth(1)
                            .strokeColor(Color.parseColor("#4D00ABFF"))
                            .fillColor(Color.parseColor("#4D00ABFF")));

                    //Add the next boundary circle
                    circ2 = rMap.addCircle(circleOptions2.center(interpolatLoadedArrayList.get(boundaryCounter + 1))
                            .radius(30)
                            .strokeWidth(1)
                            .strokeColor(Color.parseColor("#4D00FF7C"))
                            .fillColor(Color.parseColor("#4D00FF7C")));
                    boundaryCounter++;

                    //Decrement the total distance by the value of the corresponding distance for the points
                    Log.i("TestingValues", "LatLng: " + interpolatLoadedArrayList.size() + "  DIST: " + optimizedDistanceList.size());

                    for (int i = 0; i < optimizedDistanceList.size(); i++) {
                        Log.i("ReadingDistData", optimizedDistanceList.get(i) + "");
                    }

                    routeDistance = routeDistance - optimizedDistanceList.get(boundaryCounter - 1);

                    double dblMiles = myTools.convertToDoubleMiles(routeDistance);
                    distanceText.setText(dblMiles + "");

                } else {
                    //The route finishes here
                    Toast.makeText(getActivity(), "Congratulations, You Have Completed The Route", Toast.LENGTH_SHORT).show();


                    //Save route to string
                    saveNewRouteToString();

                    //Finish route settings call
                    finishedRoute();

                    //send callback to interface into MapActivity which will load stats fragment
                    //NEEDS TO BE (route, time, distance, calories, miles, miletimes)

                    //Route finished methods
                    routeFinished();


                }
            } else {
                if (inCircle == false) {
                    //Disable user location tracker
                    booleanTrack = false;
                    //Toast.makeText(getActivity(), "WARNING :::PLEASE GET BACK TO ROUTE::::", Toast.LENGTH_SHORT).show();
                    //A warning sound will play when user is outisde of the route.
                    final MediaPlayer mp = MediaPlayer.create(getActivity(), R.raw.warning_outside_of_route);
                    mp.start();
                }
            }
        }


    }




    //String to latlang conversions
    public void convertStringToLatLong(int position) {
        //Convert the current distance to double and add to distance text
        routeDistance = Double.parseDouble(routeDistanceList.get(position));
        Log.i("TheTotalDistance", routeDistance + "");
        //Get the full distance for saving purposes
        fullDistance = routeDistance;

        double inMilesConversion = myTools.convertToDoubleMiles(routeDistance);
        originalMiles = inMilesConversion;

        distanceText.setText(inMilesConversion + " mi");

        String strSp = routeStringList.get(position);
        //Split by space
        String[] splited = strSp.split("\\s+");
        //TESTING SPLITTED ARRAY
        for (int x = 0; x < splited.length; x++) {
            Log.i("SplitedArray", splited[x]);
        }
        //TESTING SPLITTED ARRAY BY COMMA
        String strTest1 = splited[1];
        String[] strTest = strTest1.split(",");
        String show = strTest[0];
        Log.i("SplittComma", " SPLIT" + show);

        // List<String> splitedList = new ArrayList<String>();
        //Clear the latlonglist and drawing lists for use
        latLngArrayList.clear();
        manualLineList.clear();
        for (int i = 1; i < splited.length; i++) {
            String[] strLatLangs = splited[i].split(",");
            Double lat = Double.parseDouble(strLatLangs[0]);
            Double lo = Double.parseDouble(strLatLangs[1]);
            LatLng location = new LatLng(lat, lo);
            latLngArrayList.add(location);
        }
        for (int t = 0; t < latLngArrayList.size(); t++) {
            Log.i("LatLngConverted", latLngArrayList.get(t).latitude + ", " + latLngArrayList.get(t).longitude);
        }
    }

    //Data base retrieval
    // Retrieve data from ROUTES_TABLE into a string
    public void retrieveData() {
        Cursor cursor;
        try {
            cursor = routeData.getAllData();
            // Check for data
            if (cursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();

            } else {
                while (cursor.moveToNext()) {
                    // Get data from index
                    //Distances
                    routeStringList.add(cursor.getString(1));
                    Log.i("RouteDataString", cursor.getString(1));
                    routeDistanceList.add(cursor.getString(2));
                    Log.i("RouteDistanceString", cursor.getString(2));

                    //Distance

                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "No Data Loaded", Toast.LENGTH_SHORT).show();
        }
    }

    //Interpolation and distance tools
    //INTERPOLATED METHODs FROM GOOGLEMAPS UTILS API  ////DID NOT WORK BY CALLING CLASS
    public LatLng interpolation(LatLng first, LatLng last, double fraction) {
        double fromLat = Math.toRadians(first.latitude);
        double fromLng = Math.toRadians(first.longitude);
        double toLat = Math.toRadians(last.latitude);
        double toLng = Math.toRadians(last.longitude);
        double cosFromLat = Math.cos(fromLat);
        double cosToLat = Math.cos(toLat);
        double angle = computeAngleBetween(first, last);
        double sinAngle = Math.sin(angle);
        if (sinAngle < 1.0E-6D) {
            return first;
        } else {
            double a = Math.sin((1.0D - fraction) * angle) / sinAngle;
            double b = Math.sin(fraction * angle) / sinAngle;
            double x = a * cosFromLat * Math.cos(fromLng) + b * cosToLat * Math.cos(toLng);
            double y = a * cosFromLat * Math.sin(fromLng) + b * cosToLat * Math.sin(toLng);
            double z = a * Math.sin(fromLat) + b * Math.sin(toLat);
            double lat = Math.atan2(z, Math.sqrt(x * x + y * y));
            double lng = Math.atan2(y, x);
            return new LatLng(Math.toDegrees(lat), Math.toDegrees(lng));
        }
    }

    private double distanceRadians(double lat1, double lng1, double lat2, double lng2) {
        return arcHav(havDistance(lat1, lat2, lng1 - lng2));
    }

    double computeAngleBetween(LatLng from, LatLng to) {
        return distanceRadians(Math.toRadians(from.latitude), Math.toRadians(from.longitude), Math.toRadians(to.latitude), Math.toRadians(to.longitude));
    }

    // MATH UTILS METHOD FROM GOOGLEMAPS API
    double hav(double x) {
        double sinHalf = Math.sin(x * 0.5D);
        return sinHalf * sinHalf;
    }

    double havDistance(double lat1, double lat2, double dLng) {
        return hav(lat1 - lat2) + hav(dLng) * Math.cos(lat1) * Math.cos(lat2);
    }

    double arcHav(double x) {
        return 2.0D * Math.asin(Math.sqrt(x));
    }

    //Method contains tools to finish route
    public void routeFinished() {
        stopLocationUpdates();
        resetTimer();
    }

    public void setMapMargins() {
        ViewGroup.LayoutParams params = mapFragment.getView().getLayoutParams();
        params.height = 3000;
        params.width = 3000;
        mapFragment.getView().setLayoutParams(params);
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            callBack = (RunRouteStatsInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement SelectRouteInterfaceListener");
        }
    }

//Stats interface
// MapActivity must implement this interface
public interface RunRouteStatsInterface {
    void onRouteRan(String routeData, String time, String distance, String calories, String miles, String milesTimes);

}

    public void startFragment() {
        //New fragment transaction needed
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();


        Fragment currentFragment = new RouteStats();
        fragmentTransaction.replace(R.id.routeStatsFragment, currentFragment);
        //fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void saveNewRouteToString() {
        for (int i = 0; i < interpolatLoadedArrayList.size(); i++) {
            strNewRoute = strNewRoute + " " + interpolatLoadedArrayList.get(i).latitude + "," + interpolatLoadedArrayList.get(i).longitude + " ";
        }
    }


    //Settings for route completion
    public void finishedRoute(){
        //Add the remaining distance to the miles and milesTimes string
        //Get the remainder by subtracting the whole number then overwrite that value by 1-value then round to .00
        //If it is less than 1, use the current distance ('which dblCompletedMile has')
        double value = 0;
        if(dblCompletedMile < 1){
            value = dblCompletedMile;
        }else {
            value = dblCompletedMile - (mileCounter - 1);
            value = 1 - value;
            value = Math.round(value * 100.00) / 100.00;
        }

        strMiles = strMiles + value + " ";
        strMilesTimes = strMilesTimes + strTime;
        //Save data
        String strRoute = "";
        /*for (int i = 0; i < latlongList.size(); i++) {
            strRoute = strRoute + " " + latlongList.get(i).latitude + "," + latlongList.get(i).longitude + " ";
        }
        */

        //JUST FOR TESTING PURPOSES------------------Add blank info if distance < 1
        //if(roundedDist < 1){
        //  strRoute = " 41.8781,-87.6298 " ;
        //}
        //--------------------------------------------
        totalDist = " " + rTUR + " ";
        //routeData.insertData(strNewRoute, totalDist);
        int routeDataInput = routePosition;
        routeStatsData.insertData(routeDataInput, strTime, originalMiles + "", strMiles, strMilesTimes, "", "","","");

        callBack.onRouteRan(strNewRoute, strTime, originalMiles+"", myCals+"", strMiles, strMilesTimes);


    }
}

