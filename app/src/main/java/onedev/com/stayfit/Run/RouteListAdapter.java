package onedev.com.stayfit.Run;

        import android.app.Activity;
        import android.content.Context;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
        import android.widget.ImageView;
        import android.widget.TextView;

        import java.util.ArrayList;

        import onedev.com.stayfit.R;

public class RouteListAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<String> distance;
    private ArrayList<String> dates;
    private static LayoutInflater inflater=null;
    public RouteImageLoad imageLoader;

    public RouteListAdapter(Activity a, ArrayList<String> dist, ArrayList<String> d) {
        activity = a;
        distance =dist;
        dates = d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader=new RouteImageLoad();
    }

    public int getCount() {
        return distance.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.route_list_items, null);

        TextView text=(TextView)vi.findViewById(R.id.routeListText);
        TextView txtDate = (TextView) vi.findViewById(R.id.textDatesRL);
        ImageView image=(ImageView)vi.findViewById(R.id.routeListImg);

        MyTools myTools = new MyTools();

        //Set the textviews to the corresponding position in the STR ArrayList of routes and dates
        //Convert string to double meters then to miles
        double dblMiles = 0;
        String holdString = distance.get(position);
        dblMiles = myTools.convertStringToMiles(holdString);
        text.setText(dblMiles + "");
        txtDate.setText(dates.get(position));
        //Show the image which corresponds to the route of that position

        imageLoader.showImage(activity, distance.get(position), image);
        return vi;
    }
}
