package onedev.com.stayfit.Run;

import onedev.com.stayfit.R;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.CameraPosition;


public class RunFragment extends Fragment {
    //TESTING TOOLS////
    Button btnDeletePrefs;
    SharedPreferences sharedPreferences;
    String spKEY = "FirstLaunch";


    //UI var
    Button btnAutoRoute;
    Button btnManualRoute;

    //Fragment option
    Fragment currentFragment = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        return inflater.inflate(R.layout.run_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);


        sharedPreferences = getActivity().getSharedPreferences(spKEY, getActivity().MODE_PRIVATE);
        btnDeletePrefs = (Button) view.findViewById(R.id.btnDeletePrefs);

        btnAutoRoute = (Button) view.findViewById(R.id.btnIntoAutoRoute);
        btnManualRoute = (Button) view.findViewById(R.id.btnIntoManualRoute);

		/*
        Intent myIntent = new Intent(A.this, B.class);
        myIntent.putExtra("intVariableName", intValue);
        startActivity(myIntent);
        Receiver Side:

        Intent mIntent = getIntent();
        int intValue = mIntent.getIntExtra("intVariableName", 0);
		 */

        //Button to Auto route
        btnAutoRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Start activity with intent(will be the decider for the fragment to add)
                Intent i = new Intent(getActivity(), MapsActivity.class);
				i.putExtra("FragmentKey", 0);
                startActivity(i);

				/*//Fragment settings
				FragmentManager fm = getActivity().getSupportFragmentManager();
				FragmentTransaction fragmentTransaction = fm.beginTransaction();
				if (currentFragment == null) {
				} else {
					fragmentTransaction.remove(currentFragment);
				}
				currentFragment = new AutoRoute();
				fragmentTransaction.replace(R.id.runFragment, currentFragment);
				fragmentTransaction.addToBackStack(null);
				fragmentTransaction.commit();*/
            }
        });
        //Button to Manual route
        btnManualRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Start activity with intent(will be the decider for the fragment to load)
                Intent i = new Intent(getActivity(), MapsActivity.class);
                i.putExtra("FragmentKey", 1);
                startActivity(i);

				/*//Fragment settings
				FragmentManager fm = getActivity().getSupportFragmentManager();
				FragmentTransaction fragmentTransaction = fm.beginTransaction();
				if (currentFragment == null) {
				} else {
					fragmentTransaction.remove(currentFragment);
				}
				currentFragment = new ManualRoute();
				fragmentTransaction.replace(R.id.runFragment, currentFragment);
				fragmentTransaction.addToBackStack(null);
				Log.i("BackStackCount", fm.getBackStackEntryCount() + "");
				fragmentTransaction.commit();*/
            }
        });


        ////TESTING:   SEND INTENT TO MapsActivity
        //Intent i = new Intent(getActivity(), MapsActivity.class);
        //startActivity(i);

        //TESTING//  //On btnDeletePrefs click
        btnDeletePrefs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                //Delete sharedPreferences
                sharedPreferences.edit().clear().apply();
                Toast.makeText(getActivity(), "SharedPreference has been deleted", Toast.LENGTH_LONG).show();
            }
        });


    }


}
