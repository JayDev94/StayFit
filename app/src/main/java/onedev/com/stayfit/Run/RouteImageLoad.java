package onedev.com.stayfit.Run;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;

/**
 * Created by Juan on 6/17/2016.
 */
public class RouteImageLoad {


    public void showImage(Context context, String filename, ImageView img){
        try {
            Bitmap bitmap = null;
            //Import from internal file path

            File myDirectory = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "StayFitSnapshots");
            String mPath = myDirectory+"/"+filename;

            File myFile = new File(mPath);

            Log.i("TestingFUCK1", "Test" + mPath);

            FileInputStream fi = new FileInputStream (myFile);  // 2nd line
            Log.i("TestingFUCK2", "Test" + "");

            bitmap = BitmapFactory.decodeStream(fi);


            //If the file exists, set the ImageView with it
            if(bitmap!=null)
                img.setImageBitmap(bitmap);
        } catch (Exception ex) {
            Log.i("getThumbnail()", ex.getMessage());
        }
    }


}
