package onedev.com.stayfit.Run;

import onedev.com.stayfit.R;
import onedev.com.stayfit.RunningData.RouteData;
import onedev.com.stayfit.RunningData.RouteStatsData;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class ManualRoute extends Fragment implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener,
        GoogleMap.OnCameraChangeListener, GoogleMap.OnMyLocationButtonClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationSource.OnLocationChangedListener, GoogleMap.SnapshotReadyCallback {

    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    // Keys for storing activity state in the Bundle.
    protected final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting-location-updates-key";
    protected final static String LOCATION_KEY = "location-key";
    protected final static String LAST_UPDATED_TIME_STRING_KEY = "last-updated-time-string-key";
    /**
     * Request code for location permission request.
     *
     * @see #onRequestPermissionsResult(int, String[], int[])
     */
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    /**
     * Flag indicating whether a requested permission has been denied after returning in
     * {@link #onRequestPermissionsResult(int, String[], int[])}.
     */

    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    protected Boolean mRequestingLocationUpdates;

    //current location;
    Location myLocation;
    LatLng currentLocation;

    private GoogleMap mMap;

    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    public static final String TAG = MapsActivity.class.getSimpleName();

    //Location list and settings var
    ArrayList<LatLng> latlongList;
    double previousLatitude;
    double previousLongitude;
    float[] results;
    float totalResults = 0;
    PolylineOptions userPolyOptions;
    PolylineOptions manualPolyOptions;
    ArrayList<Polyline> manualLineList;
    Polyline manualLine;
    int polylineCount = 0;

    //UI var
    Button btnBack;
    Button btnReset;
    TextView distanceText;


    //TESTING SAVING AND LOADING VAR
    ArrayList<String> loadedLatLangList;
    ArrayList<String> loadedDistances;
    ArrayAdapter<String> adapter;
    ListView routeListView;
    Button btnSave;
    Button btnLoad;
    RouteData routeDB;
    String strLoadRt = "";
    String strLoadDist = "";
    int listPosition = 0;
    boolean loadRoute;

    ListView listViewRoutes;
    ArrayList<String> testArrayList;
    //Data recieved from runfragment which was updated by loadroute fragment
    String data;


    //Fragment
    Fragment currentFragment = null;
    SupportMapFragment mapFragment;

    RouteStatsData routeStatsData;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        return inflater.inflate(R.layout.map_manual_route, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        //Route Data table initialization
        routeDB = new RouteData(getActivity());
        routeStatsData = new RouteStatsData(getActivity());
        //TEST----loaded routes list
        loadedLatLangList = new ArrayList<String>();
        loadedDistances = new ArrayList<String>();

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.location_map);
        mapFragment.getMapAsync(this);

        //Start gps
        mGoogleApiClient.connect();

        distanceText = (TextView) view.findViewById(R.id.viewMDistance);
        btnBack = (Button) view.findViewById(R.id.btnMBack);
        btnReset = (Button) view.findViewById(R.id.btnMReset);
        btnSave = (Button) view.findViewById(R.id.btnTestSave);
        btnLoad = (Button) view.findViewById(R.id.btnTestLoad);
        listViewRoutes = (ListView) view.findViewById(R.id.manualListView);

        //Initialize lists
        latlongList = new ArrayList();
        manualLineList = new ArrayList<Polyline>();
        testArrayList = new ArrayList<String>();
        //Retrieve data
        retrieveData();

        //ListView settings
        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, testArrayList);
        listViewRoutes.setAdapter(adapter);

        //Load calculations if any
        if (!(latlongList.isEmpty())) {
            Toast.makeText(getActivity(), "testing latlang existence", Toast.LENGTH_SHORT).show();

        }

        //Back button
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        //Reset button
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                latlongList.clear();
                polylineCount = 0;
                manualLineList.clear();
                distanceText.setText("0.0");
                routeDB.deleteTable();
                routeStatsData.deleteTable();
            }
        });

        //Save button
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Save the date into ROUTES_TABLE
                String totalDist = "";
                String strLatLongs = "";

                //Convert into miles and round .10
                MyTools myTools = new MyTools();

                double meters = (double) totalResults;

                // Log.i("Testing123", roundedMiles + "");
                totalDist = meters + "";

                for (int i = 0; i < latlongList.size(); i++) {
                    strLatLongs = strLatLongs + " " + latlongList.get(i).latitude + "," + latlongList.get(i).longitude + " ";
                }
                //Round data before insert into data                    roundedDist = Math.round(
                //Double rounded = Math.round(totalResults * 100.0) / 100.0;

                //Save data  - Save data in mili int format
                //routeDB.insertData(strLatLongs, totalDist);
                Toast.makeText(getActivity(), "Route Saved", Toast.LENGTH_SHORT).show();

                //Clear for use
                loadedDistances.clear();
                //Retrieve data
                retrieveData();


                //Save a snapshot of the route
                GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
                    Bitmap bitmap;

                    //Save the screenshot in the location of the the last distance +1
                    @Override
                    public void onSnapshotReady(Bitmap snapshot) {
                        bitmap = snapshot;
                        try {
                            //String filePath = Environment.DIRECTORY_DCIM;
                            //File dir=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                            //File file = new File(dir, "mapTestImage1.png");
                            //file.createNewFile();

                            //Save image to internal storage
                            FileOutputStream out = getContext().openFileOutput(loadedDistances.get(loadedDistances.size() - 1),
                                    Context.MODE_PRIVATE);

                            //Scale down the bitmap size before compressing and saving
                            Bitmap scaledBitmap = Bitmap.createScaledBitmap(snapshot, 300, 300, false);
                            scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                            out.close();

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.i("SnapshotError", "Cannot save snapshot");
                        }
                    }
                };
                mMap.snapshot(callback);

            }
        });
        //Load button
        btnLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Fragment settings
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                if (currentFragment == null) {
                } else {
                    fragmentTransaction.remove(currentFragment);
                }
                currentFragment = new SelectRoute();


            }
        });


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapClickListener(this);
        mMap.setOnMapLongClickListener(this);
        mMap.setOnCameraChangeListener(this);
        mMap.setOnMyLocationButtonClickListener(this);
        enableMyLocation();

        //Connect google API
        mGoogleApiClient.connect();


    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(getActivity(), "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    @Override
    public void onMapLongClick(LatLng point) {

    }

    @Override
    public void onCameraChange(final CameraPosition position) {

    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "Location services connected.");
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Location services suspended. Please reconnect.");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResume() {
        super.onResume();
        //Connect the google api
        mGoogleApiClient.connect();

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Stores activity data in the Bundle.
     */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY, mRequestingLocationUpdates);
        savedInstanceState.putParcelable(LOCATION_KEY, myLocation);
        //savedInstanceState.putString(LAST_UPDATED_TIME_STRING_KEY, mLastUpdateTime);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onMapClick(LatLng point) {
        //Add the first point the the list
        LatLng lt = new LatLng(point.latitude, point.longitude);
        latlongList.add(lt);

        Log.i("ClickedPointMap", point.latitude + " ," + point.longitude);

        //Caclulate the distance
        if (!(latlongList.isEmpty())) {
            if (latlongList.size() < 2) {
                //The arraylist only holds one value, use that for the first value
                previousLatitude = latlongList.get(0).latitude;
                previousLongitude = latlongList.get(0).longitude;
            } else {
                for (int i = 0; i < latlongList.size(); ++i) {
                    //Initialize the results float to retrieve the distance between points
                    results = new float[1];
                    //New location object to hold the current location
                    Location location = new Location("");
                    location.setLatitude(point.latitude);
                    location.setLongitude(point.longitude);
                    //Calculate the distance between the previous point and the current
                    Location.distanceBetween(previousLatitude, previousLongitude, point.latitude, point.longitude, results);
                    //Update the total distance
                    totalResults = totalResults + results[0];
                    // Modify the previous ones with the ones just used
                    previousLatitude = point.latitude;
                    previousLongitude = point.longitude;

                    //set the TextView with the total distance
                    //After conversion to miles
                    double dist = 0;
                    double roundedDist;
                    dist = (double) totalResults * 0.00062137;
                    roundedDist = Math.round(dist * 100.0) / 100.0;
                    distanceText.setText("" + roundedDist + " mi");

                }
            }
        }
        //Create the polyline from latlongList arraylist and add it to manualLine for edit
        Log.i("TestingDistance", "Distance: " + totalResults);
        manualPolyOptions = new PolylineOptions();
        manualPolyOptions.addAll(latlongList);
        manualPolyOptions.color(Color.RED);
        manualLine = mMap.addPolyline(manualPolyOptions);
        //Add the line to the polylinelist
        manualLineList.add(manualLine);
        polylineCount++;

        //TESTING FOR RUNROUTE::::::TESTING::: TESTING:::CREATE A CIRCLE FOR EVERY LOCATION
        //Circle Options
        /*for(int x = 0; x < latlongList.size(); x++) {
            Circle circle = mMap.addCircle(new CircleOptions()
                    .center(latlongList.get(x))
                    .radius(10)
                    .strokeWidth(5)
                    .strokeColor(Color.BLUE)
                    .fillColor(Color.parseColor("#26006CFF")));
            if(x < 2){
                break;
            }*/
    }


    //Enable the the my location
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission((AppCompatActivity) getActivity(), LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }


    // Retrieve data from ROUTES_TABLE into a string
    public void retrieveData() {
        Cursor cursor;
        try {
            cursor = routeDB.getAllData();
            // Check for data
            if (cursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();

            } else {
                while (cursor.moveToNext()) {
                    // Get data from index
                    //Routes
                    loadedLatLangList.add(cursor.getString(1));
                    //Distances
                    loadedDistances.add(cursor.getString(2));
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "No Data Loaded", Toast.LENGTH_SHORT).show();
        }
    }

    //Calculate the distance and draw on map
    public void calculationsLoaded() {
        //Caclulate the distance
        if (!(latlongList.isEmpty())) {
            if (latlongList.size() < 2) {
                //The arraylist only holds one value, use that for the first value
                previousLatitude = latlongList.get(0).latitude;
                previousLongitude = latlongList.get(0).longitude;
            } else {
                for (int i = 0; i < latlongList.size(); ++i) {
                    //Initialize the results float to retrieve the distance between points
                    results = new float[1];
                    //New location object to hold the current location
                    Location location = new Location("");
                    location.setLatitude(latlongList.get(i).latitude);
                    location.setLongitude(latlongList.get(i).longitude);
                    //Calculate the distance between the previous point and the current
                    Location.distanceBetween(previousLatitude, previousLongitude, location.getLatitude(), location.getLongitude(), results);
                    //Update the total distance
                    totalResults = totalResults + results[0];
                    // Modify the previous ones with the ones just used
                    previousLatitude = location.getLatitude();
                    previousLongitude = location.getLongitude();

                    //set the TextView with the total distance
                    //After conversion to miles
                    double dist = 0;
                    double roundedDist;
                    dist = (double) totalResults * 0.00062137;
                    roundedDist = Math.round(dist * 100.0) / 100.0;
                    distanceText.setText("" + roundedDist + " mi");
                }
            }
        }
        //Create the polyline from latlongList arraylist and add it to manualLine for edit
        Log.i("TestingDistance", "Distance: " + totalResults);
        manualPolyOptions = new PolylineOptions();
        manualPolyOptions.addAll(latlongList);
        manualPolyOptions.color(Color.RED);
        manualLine = mMap.addPolyline(manualPolyOptions);
        //Add the line to the polylinelist
        manualLineList.add(manualLine);
        polylineCount++;

    }

    //String to latlang conversions
    public void convertStringToLatLang(int position) {

        String strSp = loadedLatLangList.get(position);
        String[] splited = strSp.split("\\s+");
        //TESTING SPLITTED ARRAY
        for (int x = 0; x < splited.length; x++) {
            Log.i("SplitedArray", splited[x]);
        }
        //TESTING SPLITTED ARRAY BY COMMA
        String strTest1 = splited[1];
        String[] strTest = strTest1.split(",");
        String show = strTest[0];
        Log.i("SplittComma", " SPLIT" + show);

        // List<String> splitedList = new ArrayList<String>();
        //Clear the latlonglist and drawing lists for use
        latlongList.clear();
        manualLineList.clear();
        for (int i = 1; i < splited.length; i++) {
            String[] strLatLangs = splited[i].split(",");
            Double lat = Double.parseDouble(strLatLangs[0]);
            Double lo = Double.parseDouble(strLatLangs[1]);
            LatLng location = new LatLng(lat, lo);
            latlongList.add(location);
        }
    }


    @Override
    public void onSnapshotReady(Bitmap bitmap) {

    }


}
