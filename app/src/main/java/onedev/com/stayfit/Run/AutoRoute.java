package onedev.com.stayfit.Run;

import onedev.com.stayfit.R;
import onedev.com.stayfit.RunningData.RouteData;
import onedev.com.stayfit.RunningData.RouteStatsData;

import android.Manifest;
import android.app.ActionBar;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;


public class AutoRoute extends Fragment implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener,
        GoogleMap.OnCameraChangeListener, GoogleMap.OnMyLocationButtonClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        AutoRouteService.Callbacks {

    //AutoRouteService
    AutoRouteService autoRouteService;
    Intent serviceIntent;


    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    // Keys for storing activity state in the Bundle.
    protected final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting-location-updates-key";
    protected final static String LOCATION_KEY = "location-key";
    protected final static String LAST_UPDATED_TIME_STRING_KEY = "last-updated-time-string-key";
    /**
     * Request code for location permission request.
     *
     * @see #onRequestPermissionsResult(int, String[], int[])
     */
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    /**
     * Flag indicating whether a requested permission has been denied after returning in
     * {@link #onRequestPermissionsResult(int, String[], int[])}.
     */
    private boolean mPermissionDenied = false;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    public static final String TAG = MapsActivity.class.getSimpleName();

    //PolyOptions content
    private static final LatLng PERTH = new LatLng(-31.95285, 115.85734);
    private static final LatLng MELBOURNE = new LatLng(-37.81319, 144.96298);
    private static final LatLng SYDNEY = new LatLng(-33.87365, 151.20689);
    private static final LatLng ADELAIDE = new LatLng(-34.92873, 138.59995);


    private Polyline mClickablePolyLine;
    private Polyline mMutablePolyline;

    //The lists of latlangs and locations
    ArrayList<LatLng> latlongList;
    ArrayList<Location> locationList;
    ArrayList<LatLng> testLocationList;
    double distanceInMeters = 0;
    double totalDistance = 0;

    //Widgets
    private GoogleMap mMap;
    TextView cameraText;
    TextView locationText;
    TextView distanceText;
    TextView caloriesText;
    Button btnContinuePause;
    Button btnPause;
    Button btnFinish;
    Button btnReset;
    Button btnStart;
    ListView listView;
    ArrayList<String> arraylist;
    ArrayAdapter<String> adapter;

    //Will hold the location: Lat and Long str first- parse to double
    String strLong = "", strLat = "";
    double dblLong = 0, dblLat = 0;

    //Location change
    boolean booleanTrack;
    double previousLatitude;
    double previousLongitude;
    float[] results;
    float totalResults = 0;
    double roundedDist = 0;

    //current location;
    Location myLocation;
    LatLng currentLocation;

    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    protected Boolean mRequestingLocationUpdates;

    PolylineOptions userPolyOptions;
    PolylineOptions manualPolyOptions;
    ArrayList<Polyline> manualLineList;
    ArrayList<Polyline> userLineList;
    Polyline manualLine;
    Polyline userLocationLine;
    int polylineCount = 0;
    int userLineCount = 0;

    //Toolbar
    private Toolbar toolbar;

    //Location request --TESTING-_
    LocationManager locationManager;

    //Boolean for onMap events
    boolean booleanClickMap;

    //Timer vars
    long startTime = 0L;
    long timeMilli = 0L;
    long timeBuffer = 0L;
    long timeUpdate = 0L;
    TextView timeText;
    private Handler timeHandler = new Handler();
    String strTime = "";
    String sec = "";
    String mili = "";
    TextView textMapTime;

    //Calorie vars
    int age = 21;
    int weight = 79;
    double calMinutes = 0.0;
    String strSeconds = "";
    String strMinutes = "";
    double myCals = 0;

    boolean pause;

    //Create line testing
    Button btnCreate;

    //Route data table
    RouteData routeData;
    private Location lastLocation;
    String totalDist = "";


    AutoRouteStatsInterface callBack;

    //Distance counter for each miles completed and miles string holder
    double dblCompletedMile = 0;
    String strMiles = " ";
    String strMilesTimes = " ";
    int mileCounter = 1;

    MyTools myTools;

    //Route setsdata class
    RouteStatsData routeStatsData;

    //Route retrieval list
    ArrayList<String> routeDataList;
    ArrayList<String> routeDistances;
    ArrayList<Integer> dataRouteTimesList;


    //Saves a time stamp for start and end using this format
    SimpleDateFormat SDF = new SimpleDateFormat("HH:mm:ss");
    String strStartTime = "";
    String strEndTime = "";


    AutoRouteReceiver autoRouteReceiver;
    IntentFilter intentFilter;


    //Broadcast contents
    public final String BROADCAST_LATLNG = "RouteLatLngBroadcast";
    public final String BROADCAST_ROUTE_INFO = "RouteInfoBroadcast";
    public final String BROADCAST_TIME = "RouteTimeBroadcast";
    public final String BROADCAST_COMPLETION = "RouteCompletionBroadcast";
    public final String BROADCAST_STOP_SERVICE = "StopRouteService";

    //Boolean for serviceConnection
    boolean serviceIsBound = false;

    ArrayList<String> routeBroadcastedInfoList;


    ServiceConnection copyServiceConnection;


    public AutoRoute() {
    }


    /*_________________________________________________________________________________________________________*/
    /*_________________________________________________________________________________________________________*/
    /*_________________________________________________________________________________________________________*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        return inflater.inflate(R.layout.map_auto_route, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        foreGroundCheck();

        //Intent filter setup
        intentFilter = new IntentFilter();
        intentFilter.addAction(BROADCAST_LATLNG);
        intentFilter.addAction(BROADCAST_ROUTE_INFO);
        intentFilter.addAction(BROADCAST_TIME);
        intentFilter.addAction(BROADCAST_COMPLETION);
        intentFilter.addAction(BROADCAST_STOP_SERVICE);

        routeBroadcastedInfoList = new ArrayList<>();


        routeDataList = new ArrayList<>();

        myTools = new MyTools();
        //Initiate RouteData and RouteStatsData
        routeData = new RouteData(getActivity());
        routeStatsData = new RouteStatsData(getActivity());
        //Set MapClicked events to false
        booleanClickMap = false;

        //Initialize location manager with location services
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        //Location updates PolyOptions
        userPolyOptions = new PolylineOptions();

        //Tracker boolean initialization
        booleanTrack = false;


        locationList = new ArrayList<Location>();
        latlongList = new ArrayList();
        manualLineList = new ArrayList<Polyline>();
        userLineList = new ArrayList<Polyline>();
        routeDistances = new ArrayList<>();

        //Testing locations list
        testLocationList = new ArrayList<LatLng>();
        testLocationList.add(PERTH);
        testLocationList.add(MELBOURNE);
        testLocationList.add(SYDNEY);
        testLocationList.add(ADELAIDE);

        mRequestingLocationUpdates = false;

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //UI vars
        distanceText = (TextView) view.findViewById(R.id.textDistance);
        btnContinuePause = (Button) view.findViewById(R.id.btnContinuePause);
        btnFinish = (Button) view.findViewById(R.id.btnFinish);
        btnReset = (Button) view.findViewById(R.id.btnResetRR);
        btnStart = (Button) view.findViewById(R.id.btnStart);
        textMapTime = (TextView) view.findViewById(R.id.mapTime);
        caloriesText = (TextView) view.findViewById(R.id.textCalories);

        //Retrieve route data
        retrieveData();
        for (int i = 0; i < routeDataList.size(); i++) {
            Log.i("RouteListIndex", routeDataList.get(i) + "");
        }
        Log.i("RouteListSize", routeDataList.size() + " ");

        //Start gps
        mGoogleApiClient.connect();

        //Set the continue settings to off


        //Start button
        String test = "This is a service test for AutoRoute";
        serviceIntent = new Intent(getActivity(), AutoRouteService.class);
        serviceIntent.putExtra("Route", test);

        // getActivity().startService(serviceIntent);


      /*////NEW:   SET THIS UI TO DISPLAY A "RUN" BUTTON.  AFTER THE RUN BUTTON IS STARTED, A START
      BUTTON WILL WHICH WILL BE THE INITIATOR FOR THE RUN ROUTE FUNCTION-------this fixes the problems with the double
      press on the start button*/
        btnStart.setText("RUN A ROUTE");
        btnStart.setBackgroundColor(Color.BLACK);


        btnStart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().startService(serviceIntent);
                btnStart.setBackgroundColor(Color.WHITE);
                btnStart.setText("START");

                //TESTING SERVICE
                //Bind the service
                getActivity().bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);

                SharedPreferences startPrefs = getActivity().getSharedPreferences("FINISH", Context.MODE_PRIVATE);
                startPrefs.edit().putBoolean("DONE", false).commit();

                //Register the broadcast reciever
                if (autoRouteReceiver == null) autoRouteReceiver = new AutoRouteReceiver();
                getActivity().registerReceiver(autoRouteReceiver, intentFilter);


                //Connect the api and start updating location
                //onResume();
                if (!(autoRouteService == null)) {
                    autoRouteService.startLocationUpdates();
                    autoRouteService.startTimer();
                    autoRouteService.booleanTrack = true;
                    //Clear the latlng list
                    latlongList.clear();

                    //Used for database
                    strStartTime = SDF.format(Calendar.getInstance().getTime());
                    Log.i("StartStamp", strStartTime);

                    //Start Timer
                    //startTimer();
                    btnStart.setVisibility(View.GONE);
                }
            }
        });
        //Stop button
        btnContinuePause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (autoRouteService.pause == false) {
                    //Pause button shows next time
                    autoRouteService.pause = true;

                    Toast.makeText(getActivity(), "Tracking Paused", Toast.LENGTH_SHORT).show();
                    autoRouteService.stopLocationUpdates();
                    //Pause timer
                    btnContinuePause.setText("Continue");
                    btnContinuePause.setTextColor(Color.parseColor("#FF58C745"));
                    autoRouteService.pauseTimer();
                } else {
                    Toast.makeText(getActivity(), "Tracking Continued", Toast.LENGTH_SHORT).show();
                    autoRouteService.startLocationUpdates();
                    booleanTrack = true;
                    //Start Timer
                    autoRouteService.startTimer();
                    //Continue button shows next time
                    btnContinuePause.setText("Pause");
                    btnContinuePause.setTextColor(Color.parseColor("#ff5100"));

                    autoRouteService.pause = false;
                }

            }
        });
        //Reset button
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoRouteService.resetTracker();
                autoRouteService.stopLocationUpdates();
                //Reset timer
                autoRouteService.pauseTimer();
                autoRouteService.resetTimer();
            }
        });

        //Finish button
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoRouteService.stopLocationUpdates();
                autoRouteService.pauseTimer();
                strEndTime = SDF.format(Calendar.getInstance().getTime());
                Log.i("EndTimeStamp", strEndTime);

                new AlertDialog.Builder(getActivity())
                        .setTitle("Route Completion")
                        .setMessage("Are you done running?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                if (serviceIsBound) {
                                    Log.i("UnbindingService", "Service unbinding");
                                    getActivity().stopService(serviceIntent);
                                    getActivity().unbindService(serviceConnection);
                                    serviceIsBound = true;


                                    SharedPreferences finishPrefs = getActivity().getSharedPreferences("FINISH", Context.MODE_PRIVATE);
                                    finishPrefs.edit().putBoolean("DONE", true).commit();
                                }
                                //Request route completion from service
                                autoRouteService.completeRoute();


                                //Stop service
                                Log.i("ServiceStopped", "Service is stopped");
                                //autoRouteService.unbindedAndStop();
                                // unbindStopService();


                                // getActivity().stopService(new Intent(getActivity(), AutoRouteService.class));


                                //Add the remaining distance to the miles and milesTimes string
                                //Get the remainder by subtracting the whole number then overwrite that value by 1-value then round to .00
                                //If it is less than 1, use the current distance ('which dblCompletedMile has')

                                /*double value = 0;
                                if (dblCompletedMile < 1) {
                                    value = dblCompletedMile;
                                } else {
                                    value = dblCompletedMile - (mileCounter - 1);
                                    value = 1 - value;z
                                    value = Math.round(value * 100.00) / 100.00;
                                }
                                strMiles = strMiles + value + " ";
                                */
                                /*
                                strMilesTimes = strMilesTimes + strTime;
                                //Save data
                                String strRoute = "";
                                for (int i = 0; i < latlongList.size(); i++) {
                                    strRoute = strRoute + " " + latlongList.get(i).latitude + "," + latlongList.get(i).longitude + " ";
                                }


                                //JUST FOR TESTING PURPOSES------------------Add blank info if distance < 1
                                //if(roundedDist < 1){
                                //  strRoute = " 41.8781,-87.6298 " ;
                                //}
                                //--------------------------------------------

                                totalDist = " " + roundedDist + " ";
                                /*
                                routeData.insertData(strRoute, totalDist);
                                int routeDataInput = routeDataList.size();
                                //Save the miles, and mile times as well
                               routeStatsData.insertData(routeDataInput, strTime, totalDist, strMiles, strMilesTimes);
                                */
                                /*
                                DateFormat formatter = SimpleDateFormat.getDateInstance();
                                Date saveDate = new Date();
                                //String strDate = formatter.format(saveDate);
                                String strDate = String.valueOf(System.currentTimeMillis());

                                //Start time, End time, Calories burned//
                                String startTime = "", endTime = "";
                                startTime = strStartTime;
                                endTime = strEndTime;
                                Log.i("DataSentToStats", "   \nstrRoute: " +strRoute +"   \nstrTime: " + strTime+
                                        "   \ntotalDist: " +totalDist+ "   \nmyCals: " +  myCals + "   \nstrMiles: " + strMiles + "   \nstrMileTimes: " +  strMilesTimes +
                                        "   \nstrDate: " +strDate + "   \nstartTime: " +startTime + "   \nendTime: " + endTime);

                                callBack.autoRouteRan(strRoute, strTime, totalDist, myCals + "", strMiles, strMilesTimes, strDate, startTime, endTime);
                            **/

                            }
                        })
                        .setNegativeButton("Continue Running", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue
                                autoRouteService.startLocationUpdates();
                                autoRouteService.startTimer();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });


        //Setup ui from broadcast data
        if (routeBroadcastedInfoList.size() > 2) {
            timeText.setText(routeBroadcastedInfoList.get(0));
            caloriesText.setText(routeBroadcastedInfoList.get(1));
        }

    }

    //--TESTING-- location listener ///////CHANGED ------WENT HERE------//////////

    //Service connection settings
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder iBinderService) {
            Log.i("ServiceIsConnected", "Service is connected");
            serviceIsBound = true;

            //Cast IBinder and get service instance
            AutoRouteService.LocalBinder binder = (AutoRouteService.LocalBinder) iBinderService;
            autoRouteService = binder.getServiceInstance();
            autoRouteService.registerClient(getActivity().getParent());

            copyServiceConnection = serviceConnection;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i("ServiceDisconnected", "Service has been disconnected");
            serviceIsBound = false;

        }

    };

    public void unbindStopService() {
        Intent uIntent = new Intent(getActivity(), AutoRouteService.class);
        getActivity().stopService(uIntent);
        getActivity().unbindService(copyServiceConnection);

    }


    //*   -------------------------------------interface method to communicate with service-----------------------------------------
    @Override
    public void updateAutoRouteTracksData(double distance, double calories, String strMiles,
                                          ArrayList<LatLng> latLongList) {
        Log.i("LatLongListTe", latLongList.get(latLongList.size()) + "");
        Log.i("DistanceTest", distance + "");

        //Set the ui
        distanceText.setText(distance + "");
        caloriesText.setText(calories + "");


    }


    /**
     * Updates fields based on data stored in the bundle.
     *
     * @param savedInstanceState The activity state saved in the Bundle.
     */
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        Log.i(TAG, "Updating values from bundle");
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
            // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(REQUESTING_LOCATION_UPDATES_KEY)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        REQUESTING_LOCATION_UPDATES_KEY);
                //  setButtonsEnabledState();
            }

            // Update the value of mCurrentLocation from the Bundle and update the UI to show the
            // correct latitude and longitude.
            if (savedInstanceState.keySet().contains(LOCATION_KEY)) {
                // Since LOCATION_KEY was found in the Bundle, we can be sure that mCurrentLocation
                // is not null.
                myLocation = savedInstanceState.getParcelable(LOCATION_KEY);
            }

            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(LAST_UPDATED_TIME_STRING_KEY)) {
                //  mLastUpdateTime = savedInstanceState.getString(LAST_UPDATED_TIME_STRING_KEY);
            }
            //  updateUI();
        }
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapClickListener(this);
        mMap.setOnMapLongClickListener(this);
        mMap.setOnCameraChangeListener(this);
        mMap.setOnMyLocationButtonClickListener(this);
        enableMyLocation();

        //Connect google API
        mGoogleApiClient.connect();


    }

    //Enable the the my location
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission((AppCompatActivity) getActivity(), LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(getActivity(), "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }
        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }


    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getActivity().getSupportFragmentManager(), "dialog");
    }

    /*------COMPLETED------*/
    @Override
    public void onMapClick(LatLng point) {
        booleanClickMap = true;
        /*-----------TESTING DISTANCE CALCULATIONS------------*/
        if (booleanClickMap == true) {
            //Add the first point the the list
            LatLng lt = new LatLng(point.latitude, point.longitude);
            latlongList.add(lt);


            //Caclulate the distance
            if (!(latlongList.isEmpty())) {
                if (latlongList.size() < 2) {
                    //The arraylist only holds one value, use that for the first value
                    previousLatitude = latlongList.get(0).latitude;
                    previousLongitude = latlongList.get(0).longitude;
                } else {
                    for (int i = 0; i < latlongList.size(); ++i) {
                        //Initialize the results float to retrieve the distance between points
                        results = new float[1];
                        //New location object to hold the current location
                        Location location = new Location("");
                        location.setLatitude(point.latitude);
                        location.setLongitude(point.longitude);
                        //Calculate the distance between the previous point and the current
                        Location.distanceBetween(previousLatitude, previousLongitude, point.latitude, point.longitude, results);
                        //Update the total distance
                        totalResults = totalResults + results[0];
                        // Modify the previous ones with the ones just used
                        previousLatitude = point.latitude;
                        previousLongitude = point.longitude;
                        //set the textview with the total distance
                        //distanceText.setText("" + totalResults);

                        //Calorie Settings (TIME CONVERSTION TO MINUTES)
                        double timeCals = 0;
                        double dblSec = 0;
                        double dblMin = 0;
                        dblSec = Double.parseDouble(strSeconds);
                        dblMin = Double.parseDouble(strMinutes);
                        timeCals = ((dblMin * 60) + dblSec);
                        Log.i("totalTimeCals", "" + timeCals);
                        double myCals = 0;

                        myCals = calculateCalories(timeCals, totalResults);
                        Log.i("TestingClaories", "Calories: " + myCals);
                        myCals = calculateCalories(timeCals, totalResults);
                        myCals = Math.round(100.0 * myCals) / 100.0;
                        //Set the calories TextView
                        caloriesText.setText("" + myCals);

                    }
                }
            }
            //Create the polyline from latlongList arraylist and add it to manualLine for edit
            Log.i("TestingDistance", "Distance: " + totalResults);
            manualPolyOptions = new PolylineOptions();
            manualPolyOptions.addAll(latlongList);
            manualPolyOptions.color(Color.RED);
            manualLine = mMap.addPolyline(manualPolyOptions);
            //Add the line to the polylinelist
            manualLineList.add(manualLine);
            polylineCount++;


        }

    }

    @Override
    public void onMapLongClick(LatLng point) {

    }

    @Override
    public void onCameraChange(final CameraPosition position) {

    }

    /*
     *This method handles -code- when connected to the api
     * and when it has access to permissions
     * While the connection is available, the location will be updated and in turn, the camera
     */
    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "Location services connected.");
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        //Get the last known location from the network
        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        //Set the camera view to the user's location by default
        if (location == null) {
            // Wait...
        } else {
            LatLng firstLocation = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(firstLocation, 16));
        }

    }


    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Location services suspended. Please reconnect.");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResume() {
        super.onResume();


        Boolean completion = false;


        SharedPreferences prefs = getActivity().getSharedPreferences("FINISH", Context.MODE_PRIVATE);
        completion = prefs.getBoolean("DONE", false);
        if (completion) {
            Log.i("BooleanTest", "TRUE");
        } else {
            Log.i("BoleanTest", "FALSE");

        }

        //Register the broadcast reciever
        if (autoRouteReceiver == null) autoRouteReceiver = new AutoRouteReceiver();
        getActivity().registerReceiver(autoRouteReceiver, intentFilter);
        //Connect the google api
        mGoogleApiClient.connect();

    }

    @Override
    public void onPause() {
        super.onPause();

        //Check weather the service is bound or not & if the receiver is registered already
        if (serviceIsBound == false) {
            if (autoRouteReceiver != null) {
                Log.i("UnregisteringRec", "Reciever is being unregisterd");
                getActivity().unregisterReceiver(autoRouteReceiver);
            }
        } else {
            if (autoRouteReceiver != null) getActivity().unregisterReceiver(autoRouteReceiver);
        }


        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    //Will hold the location
    private void handleNewLocation(Location location) {
        Log.d(TAG, location.toString());
        //Get the latitude and longitute then convert it to double for use
        strLat = "" + location.getLatitude();
        strLong = "" + location.getLongitude();
        dblLat = Double.parseDouble(strLat);
        dblLong = Double.parseDouble(strLong);

        if (dblLat == 0 || dblLong == 0) {

        } else {
            currentLocation = new LatLng(dblLat, dblLong);
        }

        //Get the current cord. and add it to a LatLang to be used for the camera's view position
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 2));


        //This disables the user control of the camera
        //mMap.getUiSettings().setScrollGesturesEnabled(false);


        Log.i("Location-CORD.", "Latitude: " + strLat + "  Longitude: " + strLong);
    }


    /**
     * Stores activity data in the Bundle.
     */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY, mRequestingLocationUpdates);
        savedInstanceState.putParcelable(LOCATION_KEY, myLocation);
        //savedInstanceState.putString(LAST_UPDATED_TIME_STRING_KEY, mLastUpdateTime);
        super.onSaveInstanceState(savedInstanceState);
    }

    //Law of cosines algorithm
    public double CalculateDistance(double lat1, double lat2, double lon1, double lon2) {
        double r = 6371008;
        double d;
        /*(double dLat, dLon;
        dLat = (lat2-lat1);dLon = (lon2-lon1);double a = (Math.sin(dLat/2)* Math.sin(dLat/2)) + (Math.cos(lat1)*Math.cos(lat2)
        *((Math.sin(dLon/2)*Math.sin(dLon/2))));double c = 2*(Math.atan2(Math.sqrt(a), Math.sqrt(1-a))); d = r * c;
        */
        double latRad1, latRad2, lon12;
        double Rad = (Math.PI / 180);
        latRad1 = Math.toRadians(lat1);
        latRad2 = Math.toRadians(lat2);
        lon12 = Math.toRadians(lon2 - lon1);
        d = Math.acos(Math.sin(latRad1) * Math.sin(latRad2) + Math.cos(latRad1) * Math.cos(latRad2) * Math.cos(lon12)) * r;

        //Return distance in m
        return d;
    }

    //Reset the tracker vars and UI elements
    public void resetTracker() {
        //set the distance text to empty string
        distanceText.setText("");
        //delete the list of locations
        latlongList.clear();
        //stop the tracker
        booleanTrack = false;

        //remove polylines
        if (!(manualLineList.isEmpty())) {
            for (int i = 0; i < manualLineList.size(); i++) {
                manualLineList.get(i).remove();
            }
        }
        //Clear the polyline list
        manualLineList.clear();
        //Set the polyline count to 0
        polylineCount = 0;
        if (!(userLineList.isEmpty())) {
            for (int i = 0; i < userLineList.size(); i++) {
                userLineList.get(i).remove();
            }
        }
        //Clear the polyline list
        userLineList.clear();
        //Set the polyline count to 0
        userLineCount = 0;
        //Reset the total distance
        totalResults = 0;

        //Reset calories text
        caloriesText.setText("Cals: ");

        //Show the start button
        btnStart.setVisibility(View.VISIBLE);
        //Continue button shows next time
        btnContinuePause.setText("Pause");
        btnContinuePause.setTextColor(Color.parseColor("#ff5100"));

        pause = false;

    }

    public void goBack() {
        //Remove the last polyline added
        manualLineList.get(polylineCount - 1).remove();
        manualLineList.remove(polylineCount - 1);
        polylineCount--;
        //manualLineList.remove(manualLineList.size()-1);


        //Remove the last location added
        latlongList.remove(latlongList.size() - 1);
        arraylist.remove(arraylist.size() - 1);
        adapter.notifyDataSetChanged();

        // manualLineList.get(manualLineList.size()-1).remove();

    }


    //Popup with statistics
    public void showPopup(View view) {
        View popupView = getActivity().getLayoutInflater().inflate(R.layout.map_popup, null);
        PopupWindow popupWindow = new PopupWindow(popupView, ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

        TextView distance = (TextView) popupView.findViewById(R.id.testLabelDistance);
        TextView calories = (TextView) popupView.findViewById(R.id.testLabelCalories);
        timeText = (TextView) popupView.findViewById(R.id.textTime);

        //Set timeText with updated time
        timeText.setText(strTime);

        double dist = 0;
        double roundedDist;
        dist = (double) totalResults * .000621;
        roundedDist = Math.round(dist * 100.0) / 100.0;
        distance.setText("" + roundedDist);

        // If the PopupWindow should be focusable
        popupWindow.setFocusable(true);
        // If you need the PopupWindow to dismiss when when touched outside
        popupWindow.setBackgroundDrawable(new ColorDrawable());
        int location[] = new int[2];
        // Get the View's(the one that was clicked in the Fragment) location
        view.getLocationOnScreen(location);
        // Using location, the Pop-upWindow will be displayed right under
        // anchorView
        popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, location[0], location[1] + view.getHeight());
    }


    /**
     * Calorie calculations are done using the ACSM formula for running
     * ---[VO2 = (.2*metersMin) + 3.5]---
     * the kcals are calculated using this formula
     * ---[kcal/min = 5((VO2*BMkg)/1000]---
     */
    public double calculateCalories(double time, float distance) {
        double vO2 = 0;
        double metersMin = 0;
        double kCal = 0;
        double totalKCal = 0;
        double timeVar = 0;
        double kcalVar = 0;

        //Conversions
        timeVar = time / 60;
        metersMin = distance / timeVar;
        //VO2 calculations
        vO2 = (.2 * metersMin) + 3.5;
        //Kcal calculations
        kcalVar = (vO2 * weight) / 1000;
        kCal = 5 * kcalVar;
        totalKCal = timeVar * kCal;

        return totalKCal;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            callBack = (AutoRouteStatsInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement AutoRouteStatsInterface");
        }
    }


    //Interface for statistics
    public interface AutoRouteStatsInterface {
        void autoRouteRan(String routeData, String time, String distance, String calories, String miles, String milesTimes, String dateRan, String startTime,
                          String endTime);
    }

    //Retrieve route data to aquire the positions of the routs and then add this one into it.
    public void retrieveData() {
        Cursor cursor;
        try {
            cursor = routeData.getAllData();
            // Check for data
            if (cursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();

            } else {
                while (cursor.moveToNext()) {
                    // Get data from index
                    routeDataList.add(cursor.getString(1));
                    Log.i("RouteDataString", cursor.getString(1));
                    routeDistances.add(cursor.getString(2));

                    dataRouteTimesList.add(Integer.parseInt(cursor.getString(3)));
                }

                for (int t = 0; t < routeDataList.size(); t++) {
                    Log.i("RouteDataDistances", routeDataList.get(t));
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "No Data Loaded", Toast.LENGTH_SHORT).show();
        }
    }


    /*-------------------------CHECK IF APP IS IN FOREGROUND OR NOT--------------------------------------------*/
    class ForegroundCheckTask extends AsyncTask<Context, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Context... params) {
            final Context context = params[0].getApplicationContext();
            return isAppOnForeground(context);
        }

        private boolean isAppOnForeground(Context context) {
            ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
            if (appProcesses == null) {
                return false;
            }
            final String packageName = context.getPackageName();
            for (RunningAppProcessInfo appProcess : appProcesses) {
                if (appProcess.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                    return true;
                }
            }
            return false;
        }
    }

    //Check if the app is running in the background or not
    public void foreGroundCheck() {
        // Use like this:
        boolean foreground = false;
        try {
            foreground = new ForegroundCheckTask().execute(getActivity().getApplicationContext()).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        Log.i("ForeGroundCheck", foreground + "");
    }


    //---------------------------------
    /*public void updateUI(String time, String calories){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView caloriesText = (TextView) getView().findViewById(R.id.)
            }
        });
    }*/

    //-----------------------------------------------------------------------------------------------------
    /*
     ALLOWS COMMUNICATION WITH THE AUTOROUTE SERVICE
      */
    public class AutoRouteReceiver extends BroadcastReceiver {
        final String PLAY_ACTION = "PLAY_ACTION";
        final String PAUSE_ACTION = "PAUSE_ACTION";

        @Override
        public void onReceive(Context context, Intent intent) {
            String strAction = "";
            strAction = intent.getAction();
            //Handle action retrieved
            if (strAction.equals(PLAY_ACTION)) {
                Log.i("PLAY_ACTION", "Play action retrieved");
            }
            if (strAction.equals(PAUSE_ACTION)) {
                Log.i("PAUSE_ACTION", "Pause action retrieved");
                Intent stopIntent = new Intent(getActivity().getApplicationContext(), AutoRoute.class);
                getActivity().stopService(stopIntent);
            }

            //Test broadcast.... //WORKED/  Continue creating various bundles corresponding the the tracking functions
            // then update the ui using that transfered data.
            if (strAction.equals(BROADCAST_LATLNG)) {
                Bundle latlngBundle = intent.getParcelableExtra("RouteLatLngBroadcast");
                ArrayList<LatLng> llList = latlngBundle.getParcelableArrayList("latlng");

                for (int i = 0; i < llList.size(); i++) {
                    Log.i("BundleRouteData", llList.get(i).latitude + ", " + llList.get(i).longitude);
                }

                //Retrieve the latlng from the service and set the polyline
                //Set the ui
                latlongList.clear();
                latlongList.addAll(llList);
                //Create the polyline from latlongList arraylist
                userPolyOptions = new PolylineOptions();
                userPolyOptions.addAll(latlongList);
                userPolyOptions.color(Color.GREEN);
                userLocationLine = mMap.addPolyline(userPolyOptions);
                //Add the line to the polylinelist
                userLineList.add(userLocationLine);
            }


            //Retrieve route data info contents
            if (strAction.equals(BROADCAST_ROUTE_INFO)) {
                Bundle infoBundle = intent.getParcelableExtra("RouteInfoBundle");
                ArrayList<String> extraInfoList = infoBundle.getStringArrayList("RouteInfoArg");
                routeBroadcastedInfoList.addAll(extraInfoList);

                for (int i = 0; i < extraInfoList.size(); i++) {
                    Log.i("TestingBroadcast", extraInfoList.get(i));
                }

                if (extraInfoList != null) {
                    distanceText.setText(extraInfoList.get(0));
                    caloriesText.setText(extraInfoList.get(1));
                }

                //Retrive route latlng from bundle
                //Bundle bundle = intent.getStringArrayListExtra("RouteContentBundle");
            }

            //Retrieve route time
            if (strAction.equals(BROADCAST_TIME)) {
                Bundle timeBundle = intent.getParcelableExtra("RouteTimeBundle");
                String strRouteTime = timeBundle.getString("RouteTimeArg");

                //Set the text view to the retrieved time
                textMapTime.setText(strRouteTime);
            }
            //Retrieve completion request
            if (strAction.equals(BROADCAST_COMPLETION)) {
                Log.i("BroadcastRecievedTest", "testing");
                Bundle completionBundele = intent.getParcelableExtra("RouteCompletionBundle");
                ArrayList<String> completionList = completionBundele.getStringArrayList("RouteCompletionList");
                /*    0 --> 6
                [strRoute - strTime - totalDist - myCals - strMiles - strMilesTimes - strDate -]
                 */
                Log.i("RouteToStats", completionList.get(0) + "\n" + completionList.get(1) + "\n" + completionList.get(2) + "\n" + completionList.get(3) + "\n" +
                        completionList.get(4) + "\n" + completionList.get(5) + "\n" + completionList.get(6) + "\n" + startTime + "" + "\n" + strEndTime + "");


                Log.i("TestsRunningCallback", BROADCAST_COMPLETION + "");
                callBack.autoRouteRan(completionList.get(0), completionList.get(1), completionList.get(2), completionList.get(3),
                        completionList.get(4), completionList.get(5), completionList.get(6), startTime + "", strEndTime + "");


                // callBack.autoRouteRan(strRoute, strTime, totalDist, myCals + "", strMiles, strMilesTimes, strDate, startTime, endTime);
            }

            //Stop service request
            if (strAction.equals(BROADCAST_STOP_SERVICE)) {
                Log.i("STOP_SERVICE", "stop service initiated");
                Intent stopIntent = new Intent(getActivity(), AutoRoute.class);
                //etActivity().stopService(serviceIntent);

                //getActivity().unbindService(serviceConnection);


            }
        }


    }

}
