package onedev.com.stayfit.Run;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import onedev.com.stayfit.R;
import onedev.com.stayfit.RunningData.RouteData;

/**
 * Created by Juan on 6/10/2016.
 */
/**
 * This class serves as the Route Selection part of RUN.
 * Holds the Route selection UI.
 * - After route selection, RunRoute fragment will load and the tracking will begin.
 * - If route is long pressed, Edit and Remove options will appear.
 *  -- If edit is pressed, ManualRoute will load with the selected route for edit.
 *  -- If Remove is pressed, dialog appears, if yes, route will be deleted from DB.
 * - Bottom + button creates a new route, will load ManualRoute for creation.
 */
public class SelectRoute extends Fragment {
    //Route database
    RouteData routeData;

    private ListView listViewRoutes;
    private Button btnCreate;
    ArrayAdapter<String> adapter;
    ArrayList<String>routesList;
    ArrayList<String>routesListMiles;
    ArrayList<String>datesList;

    //Interface var
    SelectRouteInterfaceListener cCallback;

    //Custom adapter
    RouteListAdapter routeAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        return inflater.inflate(R.layout.run_select_route, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        //if the view was hidden in RunRoute fragment, show it
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
        //Enable options menu
        setHasOptionsMenu(true);
        //Initialize database class
        routeData = new RouteData(getActivity());



        listViewRoutes = (ListView) view.findViewById(R.id.viewRouteList);
        btnCreate = (Button) view.findViewById(R.id.btnCreateRoute);
        //Initialize arraylist
        routesList = new ArrayList<String>();
        routesListMiles = new ArrayList<String>();
        datesList = new ArrayList<String>();
        retrieveData();


        //Initialize costume routeAdapter (context, routes, dates)
        routeAdapter = new RouteListAdapter(getActivity(), routesList, datesList);
        listViewRoutes.setAdapter(routeAdapter);

        //ListView settings
        //adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, routesList);
        //listViewRoutes.setAdapter(adapter);

        //On item clicked for listview
        listViewRoutes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Close the DialogFragment
                //Update the data and send to runfragment which then sends to manualroute fragment

                //Pass the position through the interface to main activity then to runroute fragment
                cCallback.onSelectRoutePressed(position);
            }
        });

        //On create button clicked
        btnCreate.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                //MapActivity will replace this fragment with ManualFragment to create a route
                cCallback.OnCreateRoutePressed();

            }
        });

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            cCallback = (SelectRouteInterfaceListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement SelectRouteInterfaceListener");
        }
    }

    // Interface for create route
    // MapActivity must implement this interface
    public interface SelectRouteInterfaceListener {
        void onSelectRoutePressed(int position);
        void OnCreateRoutePressed();
        void OnRemoveRoutePressed();
        void OnEditRoutePressed();
    }

    //Data base retrieval
    // Retrieve data from ROUTES_TABLE into a string
    public void retrieveData() {
        Cursor cursor;
        try {
            cursor = routeData.getAllData();
            // Check for data
            if (cursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();

            } else {
                while (cursor.moveToNext()) {
                    // Get data from index
                    //Distances
                    routesList.add(cursor.getString(2));

                    //Convert the miliseconds date to readable formate
                    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
                    Long dateL = Long.parseLong(cursor.getString(3));
                    Date resultdate = new Date(dateL);
                    //Add the string of dates to an arraylist
                    datesList.add(sdf.format(resultdate));
                    Log.i("DateTesting", ""+sdf.format(resultdate));
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "No Data Loaded", Toast.LENGTH_SHORT).show();
        }
    }




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    //Menu options
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.reset_route_data){
            //Reset the dataBase and update list
            routeData.deleteTable();
            routesList.clear();
            adapter.notifyDataSetChanged();
            Toast.makeText(getActivity(), "Route data has been deleted", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }


}
