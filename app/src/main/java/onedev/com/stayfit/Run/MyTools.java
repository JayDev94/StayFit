package onedev.com.stayfit.Run;

/**
 * Created by Juan on 7/11/2016.
 */

public class MyTools {
    private double miles;
    public double convertToDoubleMiles(double meters){
        //Convert into miles and round .10
        miles = meters * 0.00062137;
        double roundedMiles = Math.round(miles * 100.0) / 100.0;
        return roundedMiles;

    }

    public double convertStringToMiles(String string){
        double meters = Double.parseDouble(string);
        //Convert into miles and round .10
        miles = meters * 0.00062137;
        double roundedMiles = Math.round(miles * 100.0) / 100.0;
        return roundedMiles;
    }

    public double converStringToMeters(String string){
        double meters = Double.parseDouble(string);
        double roundedMeters = Math.round(meters * 100.0)/100.0;
        return  roundedMeters;
    }

    public String converStringToDouble(String value){
        double dbl = Double.parseDouble(value);
        return value;
    }
    public double convertMiToKm(double miles) {
        double km = miles * 1.60934;
        km = Math.round(km * 100.0) / 100.0;
        return km;
    }

    public double convertKmToMi(double km){
        double miles = km * 0.621371;
        miles = Math.round(miles * 100.0) / 100.0;
        return miles;
    }

    public double roundToTenth(double value){
        double newVal = Math.round(value * 100.0)/100.0;
        return newVal;
    }
}
