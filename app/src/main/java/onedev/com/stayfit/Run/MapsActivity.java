package onedev.com.stayfit.Run;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;

import onedev.com.stayfit.MainActivity;
import onedev.com.stayfit.R;
import onedev.com.stayfit.RunningData.RouteData;

public class MapsActivity extends AppCompatActivity implements SelectRoute.SelectRouteInterfaceListener,
        RunRoute.RunRouteStatsInterface, RouteStats.StatsFinishedInterface, AutoRoute.AutoRouteStatsInterface {
    //Routes data var
    RouteData routeData;

    Toolbar toolbar;
    Fragment currentFragment = null;
    int fragmentKey = -1;
    FragmentManager fm;
    FragmentTransaction fragmentTransaction;

    //Data sharing in fragments var
    Bundle bundle;

    //Bundles will stats data
    Bundle statsBundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_activity);

        //Bundle initialization
        bundle = new Bundle();
        statsBundle = new Bundle();

        //Initialize route data
        routeData = new RouteData(this);

        //Toolbar settings
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.parseColor("#65cf5d"));
        toolbar.setTitle("Run");

        //Receive intent for fragment decision
        Intent intent = getIntent();
        fragmentKey = intent.getIntExtra("FragmentKey", -1);

        fm = getSupportFragmentManager();
        fragmentTransaction = fm.beginTransaction();

        //Use the fragmentKey (RETRIEVED FROM RunFragment in Intent) to add corresponding Fragment
        switch (fragmentKey) {
            case 0:
                if (currentFragment == null) {
                } else {
                    fragmentTransaction.remove(currentFragment);
                }
                currentFragment = new AutoRoute();
                fragmentTransaction.replace(R.id.autoRunFragment, currentFragment);
                fragmentTransaction.commit();
                break;
            case 1:
                if (currentFragment == null) {
                } else {
                    fragmentTransaction.remove(currentFragment);
                }
                currentFragment = new SelectRoute();
                fragmentTransaction.replace(R.id.selectRouteFragment, currentFragment);
                fragmentTransaction.commit();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.map_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onSelectRoutePressed(int position) {

        //Put position in bundle and send to RunRouteu fragment
        bundle.putInt("Position", position);

        Toast.makeText(this, "Position of Route is " + position, Toast.LENGTH_SHORT).show();

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (currentFragment == null) {
        } else {
            fragmentTransaction.remove(currentFragment);
        }
        Fragment currentFragment = new RunRoute();
        //Set the bundle
        currentFragment.setArguments(bundle);

        ft.replace(R.id.selectRouteFragment, currentFragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void OnCreateRoutePressed() {

        //New fragment transaction needed
        fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (currentFragment == null) {
        } else {
            fragmentTransaction.remove(currentFragment);
        }
        Fragment currentFragment = new ManualRoute();
        fragmentTransaction.replace(R.id.manualRouteFragment, currentFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void OnRemoveRoutePressed() {

    }

    @Override
    public void OnEditRoutePressed() {

    }


    @Override
    public void onRouteRan(String route, String time, String distance, String calories, String miles, String milesTimes) {
        ArrayList<String> statsArray = new ArrayList<>();
        statsArray.add(route);
        statsArray.add(time);
        statsArray.add(distance);
        statsArray.add(calories);
        statsArray.add(miles);
        statsArray.add(milesTimes);
        //Set bundles
        statsBundle.putStringArrayList("RouteTimeDistanceBundle", statsArray);

        //New fragment transaction needed
        fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (currentFragment == null) {
        } else {
            fragmentTransaction.remove(currentFragment);
        }
        currentFragment = new RouteStats();
        fragmentTransaction.add(R.id.routeStatsFragment, currentFragment);
        //fragmentTransaction.addToBackStack(null);
        //Set arguments
        currentFragment.setArguments(statsBundle);
        fragmentTransaction.commit();
    }

    @Override
    public void onStatsComplete() {
        Intent i;
        i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    @Override
    public void autoRouteRan(String routeData, String time, String distance, String calories, String miles, String milesTimes, String dataRan, String startTime, String endTime) {
        ArrayList<String> statsArray = new ArrayList<>();
        statsArray.add(routeData);
        statsArray.add(time);
        statsArray.add(distance);
        statsArray.add(calories);
        statsArray.add(miles);
        statsArray.add(milesTimes);
        statsArray.add(startTime);
        statsArray.add(endTime);
        //Set bundles
        statsBundle.putStringArrayList("RouteTimeDistanceBundle", statsArray);

        //New fragment transaction needed
        fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (currentFragment == null) {
        } else {
            fragmentTransaction.remove(currentFragment);
        }
        currentFragment = new RouteStats();
        fragmentTransaction.add(R.id.routeStatsFragment, currentFragment);
        //fragmentTransaction.addToBackStack(null);
        //Set arguments
        currentFragment.setArguments(statsBundle);
        fragmentTransaction.commit();
    }
}

