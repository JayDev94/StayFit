package onedev.com.stayfit.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/*
 * This database hold's the user's name, age, height, and weight.  This info will be used for the app's physical health calculations.
 * The data can be edited in the profile/edit/attribute portion of the application.
 */

public class ProfileDatabase extends SQLiteOpenHelper {
	public static String DATABASE_NAME = "StayFitProfile.db";
	public static String TABLE_NAME = "profile_table";
	public static String COL_1 = "ID";
	public static String COL_2 = "NAME";
	public static String COL_3 = "AGE";
	public static String COL_4 = "HEIGHT";
	public static String COL_5 = "WEIGHT";


	public ProfileDatabase(Context context) {
		super(context, DATABASE_NAME, null, 1);
		// TODO Auto-generated constructor stub
	}


	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		 //Create the table  //////DO NOT USE AUTOINCREMENT///////
        db.execSQL("create table " + TABLE_NAME + " ("+COL_1+" INTEGER PRIMARY KEY, "+COL_2+" TEXT, "+COL_3+" INTEGER, "+COL_4+" INTEGER, "+ COL_5+ " INTEGER)");

	}


	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		//If table exists
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        //pass this db instance
        onCreate(db);

	}
	
	//Insert data
    public boolean insertData( String name, Integer age, Integer height, Integer weight) {
        //Create database to write
        SQLiteDatabase db = this.getWritableDatabase();

        //To hold the content
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, name);
        contentValues.put(COL_3, age);
        contentValues.put(COL_4, height);
        contentValues.put(COL_5, weight);
        
        //Insert data into result to check for existence
        long result = db.insert(TABLE_NAME, null, contentValues);
        return result != -1;
    }
    
    //Get data
    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_NAME, null);
        return cursor;
    }

    //Update data
    public boolean updateData(String id, String name, Integer age, Integer height, Integer weight){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_1, id);
        contentValues.put(COL_2, name);
        contentValues.put(COL_3, age);
        contentValues.put(COL_4, height);
        contentValues.put(COL_5, weight);
        //Update data on the basis of id
        db.update(TABLE_NAME, contentValues, "ID = ?",new String[] {id});
        return true;
    }

    //Delete specific data
    public Integer deleteData (String id){
        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(TABLE_NAME, "ID = ?", new String[] {id});
    }
    
    public void deleteTable(){
        SQLiteDatabase db = this.getWritableDatabase();
        
        db.execSQL("delete from "+ TABLE_NAME);

    }
}





