package onedev.com.stayfit.FirstLaunch;

import onedev.com.stayfit.MainActivity;
import onedev.com.stayfit.R;

import onedev.com.stayfit.database.ProfileDatabase;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LaunchInfo extends Fragment implements AnimationListener {
	// Profile Database
	ProfileDatabase myDb;

	// SharedPreferences objects
	SharedPreferences sharedPrefs;
	String spKEY = "FirstLaunch";

	Button btnContinueInfo;
	Button btnBack;
	TextView labelText;
	EditText editText;

	int infoPage = 2;
	int infoPosition = -1;

	String strName = "-";
	String strAge = "-";
	String strHeight = "-";
	String strWeight = "-";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.launch_fragment_infotext, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		sharedPrefs = getActivity().getSharedPreferences(spKEY, getActivity().MODE_PRIVATE);

		btnContinueInfo = (Button) view.findViewById(R.id.btnContinueInfo);
		btnBack = (Button) view.findViewById(R.id.btnInfoBack);
		labelText = (TextView) view.findViewById(R.id.viewInfoLabel);
		editText = (EditText) view.findViewById(R.id.editLaunchInfoEdit);

		// Initialize the db with the current context
		myDb = new ProfileDatabase(getActivity());

		// Set default text for labelText
		showInfo();

		// On btnContinueInfo clicked
		btnContinueInfo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (editText.getText().toString().isEmpty()) {
					editText.setText("");
				}
				getInfo();
				if (!(infoPosition >= 4)) {
					showInfo();
					infoPosition++;
					showInfo();
					Log.i("Info-Position", infoPosition + "");
				} else {
					// infoPosition = 4;
				}
			}
		});

		// On btnBack clicked
		btnBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!(infoPosition <= -1)) {
					showInfo();
					infoPosition--;
					Log.i("Info-Position", infoPosition + "");
					showInfo();
				} else {
					// infoPosition = -1;
				}

			}
		});

	}

	// Which information to show
	/* CASE 3 --continues to -> MainActivity // Saves info to ProfileDatabase */
	public void showInfo() {
		switch (infoPosition) {
		case -1:
			labelText.setText("Full Name");
			break;
		case 0:
			labelText.setText("Age");
			break;
		case 1:
			labelText.setText("Height");
			break;
		case 2:
			labelText.setText("Weight");
			break;
		case 3:
			Log.i("TEXT INFORMATION", strName + " " + strAge + " " + strHeight + " " + strWeight);
			sharedPrefs.edit().putBoolean("Launch", false).apply();

			int age, height, weight;

			// Convert to int myDb.insertData(String, Integer, Integer, Integer)
			age = Integer.parseInt(strAge);
			height = Integer.parseInt(strHeight);
			weight = Integer.parseInt(strWeight);
			
			////SAVE DATA_______________________________________________________________________________
			boolean isInserted = myDb.insertData(strName, age, height, weight);
			if (isInserted = true) {
				Toast.makeText(getActivity(), "Data is inserted", Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(getActivity(), "ERROR, DATA WAS NOT INSERTED", Toast.LENGTH_LONG).show();
			}
			////________________________________________________________________________________________
			
			// Go to MainActivity.class
			Intent i = new Intent(getActivity(), MainActivity.class);
			startActivity(i);
			break;
		}
	}

	// Get info from editText
	public void getInfo() {
		switch (infoPosition) {
		case -1:
			strName = editText.getText().toString();
			break;
		case 0:
			strAge = editText.getText().toString();
			break;
		case 1:
			strHeight = editText.getText().toString();
			break;
		case 2:
			strWeight = editText.getText().toString();
			break;
		}
	}

	/*
	 * // Fragment settings public void nextFragment() { Fragment f = new
	 * LaunchProfilePic(); FragmentManager fragmentManager =
	 * getActivity().getSupportFragmentManager(); FragmentTransaction
	 * fragmentTransaction = fragmentManager.beginTransaction();
	 * 
	 * fragmentTransaction.replace(R.id.container, f);
	 * fragmentTransaction.addToBackStack(null); fragmentTransaction.commit(); }
	 */

	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub

	}

}
