package onedev.com.stayfit.FirstLaunch;

import onedev.com.stayfit.MainActivity;
import onedev.com.stayfit.R;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class LaunchProfilePic extends Fragment {
	Button btnFinish;
	// SharedPreferences objects
	SharedPreferences sharedPrefs;
	String spKEY = "FirstLaunch";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.launch_fragment_profilepic, container, false);

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		sharedPrefs = getActivity().getSharedPreferences(spKEY, getActivity().MODE_PRIVATE);

		btnFinish = (Button) view.findViewById(R.id.btnContinueToMain);

		// On btnFinish click
		btnFinish.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sharedPrefs.edit().putBoolean("Launch", false).apply();


				//Go to MainActivity.class
				Intent i = new Intent(getActivity(), MainActivity.class);
				startActivity(i);
			}
		});
	}

}
