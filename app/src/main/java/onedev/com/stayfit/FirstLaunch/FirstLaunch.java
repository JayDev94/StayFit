package onedev.com.stayfit.FirstLaunch;

import android.app.Activity;


import onedev.com.stayfit.R;

import android.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Build;

public class FirstLaunch extends AppCompatActivity {
	FragmentManager fragmentManager;
	FragmentTransaction fragmentTransaction;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_first_launch);
		if (savedInstanceState == null) {
			// Launch PlaceHolderFragment
			fragmentManager = getSupportFragmentManager();
			fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.first_start, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment implements AnimationListener {
		TextView welcomeText;
		Button btnContinue;
		Button btnDeletePrefs;
		// For Welcome Text
		Animation animFadeInTxt;
		// For Continue Button
		Animation animFadeInBtn;

		// SharedPreferences objects
		SharedPreferences sharedPrefs;
		String spKEY = "FirstLaunch";

		/*
		 * //Fragment settings objects Fragment fragment; FragmentManager fM;
		 * FragmentTransaction fT;
		 */
		

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_first_start, container, false);
			return rootView;
		}

		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onViewCreated(view, savedInstanceState);
			sharedPrefs = getActivity().getSharedPreferences(spKEY, getActivity().MODE_PRIVATE);
			
			welcomeText = (TextView) view.findViewById(R.id.viewWelcome);
			btnContinue = (Button) view.findViewById(R.id.btnContinue);
			// Hide Continue Button ///Will be unhiden after text animation is
			// done
			btnContinue.setVisibility(View.GONE);
			// btnDeletePrefs = (Button) view.findViewById(R.id.btnDeletePrefs);
			
			

			// Load fade-in animation
			animFadeInTxt = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
			// Start animation
			welcomeText.startAnimation(animFadeInTxt);

			// Animation Listener ///On End
			animFadeInTxt.setAnimationListener(this);
			
			
			
			
			
			// On btnContinue clicked
			btnContinue.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					//sharedPrefs.edit().putBoolean("Launch", false).apply();
					
					//Replace current fragment and go to next (LaunchInfo Fragment)
					Fragment f = new LaunchInfo();
			        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
			        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			        fragmentTransaction.replace(R.id.container,  f);
			        fragmentTransaction.addToBackStack(null);
			        fragmentTransaction.commit();

					// Intent i = new Intent(getActivity(), MainActivity.class);
					// startActivity(i);
				}
			});

			

		}

		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onAnimationEnd(Animation animation) {
			// TODO Auto-generated method stub
			if (animation == animFadeInTxt) {
				// Load fade-in animation // make Continue button visible
				btnContinue.setVisibility(View.VISIBLE);
				animFadeInBtn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
				btnContinue.startAnimation(animFadeInBtn);
			}
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub

		}

	}
}
