package onedev.com.stayfit;

import android.os.Environment;
import android.support.v4.widget.DrawerLayout;

import onedev.com.stayfit.FirstLaunch.FirstLaunch;
//import onedev.com.stayfit.Profile.ProfileFragment;
import onedev.com.stayfit.Profile.ProfileFragment;
import onedev.com.stayfit.Progress.ProgressFragment;
import onedev.com.stayfit.Run.AutoRoute;
import onedev.com.stayfit.Run.RouteStats;
import onedev.com.stayfit.RunningData.RouteData;
import onedev.com.stayfit.RunningData.RouteStatsData;
import onedev.com.stayfit.Settings.SettingsFragment;
import onedev.com.stayfit.WorkoutDatabase.*;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

//By default, the first screen to appear will be the Run fragment's UI
/*
APIs being used:
    GoogleMaps API
 */

public class MainActivity extends AppCompatActivity implements AutoRoute.AutoRouteStatsInterface, RouteStats.StatsFinishedInterface {
    Fragment fragment1 = null, fragment2 = null;
    Fragment currentFragment = null;
    FragmentManager fm;
    FragmentTransaction fragmentTransaction;

    //Database contents
    CalendarData cD;
    WorkoutData wD;
    ExerciseData eD;
    SetsData sD;
    RouteStatsData routeStatsData;
    RouteData routeData;
    WorkoutStatsData workoutStatsData;

    SharedPreferences sharedPrefs;
    String spKEY = "FirstLaunch";

    // Declare Titles And Icons For Our Navigation Drawer List View
    // Icons And Titles are held in an array --- position's are corresponding
    String TITLES[] = {"Run", "Workouts", "Progress", "Settings"};
    int ICONS[] = {R.drawable.icon_run_small, R.drawable.icon_workout,
            R.drawable.icon_progress, R.drawable.icon_settings};

    // Similarly we Create a String Resource for the name and email in the
    // header view
    // And we also create a int resource for profile picture in the header view
    String NAME = "Juan Garcia";
    String EMAIL = "one.dev94@gmail.com";
    int PROFILE = R.drawable.header_img_stayfit;

    // Declaring the Toolbar Object
    private Toolbar toolbar;

    // Declaring RecyclerView
    RecyclerView mRecyclerView;
    // Declaring Adapter For Recycler View
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    DrawerLayout Drawer;

    ActionBarDrawerToggle mDrawerToggle;

    //Adapter position selection
    int adapterSelection = -1;

    boolean firstLaunch = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Initialize database classes
        cD = new CalendarData(this);
        wD = new WorkoutData(this);
        sD = new SetsData(this);
        eD = new ExerciseData(this);
        routeData = new RouteData(this);
        routeStatsData = new RouteStatsData(this);
        workoutStatsData = new WorkoutStatsData(this);

        // SharedPreference initialization and editor
        sharedPrefs = getSharedPreferences(spKEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor spEditor = sharedPrefs.edit();

        // Retrieve SharedPreferences
        firstLaunch = sharedPrefs.getBoolean("Launch", true);

        // Loads first Launch Activity
        if (firstLaunch == true) {
            // Start FirstLaunch activity
            Intent i = new Intent(getApplicationContext(), FirstLaunch.class);
            startActivity(i);
        } else {
            setContentView(R.layout.activity_main);

            // Load the RunFragment by default
            fm = getSupportFragmentManager();
            fragmentTransaction = fm.beginTransaction();
            currentFragment = new AutoRoute();
            fragmentTransaction.replace(R.id.autoRunFragment, currentFragment);
            fragmentTransaction.commit();


            //Tool bar settings
            toolbar = (Toolbar) findViewById(R.id.app_bar);
            setSupportActionBar(toolbar);

            // Set the title to white
            toolbar.setTitleTextColor(Color.parseColor("#65cf5d"));
            // Set the padding to match the Status Bar height
            //toolbar.setPadding(0, 0, 0, 0);



            // Assigning the RecyclerView Object to the xml View
            mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
            // Letting the system know that the list objects are of fixed size
            mRecyclerView.setHasFixedSize(true);


            // Adapter with CustomAdapter initialization
            mAdapter = new CustomAdapter(TITLES, ICONS, NAME, EMAIL, PROFILE, adapterSelection);

            // Setting the adapter to RecyclerView
            mRecyclerView.setAdapter(mAdapter);

            final GestureDetector mGestureDetector = new GestureDetector(MainActivity.this,
                    new GestureDetector.SimpleOnGestureListener() {

                        @Override
                        public boolean onSingleTapUp(MotionEvent e) {
                            return true;
                        }

                    });
            // When item is clicked in the drawer (MENU)
            mRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                @Override
                public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                    View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                    if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {
                        ///////// FRAGMENT UPLOAD\\\\\\\\\\\\
                        // Fragment manager and fragmentTransaction
                        fm = getSupportFragmentManager();
                        fragmentTransaction = fm.beginTransaction();
                        // Load fragments and replace each with a conditional
                        // statement(REMOVES CURRENT FRAGMENT)
                        switch (recyclerView.getChildPosition(child)) {
                            case 0:
                                if (currentFragment == null) {
                                } else {
                                    fragmentTransaction.remove(currentFragment);
                                }
                                currentFragment = new ProfileFragment();
                                fragmentTransaction.replace(R.id.profileFragment, currentFragment);
                                fragmentTransaction.commit();
                                break;

                            case 1:
                                updateAdapter(1);
                                if (currentFragment == null) {
                                } else {
                                    fragmentTransaction.remove(currentFragment);
                                }
                                currentFragment = new AutoRoute();
                                fragmentTransaction.replace(R.id.autoRunFragment, currentFragment);
                                fragmentTransaction.commit();

                                break;
                            case 2:
                                updateAdapter(2);
                                if (currentFragment == null) {
                                } else {
                                    fragmentTransaction.remove(currentFragment);
                                }
                                currentFragment = new WorkoutFragment();
                                fragmentTransaction.replace(R.id.workoutFragment, currentFragment);
                                fragmentTransaction.commit();
                                break;
                            case 3:
                                updateAdapter(3);
                                if (currentFragment == null) {
                                } else {
                                    fragmentTransaction.remove(currentFragment);
                                }
                                currentFragment = new ProgressFragment();
                                fragmentTransaction.replace(R.id.progressFragment, currentFragment);
                                fragmentTransaction.commit();
                                break;
                            case 4:
                                updateAdapter(4);
                                if (currentFragment == null) {
                                } else {
                                    fragmentTransaction.remove(currentFragment);
                                }
                                currentFragment = new SettingsFragment();
                                fragmentTransaction.replace(R.id.setttingsFragment, currentFragment);
                                fragmentTransaction.commit();
                                break;

                            case 5

                                    :
                                break;
                        }
                        ///////// FRAGMENT UPLOAD\\\\\\\\\\\\

                        // Sets the current linear layout (item) to a selected
                        // state
                        ///// TRYING TO SHOW A PRESSED CONDITION ON THE SELECTED
                        // ITEM//////
                        /////// NEEDS FIX//////////


                        Drawer.closeDrawers();
                        Toast.makeText(MainActivity.this,
                                "The Item Clicked is: " + recyclerView.getChildPosition(child), Toast.LENGTH_SHORT)
                                .show();

                        return true;
                    }

                    return false;
                }

                @Override
                public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

                }

                @Override
                public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                }
            });
            // Creating a layout Manager
            mLayoutManager = new LinearLayoutManager(this);

            // Setting the layout Manager
            mRecyclerView.setLayoutManager(mLayoutManager);

            // Drawer object Assigned to the view
            Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);
            //Set the Status Bar color
            Drawer.setStatusBarBackgroundColor(Color.parseColor("#65cf5d"));
           // Drawer.setStatusBarBackgroundColor(Color.TRANSPARENT);
            mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, toolbar, R.string.hello_world,
                    R.string.hello_world) {

                // code here will execute once the drawer is opened( As I dont
                // want
                // anything happened whe drawer is
                // open I am not going to put anything here)
                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                }

                // Code here will execute once drawer is closed
                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);

                }
            };
            // Drawer Toggle Object Made
            // Drawer Listener set to the Drawer toggle
            Drawer.setDrawerListener(mDrawerToggle);
            // Finally we set the drawer toggle sync State
            mDrawerToggle.syncState();

        }
    }

    //COULD NOT GET THE ADAPTER TO UPDATE SO CREATED A METHOD TO RECREATE
    public void updateAdapter(int position){
        mAdapter = new CustomAdapter(TITLES, ICONS, NAME, EMAIL, PROFILE, position);
        // Setting the adapter to RecyclerView
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // if first or 'Main menu' item is pressed ->(CONTAINS ITEM)
        if (id == R.id.action_favorite) {
            cD.deleteTable();
            wD.deleteTable();
            sD.deleteTable();
            eD.deleteTable();
            routeData.deleteTable();
            routeStatsData.deleteTable();
            workoutStatsData.deleteTable();

            //Delete contents of the StayFitSnapshots folder
            File myDirectory = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "StayFitSnapshots");
            if (myDirectory.isDirectory())
            {
                String[] contents = myDirectory.list();
                for (int i = 0; i < contents.length; i++)
                {
                    new File(myDirectory, contents[i]).delete();
                }
            }

            Toast.makeText(getApplicationContext(), "Everything has been deleted", Toast.LENGTH_LONG).show();
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);



        }
        // if second menu item is pressed
		/*
		 * if (id == R.id.action_settings) { return true; }
		 */

        return super.onOptionsItemSelected(item);
    }





    ////THIS SENDS AUTO ROUTE COMPLETION DATA TO THE STATS FRAGMENT///
    @Override
    public void autoRouteRan(String routeData, String time, String distance, String calories, String miles, String milesTimes, String dateRan,
                             String startTime, String endTime) {
        ArrayList<String> statsArray = new ArrayList<>();
        statsArray.add(routeData);
        statsArray.add(time);
        statsArray.add(distance);
        statsArray.add(calories);
        statsArray.add(miles);
        statsArray.add(milesTimes);
        statsArray.add(dateRan);
        statsArray.add(startTime);
        statsArray.add(endTime);

        //Set bundles
        Bundle statsBundle = new Bundle();
        statsBundle.putStringArrayList("RouteTimeDistanceBundle", statsArray);

        //New fragment transaction needed
        fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (currentFragment == null) {
        } else {
            fragmentTransaction.remove(currentFragment);
        }
        currentFragment = new RouteStats();
        fragmentTransaction.add(R.id.routeStatsFragment, currentFragment);
        //fragmentTransaction.addToBackStack(null);
        //Set arguments
        currentFragment.setArguments(statsBundle);
        fragmentTransaction.commit();
    }

    @Override
    public void onStatsComplete() {
       // Just in case there is any future confusion.
       /*This was originally supposed to call MainActivity which we are in.  Default aspects of the AutoRoute and Stats
        -portions were all meant to be implemented in the MapsActivity class.*/

        //Remove the fragment
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.remove(currentFragment).commit();

        Intent i;
        i = new Intent(this, MainActivity.class);
        startActivity(i);
    }
}