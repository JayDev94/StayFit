package onedev.com.stayfit.Workout;

import java.util.List;

import onedev.com.stayfit.WorkoutDatabase.ExerciseData;
import onedev.com.stayfit.R;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class SetsAdapter extends ArrayAdapter<Sets> {


    private List<Sets> items;
    private int layoutResourceId;
    private Context context;

    public SetsAdapter(Context context, int layoutResourceId, List<Sets> items) {
        super(context, layoutResourceId, items);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        SetsHolder holder = null;


        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        row = inflater.inflate(layoutResourceId, parent, false);


        holder = new SetsHolder();
        holder.sets = items.get(position);

        //holder.removePaymentButton = (ImageButton)row.findViewById(R.id.atomPay_removePay);
        //holder.removePaymentButton.setTag(holder.sets);

        //Initializations and textwatchers(holder)
        holder.name = (EditText) row.findViewById(R.id.editListReps);
        setRepTextChangeListener(holder);
        holder.value = (EditText) row.findViewById(R.id.editListWeight);
        setWeightTextChangeListener(holder);
        holder.labelSets = (TextView) row.findViewById(R.id.textListSet);

        row.setTag(holder);

        setupItem(holder);

        convertView.setTag(holder);
        return row;
    }

    private void setupItem(SetsHolder holder) {
        holder.name.setText(String.valueOf(holder.sets.getReps()));
        holder.value.setText(String.valueOf(holder.sets.getReps()));
        holder.labelSets.setText(holder.sets.getLabelSet());
    }

    public static class SetsHolder {
        Sets sets;
        EditText name;
        EditText value;
        TextView labelSets;
    }

    //Set set label text
    private void setLabelSetText(String label) {
        SetsHolder holder = new SetsHolder();
        holder.sets.setLabelSet(label);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 500;
    }

    //Set reps text on text changed
    private void setRepTextChangeListener(final SetsHolder holder) {
        holder.name.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                holder.sets.setReps(s.toString());

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                //holder.sets.setReps(Integer.parseInt(s.toString()));


            }

        });
    }

    //Set weight text on text changed
    private void setWeightTextChangeListener(final SetsHolder holder) {
        holder.value.addTextChangedListener(new TextWatcher() {


            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    holder.sets.setWeight(s.toString());
                    Log.i("DATA-SAVED-REPS", "Saved data");
                } catch (NumberFormatException e) {
                    //	Log.i(LOG_TAG, "error reading double value: " + s.toString());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                //holder.sets.setWeight(s.toString());


            }
        });
    }
}























/*
package onedev.com.stayfit.Workout;

import java.util.List;

import onedev.com.stayfit.WorkoutDatabase.ExerciseData;
import onedev.com.stayfit.R;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class SetsAdapter extends ArrayAdapter<Sets> {



	private List<Sets> items;
	private int layoutResourceId;
	private Context context;

	public SetsAdapter(Context context, int layoutResourceId, List<Sets> items) {
		super(context, layoutResourceId, items);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.items = items;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		SetsHolder holder = null;

		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		row = inflater.inflate(layoutResourceId, parent, false);

		holder = new SetsHolder();
		holder.sets = items.get(position);
		//holder.removePaymentButton = (ImageButton)row.findViewById(R.id.atomPay_removePay);
		//holder.removePaymentButton.setTag(holder.sets);

		//Initializations and textwatchers(holder)
		holder.name = (EditText)row.findViewById(R.id.editListReps);
		setRepTextChangeListener(holder);
		holder.value = (EditText)row.findViewById(R.id.editListWeight);
		setWeightTextChangeListener(holder);
		holder.labelSets = (TextView)row.findViewById(R.id.textListSet);

		row.setTag(holder);

		setupItem(holder);
		return row;
	}

	private void setupItem(SetsHolder holder) {
		holder.name.setText(String.valueOf(holder.sets.getReps()));
		holder.value.setText(String.valueOf(holder.sets.getReps()));
		holder.labelSets.setText(holder.sets.getLabelSet());
	}

	public static class SetsHolder {
		Sets sets;
		EditText name;
		EditText value;
		TextView labelSets;
	}

	//Set set label text
	private void setLabelSetText(String label){
		SetsHolder holder = new SetsHolder();
		holder.sets.setLabelSet(label);
	}

	//Set reps text on text changed
	private void setRepTextChangeListener(final SetsHolder holder) {
		holder.name.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				holder.sets.setReps(s.toString());

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

			@Override
			public void afterTextChanged(Editable s) {
				//holder.sets.setReps(Integer.parseInt(s.toString()));

			}

		});
	}

	//Set weight text on text changed
	private void setWeightTextChangeListener(final SetsHolder holder) {
		holder.value.addTextChangedListener(new TextWatcher() {


			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				try{
					holder.sets.setWeight(s.toString());
						Log.i("DATA-SAVED-REPS", "Saved data");
				}catch (NumberFormatException e) {
				//	Log.i(LOG_TAG, "error reading double value: " + s.toString());
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

			@Override
			public void afterTextChanged(Editable s) {
				holder.sets.setWeight(s.toString());

			}
		});
	}
}

 */