package onedev.com.stayfit.Workout;

public class Exercise {

	String name = null;
	String set = null;
	String rep = null;
	boolean selected = false;

	public Exercise(String name, String sets, String reps, boolean selected) {
		super();
		this.name = name;
		this.set = sets;
		this.rep = reps;
		this.selected = selected;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSet() {
		return set;
	}

	public void setSet(String set) {
		this.set = set;
	}

	public String getRep() {
		return rep;
	}

	public void setRep(String rep) {
		this.rep = rep;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}