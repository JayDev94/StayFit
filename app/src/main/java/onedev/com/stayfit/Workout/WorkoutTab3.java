
package onedev.com.stayfit.Workout;
/////////This tab will be tested for the workout list

import android.app.Dialog;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.SparseBooleanArray;

import java.util.ArrayList;
import java.util.List;


import onedev.com.stayfit.R;
import onedev.com.stayfit.WorkoutDatabase.ExerciseData;
import onedev.com.stayfit.WorkoutDatabase.SetsData;
import onedev.com.stayfit.WorkoutDatabase.WorkoutData;
import onedev.com.stayfit.WorkoutDatabase.WorkoutDataEdit;

import android.app.ActionBar.LayoutParams;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class WorkoutTab3 extends Fragment {
    /*
     * This view is used to capture the instance created by onViewCreated. This
     * is done so that the view can be restarted from anywhere. The EVERYTHING()
     * method holds everything that would go in onCreateView. Inside of the
     * method is a View which points to v, the view that holds view instance in
     * onViewCreated.
     */
    View ClassView;

    // workouts database table
    WorkoutData workoutData;
    ExerciseData excData;
    SetsData setsData;
    ArrayList<Integer> ExcKeyList;

    // ListView setting objects
    ListView listView;
    ArrayAdapter<String> listAdapter;
    ArrayList<String> workoutList;
    ArrayList<String> dataWorkoutKeys;
    ArrayList<String> dataWorkoutExercises;
    ArrayList<String> dataWorkoutNames;

    Button btnAdd;
    Button btnDelete;

    // Popup contents
    EditText editWName;
    Button btnSaveList;
    ListView leftList;
    ListView rightList;
    ArrayAdapter<String> leftAdapter;
    ArrayAdapter<String> rightAdapter;
    Button saveList;
    ArrayList<String> loadedArrayList;
    ArrayList<String> selectedArrayList;


    //Exercise preview contents
    ArrayList<String>dataExerciseNames;
    ArrayList<String>dataSets;

    // Will go into WORKOUT_TABLE -compiledExercises will contain the id's of
    // the workouts in one string
    String workoutName = "";
    String compiledExercises = "";

    //Editing var
    ArrayList<Integer> loadedExerciseKeysList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        return inflater.inflate(R.layout.workout_tab3, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        ClassView = view;
        EVERYTHING();

    }

    public void EVERYTHING() {
        View view = ClassView;
        // Initialize database class for use
        workoutData = new WorkoutData(getActivity());
        excData = new ExerciseData(getActivity());
        setsData = new SetsData(getActivity());

        listView = (ListView) view.findViewById(R.id.listViewWorkouts);
        btnAdd = (Button) view.findViewById(R.id.btnAddWorkout);

        //ArrayList init.
        dataWorkoutKeys = new ArrayList<>();
        dataWorkoutExercises = new ArrayList<>();
        dataWorkoutNames = new ArrayList<>();
        loadedExerciseKeysList = new ArrayList<>();
        dataExerciseNames = new ArrayList<>();
        dataSets = new ArrayList<>();

        // ListView settings
        workoutList = new ArrayList<String>();


        listAdapter = new ArrayAdapter<String>(getActivity(), R.layout.costume_simple_list_item, workoutList);
        listView.setAdapter(listAdapter);

        //Retrieve exercises
        retrieveExerciseKeys();

        ////////////////////SET ON LIST ITEM CLICK///////////////////////
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                previewPopup(view, position);
            }
        });
        //set on long click listener
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
                final Dialog openDialog = new Dialog(getActivity());
                openDialog.setContentView(R.layout.edit_dialog);
                openDialog.setTitle("EDIT");
                Button btnDelete = (Button) openDialog.findViewById(R.id.btnDialogDelete);
                Button btnEdit = (Button) openDialog.findViewById(R.id.btnDialogEdit);
                Button btnCheckData = (Button) openDialog.findViewById(R.id.btnDialogTestCheckData);
                btnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        WorkoutDataEdit workoutDataEdit = new WorkoutDataEdit(getActivity());
                        //in this case, since we are in tab 3, delete the workout data
                        workoutDataEdit.deleteWorkouts(position);
                        openDialog.dismiss();
                    }
                });

                btnEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showPopupWorkout(view, true, position);    ///Pass true since we are updating
                        openDialog.dismiss();
                    }
                });
                btnCheckData.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //testing
                    }
                });


                openDialog.show();

                return false;
            }
        });


        // Retrieve workout data from database
        retrieveWorkouts();

        // listView.setAdapter(customAdapter);

        //On btnAdd clicked event
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                showPopupWorkout(v, false, -1);    ///Pass false since we are adding not updating
            }
        });

        //TESTING TOOL: On btnDelete clicked event
        /*btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Clear the arraylist
				workoutList.clear();
				//Delete the workout table
				workoutData.deleteTable();
				//Refresh the fragment view
				EVERYTHING();
			}
		});*/
    }


    // ______________________________________________________________________________________________________________
    // Popup creates the workout
    public void showPopupWorkout(View view, final boolean isUpdate, final int workoutPosition) {
        View popupView = getActivity().getLayoutInflater().inflate(R.layout.workout_popup_creator, null);
        final PopupWindow popupWindow = new PopupWindow(popupView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        leftList = (ListView) popupView.findViewById(R.id.popupExcList);
        rightList = (ListView) popupView.findViewById(R.id.popupSelectedList);
        editWName = (EditText) popupView.findViewById(R.id.editCreateWName);
        btnSaveList = (Button) popupView.findViewById(R.id.btnSaveList);

        // Initialize the left list with the custome adapter and the right list
        // with an array adapter
        loadedArrayList = new ArrayList<String>();
        selectedArrayList = new ArrayList<String>();
        ExcKeyList = new ArrayList<Integer>();

        leftAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_small_text_item, loadedArrayList);
        rightAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_small_text_item_green,
                selectedArrayList);

        leftList.setAdapter(leftAdapter);
        rightList.setAdapter(rightAdapter);

        retrieveExercise();
        leftAdapter.notifyDataSetChanged();
        rightAdapter.notifyDataSetChanged();

        //Edit var
        loadedExerciseKeysList.clear();


        /////////////////////If the onUpdate is true load the selectedArrayList with the corresponding workout
        if (isUpdate) {
            //Load edit name with the data name in position
            editWName.setText(dataWorkoutNames.get(workoutPosition));

            //Load the exercise for the corresponding workout
            String loadedExercises = dataWorkoutExercises.get(workoutPosition);
            Log.i("LoadedExercises", loadedExercises);
            char[] excCharArray = loadedExercises.toCharArray();
            //Add excCharArray to arraylist
            for (char c : excCharArray) {
                String str = c + "";
                int k = Integer.parseInt(str);
                loadedExerciseKeysList.add(k);
            }
            for (int f = 0; f < loadedExerciseKeysList.size(); f++) {
                Log.i("f23", "" + loadedExerciseKeysList.get(f));
            }
            //Since the index here must start from 0, do -1 on the loaedExerxisesKeyLists values when comparing
            for (int y = 0; y < loadedExerciseKeysList.size(); y++) {
                for (int x = 0; x < loadedArrayList.size(); x++) {
                    if (x == (loadedExerciseKeysList.get(y) - 1)) {
                        //Add the exercises to the selected list
                        selectedArrayList.add(loadedArrayList.get(x));
                    }
                }
            }
            rightAdapter.notifyDataSetChanged();
            Log.i("LoadedLeft", selectedArrayList.size() + "");


            //Load the ExcKeyList with the exercises
            ExcKeyList.addAll(loadedExerciseKeysList);
            Log.i("ExcKeyListUpdate", ExcKeyList.size() + "");
        }


        // leftList settings
        leftList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                selectedArrayList.add(loadedArrayList.get(position));

                // Will add the position of the data into this key list -
                // Position represents the key- these 'Keys' will be used for
                // the WORKOUT_TABLE
                ExcKeyList.add(position + 1);
                Log.i("ExcKeyListAdd", ExcKeyList.size() + "");

                rightAdapter.notifyDataSetChanged();
            }

        });

        // rightList settings
        rightList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                // Dialog box 'Delete item????
                alertDialog(position);

                // Update adapter
                rightAdapter.notifyDataSetChanged();
            }

        });

        // On btnSaveList clicked
        btnSaveList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (editWName.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please name this workout", Toast.LENGTH_LONG).show();
                } else {
                    workoutName = editWName.getText().toString();
                    Integer compile;
                    for (int i = 0; i < selectedArrayList.size(); i++) {
                        //The exercises will be saved with an incremented value
                        //This is due to a bug disrupting the ability to save the 0 index
                        //You will need to decrement the values by one when loaded
                        compiledExercises = compiledExercises + ExcKeyList.get(i);
                    }
                    compile = Integer.parseInt(compiledExercises);

                    //decides whether this is an update or add
                    if (isUpdate) {
                        ///---------------------------Update
                        String dataPosition = (workoutPosition + 1) + "";
                        workoutData.updateData(dataWorkoutKeys.get(workoutPosition), workoutName, compile);
                    } else {
                        ///---------------------------Add
                        // Insert data into the database/table 'WORKOUT_TABLE'
                        workoutData.insertData(workoutName, compile);
                    }

                    Log.i("Key-List", workoutName + "  THE WORKOUTS:" + compiledExercises);
                }

                //Refresh the fragment view and clear the arraylist
                workoutList.clear();
                compiledExercises = "";
                EVERYTHING();
                popupWindow.dismiss();


            }
        });

        // If popupWindow should be focusable
        popupWindow.setFocusable(true);
        // close window when touched outside
        popupWindow.setBackgroundDrawable(new ColorDrawable());
        int location[] = new int[2];
        // Get the View's(the one that was clicked in the Fragment) location
        view.getLocationOnScreen(location);
        // Using location, the PopupWindow will be displayed right under
        // anchorView
        popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, location[0], location[1] + view.getHeight());
    }
    // ______________________________________________________________________________________________________________

    // =====================ALERT DIALOG===================//with deleteMethod
    // at the bottom
    public void alertDialog(final int position) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage("Delete Exercise");
        builder1.setCancelable(true);

        builder1.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Select the item in the ArrayList to delete through the mehtod
                // arg
                selectedArrayList.remove(position);
                // update the adapter
                rightAdapter.notifyDataSetChanged();
                //Delete the exercie key in the position
                ExcKeyList.remove(position);
                Log.i("ExcKeyListSizeA", ExcKeyList.size() + "");

                // Close dialog
                dialog.cancel();
            }
        });

        builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                // Close dialog
                dialog.cancel();
            }
        });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    // ====================================================
    //////////////////PREVIEW THE EXERCISES FOR THE SELECTED WORKOUT////////////////
    public void previewPopup(View view, int position) {
        View popupView = getActivity().getLayoutInflater().inflate(R.layout.workout_tab3_preview_pop, null);
        final PopupWindow popupWindow = new PopupWindow(popupView, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        ListView excListView = (ListView)popupView.findViewById(R.id.lvTab3Preview);
        ArrayList<Tab3Exercise> exerciseList = new ArrayList<>();
        Tab3PreviewAdapter exerciseAdapter;
        exerciseAdapter = new Tab3PreviewAdapter(getActivity(), R.layout.white_items_layout, exerciseList);
        excListView.setAdapter(exerciseAdapter);

        ArrayList<Integer> previewExerciseList = new ArrayList<>();


        //Load the exercise for the corresponding workout
        String loadedExercises = dataWorkoutExercises.get(position);
        Log.i("LoadedExercises", loadedExercises);
        char[] excCharArray = loadedExercises.toCharArray();
        //Add excCharArray to arraylist
        for (char c : excCharArray) {
            String str = c + "";
            int k = Integer.parseInt(str);
            previewExerciseList.add(k);
        }

        for (int f = 0; f < previewExerciseList.size(); f++) {
            Log.i("f23", "" + previewExerciseList.get(f));
        }

        //Since the index here must start from 0, do -1 on the loaedExerxisesKeyLists values when comparing
        for (int y = 0; y < previewExerciseList.size(); y++) {
            for (int x = 0; x < dataExerciseNames.size(); x++) {
                if (x == (previewExerciseList.get(y) - 1)) {
                    //Add the exercises to the selected list
                    Tab3Exercise tab3Exercise = new Tab3Exercise(dataExerciseNames.get(x), dataSets.get(x));
                    exerciseList.add(tab3Exercise);
                }
            }
        }
        exerciseAdapter.notifyDataSetChanged();



        // If the PopupWindow should be focusable
        popupWindow.setFocusable(true);
        // If you need the PopupWindow to dismiss when when touched outside
        popupWindow.setBackgroundDrawable(new ColorDrawable());
        int location[] = new int[2];
        // Get the View's(the one that was clicked in the Fragment) location
        view.getLocationOnScreen(location);
        // Using location, the PopupWindow will be displayed right under
        // anchorView
        popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, location[0], location[1] + view.getHeight());
    }




    /// _______________________________________________________________________________________________________________________

    /// ======THESE METHODS CONTAIN THE CONTENT THAT EITHER LOADS OR SAVES DATA
    /// FROM BOTH TABLES==========================
    // Get keys from EXERCISE_TABLE

    public void retrieveExerciseKeys() {
        Cursor excKeyCursor;
        try {
            excKeyCursor = excData.getAllData();

            // Check for data
            if (excKeyCursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();
            } else {
                while (excKeyCursor.moveToNext()) {
                    // Get data from index //GET ID WHICH IS POSITION 0
                    dataExerciseNames.add(excKeyCursor.getString(1));
                    dataSets.add("Sets "+excKeyCursor.getString(2));
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "No Data Loaded", Toast.LENGTH_SHORT).show();
        }
    }

    // Get all data from EXERCISE_TABLE
    public void retrieveExercise() {
        // Load everything into arraylists
        // ============================================================================================================
        // Use an EXERCISE_TABLE arraylist for the ListView items

        // Retrieve data from EXERCISE_TABLE if any into excDataList
        Cursor excCursor;
        try {
            excCursor = excData.getAllData();

            // Check for data
            if (excCursor.getCount() == 0) {
                // Toast.makeText(getActivity(), "NO DATA",
                // Toast.LENGTH_SHORT).show();
            } else {
                // Toast.makeText(getActivity(), "Data is present in
                // TAB3-Popup", Toast.LENGTH_SHORT).show();
                while (excCursor.moveToNext()) {
                    // Get data
                    loadedArrayList.add(excCursor.getString(1) + " (SETS:" + excCursor.getString(2) + ")");
                    leftAdapter.notifyDataSetChanged();
                    Toast.makeText(getActivity(), "THE COUNT IS" + excCursor.getCount(), Toast.LENGTH_SHORT).show();


                }
            }
        } catch (Exception e) {
            // Toast.makeText(getActivity(), "No Data Loaded",
            // Toast.LENGTH_SHORT).show();
        }
    }

    public void retrieveWorkouts() {
        Cursor wCursor;
        try {
            wCursor = workoutData.getAllData();

            // Check for data
            if (wCursor.getCount() == 0) {
                // Toast.makeText(getActivity(), "NO DATA",
                // Toast.LENGTH_SHORT).show();
            } else {
                // Toast.makeText(getActivity(), "Data is present in
                // TAB3-Popup", Toast.LENGTH_SHORT).show();
                while (wCursor.moveToNext()) {
                    // Get data
                    dataWorkoutKeys.add(wCursor.getString(0));
                    dataWorkoutNames.add(wCursor.getString(1));
                    dataWorkoutExercises.add(wCursor.getString(2));
                    //workoutList.add(wCursor.getString(1) + " - Exercise keys: " + wCursor.getString(2));
                    workoutList.add(wCursor.getString(1));

                    Log.i("WorkoutCursor", wCursor.getString(2) + "");
                    listAdapter.notifyDataSetChanged();
                }

                for (int i = 0; i < dataWorkoutKeys.size(); i++) {
                    Log.i("WorkoutDataKeys", dataWorkoutKeys.get(i));
                }
            }
        } catch (Exception e) {
            // Toast.makeText(getActivity(), "No Data Loaded",
            // Toast.LENGTH_SHORT).show();
        }
    }

	/*
     * // Retrieve data from SETS_TABLE into arraylist setsExcIdList and
	 * setsDataList public void retrieveSets() { Cursor setsCursor; try {
	 * setsCursor = setsData.getAllData();
	 * 
	 * // Check for data if (setsCursor.getCount() == 0) {
	 * Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show(); }
	 * else { while (setsCursor.moveToNext()) { // Get data from index
	 * setsExcIdList.add(setsCursor.getString(1)); setsDataList.add("Reps: " +
	 * setsCursor.getString(2) + "  Weight: " + setsCursor.getString(3)); } } }
	 * catch (Exception e) { Toast.makeText(getActivity(), "No Data Loaded",
	 * Toast.LENGTH_SHORT).show(); } }
	 */

    // Retrieve data from EXERCISE_TABLE if any into excDataList

}
