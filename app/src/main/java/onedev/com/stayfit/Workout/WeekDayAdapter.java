package onedev.com.stayfit.Workout;

/**
 * Created by Juan on 1/1/2017.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import onedev.com.stayfit.R;

public class WeekDayAdapter extends ArrayAdapter {
    private ArrayList<Tab2WeekWorkouts>weekWorkouts;
    private int res;
    private LayoutInflater inflater;
    private Context context;


    public WeekDayAdapter(Context context, int resourceId, ArrayList<Tab2WeekWorkouts> weekWorkouts) {

        super(context, resourceId, weekWorkouts);
        this.weekWorkouts = weekWorkouts;
        res = resourceId;
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //Inflate a new layout view
        convertView = (LinearLayout) inflater.inflate(res, null);

        TextView txtName = (TextView) convertView.findViewById(R.id.tvWeekDay);
        txtName.setText(weekWorkouts.get(position).getDay());

        TextView tvWorkouts = (TextView) convertView.findViewById(R.id.tvWeekItem);
        tvWorkouts.setText(weekWorkouts.get(position).getWorkout());

        return convertView;
    }


    @Override
    public Object getItem(int position) {

        return super.getItem(position);
    }
}



