package onedev.com.stayfit.Workout;

import java.io.Serializable;

public class Sets implements Serializable {
	
	private String labelSet = "";
	private String reps = "";
	private String weight = "";
	
	
	public Sets(String labelSet, String reps, String weight) {
		this.setLabelSet(labelSet);
		this.setReps(reps);
		this.setWeight(weight);
	}
	
	public String getLabelSet() {
		return labelSet;
	}
	public void setLabelSet(String labelSet) {
		this.labelSet = labelSet;
	}
	public String getReps() {
		return reps;
	}
	public void setReps(String reps) {
		this.reps = reps;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	
	
}
