package onedev.com.stayfit.Workout;

/**
 * Created by Juan on 1/4/2017.
 */

public class Tab3Exercise {
    private String name;
    private String sets;

    public Tab3Exercise(String name, String sets) {
        this.name = name;
        this.sets = sets;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSets() {
        return sets;
    }

    public void setSets(String sets) {
        this.sets = sets;
    }
}
