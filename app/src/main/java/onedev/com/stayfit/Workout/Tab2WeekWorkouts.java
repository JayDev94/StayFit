package onedev.com.stayfit.Workout;

/**
 * Created by Juan on 1/4/2017.
 */

public class Tab2WeekWorkouts {
    private String workout;
    private Integer dayPosition;

    public Tab2WeekWorkouts(String workout, Integer dayPosition) {
        this.workout = workout;
        this.dayPosition = dayPosition;
    }

    public String getWorkout() {
        return workout;
    }

    public void setWorkout(String workout) {
        this.workout = workout;
    }

    public String getDay() {
        String day = "";
        switch (dayPosition) {
            case 0:
                day =  "SUN";
            break;
            case 1:
                day =  "MON";
            break;
            case 2:
                day =  "TUE";
            break;
            case 3:
                day =  "WED";
            break;
            case 4:
                day =  "THU";
            break;
            case 5:
                day =  "FRI";
            break;
            case 6:
                day =  "SAT";
            break;
        }
        return day;
    }

    public void setDay(Integer dayPosition) {
        this.dayPosition = dayPosition;
    }

    public void ifEmpty(String name){

    }
}
