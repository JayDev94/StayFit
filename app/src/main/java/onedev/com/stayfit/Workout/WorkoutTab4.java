package onedev.com.stayfit.Workout;
/* There are various bugs that correspond to invalid syntax accross the code from accidental misplacements
 * and unintentional indents
  **/

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;

import onedev.com.stayfit.R;
import onedev.com.stayfit.WorkoutDatabase.ExerciseData;
import onedev.com.stayfit.WorkoutDatabase.SetsData;
import onedev.com.stayfit.WorkoutDatabase.WorkoutDataEdit;


import android.app.ActionBar.LayoutParams;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import static android.R.layout.simple_list_item_1;

public class WorkoutTab4 extends Fragment {
    // Fragment content
    Fragment fragment1 = null, fragment2 = null;
    Fragment currentFragment = null;
    FragmentManager fm;
    FragmentTransaction fragmentTransaction;

    // Database tables and content
    SetsData setsData;
    ExerciseData exerciseData;
    ArrayList<String> excDataList;
    ArrayList<String> excIDList;
    ArrayList<String> excNameList;
    ArrayList<String> excSetsList;

    ArrayList<String> setIdList;
    ArrayList<String> setsExcIdList;
    ArrayList<String> setsDataList;
    ArrayList<String> excList;
    ArrayList<String> setsRepsList;
    ArrayList<String> setsWeightList;

    // Will hold the keylist of EXERCISE_TABLE and be accessed by exterior
    // classes
    public static ArrayList<Integer> ExcKeyList;

    ArrayAdapter<String> mainAdapter;

    // Tab Content
    Button btnAdd;

    ArrayList<Sets> setList;
    public SetsAdapter adapter;
    int setCount = 0;

    // Popup contents
    EditText editName;
    EditText editSets;
    Spinner spinnerSets;
    Button btnEnter;
    ListView listCreator;
    int setsValue = 1;

    //Popup spinner var
    ArrayAdapter<String> spinnerAdapterTab4;
    ArrayList<String> spinnerArray;

    // holds info from EditTexts
    String exerciseName = "";

    // Popup for sets content
    TextView excLabel;
    ListView popupSetsList;
    ArrayList<String> popupArrayListSets;
    ArrayAdapter<String> setsAdapter;

    // Testing content
    ListView listTest;
    Button btnTestSave;
    Button btnDelete;
    ArrayAdapter<String> testAdapter;
    ArrayList<String> testArrayList;


    /*
     * This view is used to capture the instance created by onViewCreated. This
     * is done so that the view can be restarted from anywhere. The EVERYTHING()
     * method holds everything that would go in onCreateView. Inside of the
     * method is a View which points to v, the view that holds view instance in
     * onViewCreated.
     */
    View ClassView;


    //Listview settings var for exercise popup view
    ArrayAdapter<String> listViewPopAdapter;
    ArrayList<String> setsArrayList;
    int intReps = 0;
    int intWeight = 0;
    //These arraylists hold the reps and weights of the sets in the popup
    ArrayList<Integer> arrayListReps;
    ArrayList<Integer> arrayListWeight;


    //Editing var
    boolean editingSpinner = false;
    boolean resetDone = false;    //This changes to true if the user decides to continue with less sets in the edit functions
    boolean extraSets = false;

    boolean copyDeleteAdd = false;
    boolean deleteAddData = false;
    boolean onlyUpdateData = false;
    boolean onlyAddData = false;


    ArrayList<String> copyExcIdList;
    ArrayList<String> copySetsExcIdList;
    ArrayList<Integer> copyArrayListReps;
    ArrayList<Integer> copyArrayListWeight;
    ArrayList<String> copyExcSetsList;
    ArrayList<String> copySetsArrayList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        return inflater.inflate(R.layout.workout_tab4, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        // Initiate v
        ClassView = view;
        EVERYTHING();
    }

    public void EVERYTHING() {
        View view = ClassView;
        btnAdd = (Button) view.findViewById(R.id.btnAddExercise);

        String DATABASE_NAME = "FitnessDatabase.db";
        File dbFile = getActivity().getDatabasePath(DATABASE_NAME);
        Log.i("THE_DATA_BASE", dbFile.getAbsolutePath());


        // btnDelete = (Button) view.findViewById(R.id.btnDelete);
        ListView listView = (ListView) view.findViewById(R.id.listWorkout);
        // Initialize database
        setsData = new SetsData(getActivity());
        exerciseData = new ExerciseData(getActivity());

        // Initialize arraylists
        excIDList = new ArrayList<>();
        setsExcIdList = new ArrayList<String>();
        setsDataList = new ArrayList<String>();
        excList = new ArrayList<String>();
        ExcKeyList = new ArrayList<Integer>();
        spinnerArray = new ArrayList<String>();
        // ExerciseData content
        excDataList = new ArrayList<String>();
        excNameList = new ArrayList<>();
        excSetsList = new ArrayList<>();
        setsRepsList = new ArrayList<>();
        setsWeightList = new ArrayList<>();
        setIdList = new ArrayList<>();

        //Sets list for TEST list view settings (POPUP SETS CREATOR)
        setsArrayList = new ArrayList<>();
        arrayListReps = new ArrayList<>();
        arrayListWeight = new ArrayList<>();


        //ARRAYLISTS COPY FOR EDIT FUNCTIONS
        copyExcIdList = new ArrayList<String>();
        copySetsExcIdList = new ArrayList<String>();
        copyArrayListReps = new ArrayList<Integer>();
        copyArrayListWeight = new ArrayList<Integer>();
        copyExcSetsList = new ArrayList<>();
        copySetsArrayList = new ArrayList<>();


        mainAdapter = new ArrayAdapter<String>(getActivity(), R.layout.tab4_main_list_item, excDataList);
        listView.setAdapter(mainAdapter);


        //Popup spinner array contents
        spinnerArray = new ArrayList<String>();
        spinnerArray.add("1");
        spinnerArray.add("2");
        spinnerArray.add("3");
        spinnerArray.add("4");
        spinnerArray.add("5");
        spinnerArray.add("6");
        spinnerArray.add("7");
        spinnerArray.add("8");
        spinnerArray.add("9");
        spinnerArray.add("10");

        // -------------------------------------------------------------------------------------------------------
        // Retrieve keys from EXERCISE_TABLE if any into ExcKeyList
        retrieveExerciseKeys();
        // -------------------------------------------------------------------------------------------------------
        // Retrieve all data from EXERCISE_TABLE if any into excListData and use
        // that as the listView items
        retrieveExercise();
        mainAdapter.notifyDataSetChanged();
        // -------------------------------------------------------------------------------------------------------
        // Retieve data from SETS_TABLE, this includes the keys, and info
        retrieveSets();
        // -------------------------------------------------------------------------------------------------------

        // listView item clicked ///THE POSITION STARTS AT 0, make sure to add
        // +1 to match the data
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                // TODO Auto-generated method stub
                Toast.makeText(getActivity(), "Sets Size, " + setsExcIdList.size(), Toast.LENGTH_SHORT).show();

				/*
                 * String comp = ""; for(int x = 0; x< setsExcIdList.size(); x++){
				 * comp = comp + setsExcIdList.get(x)+"\n"; Log.i("Data from Sets",
				 * setsExcIdList.get(x)); } Log.i("The sets data", comp);
				 */

				/*
                 * for(int i = 0; i < setsExcIdList.size(); i++){ int current =
				 * Integer.parseInt(setsExcIdList.get(i));
				 *
				 * if(position+1 == current){ Log.i("SETS-REPS",
				 * setsDataList.get(i)); }else{ Toast.makeText(getActivity(),
				 * "No Data Present", Toast.LENGTH_SHORT).show(); } }
				 */
                showPopupSets(v, position, excDataList.get(position));
            }
        });

        ///On Long click item LISTVIEW
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
                final Dialog openDialog = new Dialog(getActivity());
                openDialog.setContentView(R.layout.edit_dialog);
                openDialog.setTitle("EDIT");
                Button btnDelete = (Button) openDialog.findViewById(R.id.btnDialogDelete);
                Button btnEdit = (Button) openDialog.findViewById(R.id.btnDialogEdit);
                Button btnCheckData = (Button) openDialog.findViewById(R.id.btnDialogTestCheckData);

                btnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        WorkoutDataEdit workoutDataEdit = new WorkoutDataEdit(getActivity());
                        workoutDataEdit.deleteExercise(position);
                        openDialog.dismiss();
                    }
                });

                btnEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Show the popup creator to edit data
                        showCreatorEditPopup(view, true, position);
                        //Set the edit spinner to falso on popup start
                        //editingSpinner = false;
                        openDialog.dismiss();
                    }
                });

                //Checking for the existence of data in workouts table
                btnCheckData.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        WorkoutDataEdit we = new WorkoutDataEdit(getActivity());
                        we.tabsSettings(position);
                    }
                });

                openDialog.show();


                return false;
            }

        });

        // Load the sets with the clicked Items key onto the settings
        // listView.setOnItemClickListener(listener);

        // On btnAdd clicked
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                showCreatorEditPopup(v, false, -1);    //Since its only adding data, no need to load or give position (false, -1)
            }
        });

        // -------------------------------------------------------------------------------------------------------

        // TESTINGS TOOL: btnDelete Delete everything (BOTH TABLES)
        /*
         * btnDelete.setOnClickListener(new View.OnClickListener() {
		 *
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub setsData.deleteTable(); exerciseData.deleteTable(); // Restart
		 * the fragment view EVERYTHING(); } });
		 */

    }

    /// _______________________________________________________________________________________________________________________
    // This is the PopUp editor for the exercise creator module   ////ON EDIT FUNCTION > LOAD UI WITH CORRESPONDING DATA
    public void showCreatorEditPopup(View view, final boolean loadData, final int dataPosition) {
        final View popupView = getActivity().getLayoutInflater().inflate(R.layout.workout_creator_popup, null);
        final PopupWindow popupWindow = new PopupWindow(popupView, LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT);

        editName = (EditText) popupView.findViewById(R.id.editCreatorName);
        spinnerSets = (Spinner) popupView.findViewById(R.id.spinnerTab4);
        btnEnter = (Button) popupView.findViewById(R.id.btnCreatorEnter);
        listCreator = (ListView) popupView.findViewById(R.id.listWorkoutCreator);
        btnTestSave = (Button) popupView.findViewById(R.id.btnTestSave);

        listViewPopAdapter = new ArrayAdapter<String>(getActivity(), simple_list_item_1, setsArrayList);
        listCreator.setAdapter(listViewPopAdapter);
        // Setup spinner adapter and array contents
        spinnerAdapterTab4 = new ArrayAdapter<String>(getActivity(), R.layout.simple_big_text_item, spinnerArray);
        spinnerAdapterTab4.setDropDownViewResource(R.layout.simple_big_text_item);
        spinnerSets.setAdapter(spinnerAdapterTab4);

        //Clear the lists for use
        setsArrayList.clear();
        arrayListReps.clear();
        arrayListWeight.clear();


        ///////USED FOR EDIT FUNCTION
        final ArrayList<Integer> editArrayListReps = new ArrayList<>();
        final ArrayList<Integer> editArrayListWeight = new ArrayList<>();

        //Go ahead an set these to false every time the popup is shown
        onlyAddData = false;
        onlyUpdateData = false;
        copyDeleteAdd = false;
        deleteAddData = false;


        //If the data is being loaded WHICH MEANS its being edited, set theses to true then decide later which to use
        if (loadData == true) {
            onlyUpdateData = true;
        }
        resetDone = false;


        //--------------------IF EDIT FUNCTION:::: LOAD UI WITH CORRESPONDING DATA::::
        if (loadData == true) {
            editName.setText(excNameList.get(dataPosition).toString());
            spinnerSets.setSelection(Integer.parseInt(excSetsList.get(dataPosition)) - 1);
            //listCreator =;

            //Update the current arraylist item
            //setsArrayList.remove(position);
            for (int i = 0; i < setsExcIdList.size(); i++) {
                if (Integer.parseInt(setsExcIdList.get(i)) == Integer.parseInt(excIDList.get(dataPosition))) {
                    setsArrayList.add(setsDataList.get(i));
                    // arrayListReps.add(Integer.parseInt(setsRepsList.get(i)));
                    // arrayListWeight.add(Integer.parseInt(setsWeightList.get(i)));
                }
            }
            listViewPopAdapter.notifyDataSetChanged();

            //setsArrayList.add("Reps: " + intReps + "   Weight: " + intWeight);
            //arrayListReps.add(intReps);
            //arrayListWeight.add(intWeight);
        }


        ///Hide the keyboard if ENTER is pressed
        editName.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    if (v != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }

                    return true;
                }
                return false;
            }
        });


        spinnerSets.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Set the number of sets to the selected option in spinner
                setsValue = position + 1;
                setCount = setsValue;
                Toast.makeText(getActivity(), "Set count: " + setCount, Toast.LENGTH_SHORT).show();

                //Make sure to set the save instance of the updated list to also save into the database
                /*Implement error functions to make sure and protect any exceptions from being thrown*/
                //Clear the arraylists of sets, reps of sets, and weight of sets to refresh
                // Loop decides adapter items
                ///IF IN EDIT FUNCTION AND DATA  IS LOADED, IGNORE THE AUTO CHANGES AND SET THE LOADED INSTEAD


                if (!(loadData == true)) {
                    onlyAddData = true;
                    copyDeleteAdd = false;
                    setsArrayList.clear();
                    arrayListReps.clear();
                    arrayListWeight.clear();
                    for (int a = 0; a < setCount; a++) {
                        //setsArrayList.clear();
                        //arrayListReps.clear();
                        // arrayListWeight.clear();
                        setsArrayList.add("Reps------------------------------- Weight");
                    }
                    listViewPopAdapter.notifyDataSetChanged();   ///Notify the adapter for the listview that the spinner resulted in i sets
                } else {
                    if (setCount < Integer.parseInt(excSetsList.get(dataPosition))) {
                        if (resetDone == true) {
                            resetDone = false;
                        } else if (resetDone == true) {
                            resetDone = true;
                        }
                        if (resetDone == false) {            ///if this is true, the dialog has already been shown and changes have been made
                            //if (editingSpinner == true) {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Warning!")
                                    .setMessage("Setting less sets than those that exist will reset them. Do you wan to continue?")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with reset sets
                                            //Copy the arraylists to mix the orignal when need on CopyDeleteAdd = true function
                                            copySetsArrayList.addAll(setsArrayList);
                                            copyArrayListReps.addAll(arrayListReps);
                                            copyArrayListWeight.addAll(arrayListWeight);
                                            setsArrayList.clear();
                                            arrayListReps.clear();
                                            arrayListWeight.clear();

                                            for (int a = 0; a < setCount; a++) {

                                                setsArrayList.add("Reps------------------------------- Weight");
                                            }
                                            resetDone = true;
                                            //SINCE THE LISTVIEW HAS BEEN RESET, SET THE deleteAddData TO TRUE and UPDATE TO FALSE
                                            deleteAddData = true;
                                            onlyUpdateData = false;

                                            listViewPopAdapter.notifyDataSetChanged();
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            ///////NOTHING FOR THE CANCEL
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                            editingSpinner = true;
                            listViewPopAdapter.notifyDataSetChanged();
                            //   }
                        }
                    } else {
                        //Clear the arraylist for use with the loaded data and remainder
                        setsArrayList.clear();
                        arrayListReps.clear();
                        arrayListWeight.clear();
                        //Update the current arraylist item
                        //setsArrayList.remove(position);
                        for (int i = 0; i < setsExcIdList.size(); i++) {
                            if (Integer.parseInt(setsExcIdList.get(i)) == Integer.parseInt(excIDList.get(dataPosition))) {
                                setsArrayList.add(setsDataList.get(i));
                                arrayListReps.add(Integer.parseInt(setsRepsList.get(i)));
                                arrayListWeight.add(Integer.parseInt(setsWeightList.get(i)));
                            }
                        }
                        listViewPopAdapter.notifyDataSetChanged();


                        //Check to see if the setCount is higher then the actual loaded
                        //if so, set the copyDeleteUpdateAdd to true
                        if (setCount > Integer.parseInt(excSetsList.get(dataPosition))) {
                            copyDeleteAdd = true;
                            onlyUpdateData = false;
                            //Copy the contents of the excSetsList to a new arraylist.
                            copyExcSetsList.clear();
                            copyExcSetsList.addAll(excSetsList);


                        }


                        int remainderSetCount = setCount - Integer.parseInt(excSetsList.get(dataPosition));
                        Log.i("RemaindarCount", remainderSetCount + "");

                        for (int a = 0; a < remainderSetCount; a++) {
                            //.add("Reps------------------------------Weight");
                            setsArrayList.add("Reps------------------------------- Weight");                   ///////////////////YOU ARE HERE////////////////////////
                            listViewPopAdapter.notifyDataSetChanged();
                        }
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //List view item selection settings (SETUP REPS AND WEIGHT FOR CORRESPONDING SET)
        listCreator.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                //Alert builder settings
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                //Create and inflator to setup the custom dialog layout
                LayoutInflater layoutInflator = LayoutInflater.from(getActivity());

                //Get the customized dialog layout
                final View editTextsView = layoutInflator.inflate(R.layout.two_edittexts_dialog, null);
                final EditText weightInput = (EditText) editTextsView.findViewById(R.id.editWeightText);
                final Spinner repsSpinner = (Spinner) editTextsView.findViewById(R.id.spinnerReps);

                //Set up the reps spinner
                ArrayAdapter<String> repsAdapter;
                ArrayList<String> repsArrayList = new ArrayList<String>();
                //Populate the arraylist of reps
                for (int h = 1; h <= 50; h++) {
                    repsArrayList.add(h + "");
                }
                repsAdapter = new ArrayAdapter<String>(getActivity(), simple_list_item_1, repsArrayList);
                repsSpinner.setAdapter(repsAdapter);

                //Set the item seletion for the reps
                repsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Toast.makeText(getActivity(), (position + 1) + " Reps", Toast.LENGTH_SHORT).show();
                        //Set the reps to the current selection
                        intReps = (position + 1);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                weightInput.setText("", TextView.BufferType.EDITABLE);


                alert.setView(editTextsView).setPositiveButton("Save",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Get the weight
                                if (!(weightInput.getText().toString().isEmpty())) {
                                    String strWeight = weightInput.getText().toString();
                                    intWeight = Integer.parseInt(strWeight);
                                    //Update the current arraylist item
                                    setsArrayList.remove(position);
                                    setsArrayList.add("Reps: " + intReps + "   Weight: " + intWeight);
                                    arrayListReps.add(intReps);
                                    arrayListWeight.add(intWeight);
                                    if (loadData == true) {
                                        editArrayListReps.add(intReps);
                                        editArrayListWeight.add(intWeight);
                                    }

                                    listViewPopAdapter.notifyDataSetChanged();
                                }
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alert.show();


                //Create a text watcher to retrieve any text that is in the edit box when the save button is clicked
                editName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        exerciseName = editName.getText().toString();

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        //Set the exerciseName to what ever is in the edit box at the time
                        exerciseName = editName.getText().toString();
                        Log.i("OnTextChanged", exerciseName + "");
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        exerciseName = editName.getText().toString();
                    }
                });


            }

        });


        // On btnEnter clicked
        btnEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

            }
        });
        // =======================================================================================================================
        // TESTING On clicked btnTestSave
        /*-Name and Set amount will go into both the EXERCISE_TABLE and ExcKeyList(only the set amount).
         *-Each set will go into SETS_TABLE with the last item of ExcKeyList
		 *
		 */
        btnTestSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (exerciseName.equals("") || exerciseName == null) {  ////|| arrayListReps.size() < setCount || arrayListWeight.size() < setCount
                    Log.i("RepsAndWeightSize", "RepsListSize: " + arrayListReps.size() + "  WeightListSize: " + arrayListWeight.size());
                    Log.i("ErrorEditText", "Error, please input the exercise name.");
                } else {

                    /***************************************************ADD DATA*********************************************************/
                    if (onlyAddData == true) {
                        ////ADD EXERCISE
                        exerciseName = editName.getText().toString();
                        exerciseData.insertData(exerciseName, setCount);
                        if (resetDone == true) {
                            WorkoutDataEdit workoutDataEdit = new WorkoutDataEdit(getActivity());
                            workoutDataEdit.deleteExercise(dataPosition);
                        }
                        // now Add ghost value into ExcKeyList to represent the newly
                        // added data above   //////THIS HAS THE BE THE LAST KEY SAVED IN THE TABLE
                        int ghostValue2 = 1;   // ExcKeyList.size() + 1;
                        int maxVal = 0;
                        for (int g = 0; g < ExcKeyList.size(); g++) {
                            int currentVal = ExcKeyList.get(g);
                            if (currentVal > maxVal) {
                                maxVal = currentVal;
                            }
                        }
                        ghostValue2 = maxVal + 1;
                        //ExcKeyList.add(ghostValue);
                        //ADD THE SETS
                        ////ADD THE SETS
                        for (int a = 0; a < setCount; a++) {
                            // Convert the items above to int
                            int intReps = arrayListReps.get(a);
                            int intWeight = arrayListWeight.get(a);

                            Log.i("SizeOfLists", "RepsSize " + arrayListReps.size() + "  WeightSize " + arrayListWeight.size());

                            // --------------------------------------------------------
                            // Save data into SETS_TABLE and Use the ghost value above for the SETS_TABLE foreign key arg

                            //ON ADD FUNCTION
                            if (loadData == false) {
                                setsData.insertData(ghostValue2, intReps, intWeight);
                            }
                        }
                    }
                    /********************************************************************************************************************/
                    //____________________________________________________________________________________________________________________


                    /***************************************************UPDATE DATA ONLY*************************************************/
                    if (onlyUpdateData == true) {
                        // EDIT EXERCISE NAME AND SET COUNT
                        int intDataPosition = dataPosition;
                        String intUpdatePosition = "" + (dataPosition + 1);
                        Log.i("GetName", "" + excNameList.get(intDataPosition));
                        //Reload the edit name get text
                        exerciseName = editName.getText().toString();
                        Log.i("NameToSave", "  " + exerciseName);
                        exerciseData.updateData(excIDList.get(dataPosition), exerciseName, setCount);
                        //if extra sets were added, insert the extra corresponding data

                        // EDIT EXERCISE REPS AND WEIGHT FOR SETS
                        int indexRP = 0;
                        int updatedDataPosition = dataPosition + 1;
                        for (int e = 0; e < setsExcIdList.size(); e++) {
                            Log.i("OnLoadData", "DataPosition: " + updatedDataPosition + "  setsExcIdList: " + setsExcIdList.get(e));
                            if (updatedDataPosition == Integer.parseInt(setsExcIdList.get(e))) {
                                Log.i("ItDoesEqual", updatedDataPosition + "");
                                int loadIndex = e;
                                int editReps = arrayListReps.get(indexRP);
                                int editWeight = arrayListWeight.get(indexRP);
                                indexRP++;
                                Log.i("MockUpdate", "INDEX: " + e + "      DATAPOSITION: " + updatedDataPosition + "\n (" + setIdList.get(loadIndex) + "), ("
                                        + setsExcIdList.get(loadIndex) + "), (" + editReps + ", (" + editWeight + ")");
                                //Update data (((TESTING))))
                                setsData.updateData(setIdList.get(loadIndex) + "", setsExcIdList.get(loadIndex), editReps, editWeight);
                            }
                        }
                        arrayListReps.clear();
                        arrayListWeight.clear();

                    }

                    /********************************************************************************************************************/
                    //____________________________________________________________________________________________________________________


                    /***************************************************COPY, DELETE,  ADD***********************************************/
                    if (copyDeleteAdd == true) {
                        ///////////////////////////////////////////////////////COPY DATA
                        copyExcIdList.addAll(excIDList);
                        copySetsExcIdList.addAll(setsExcIdList);


                        //copyArrayListReps.addAll(arrayListReps);
                        //copyArrayListWeight.addAll(arrayListWeight);

                        ///////////////////////////////////////////////////////DELETE DATA
                        WorkoutDataEdit workoutDataEdit = new WorkoutDataEdit(getActivity());
                        workoutDataEdit.deleteExercise(dataPosition);
                        /////////////////////////////////////////////////////////ADD DATA
                        ////ADD EXERCISE
                        ////ADD EXERCISE
                        exerciseName = editName.getText().toString();
                        exerciseData.insertData(exerciseName, setCount);
                        // now Add ghost value into ExcKeyList to represent the
                        // newly
                        // added data above   //////THIS HAS THE BE THE LAST KEY SAVED IN THE TABLE
                        int ghostValue2 = 1;   // ExcKeyList.size() + 1;
                        int maxVal = 0;
                        for (int g = 0; g < ExcKeyList.size(); g++) {
                            int currentVal = ExcKeyList.get(g);
                            if (currentVal > maxVal) {
                                maxVal = currentVal;
                            }
                        }
                        ghostValue2 = maxVal +1;         ////*************VALUES FROM OTHER EXERCISES ARE BEING SAVED WITH THE NEW ONES********

                        //ExcKeyList.add(ghostValue);
                        //ADD THE SETS
                        ////ADD THE SETS
                        Log.i("OriginalSize1", setsRepsList.size() + "");
                        Log.i("BeforeCopySize1", copyArrayListReps.size() + "");
                        //Add the original sets first

                        //Add the new sets now
                        copyArrayListReps.addAll(arrayListReps);
                        copyArrayListWeight.addAll(arrayListWeight);
                        Log.i("AfterCopySize1", copyArrayListReps.size() + "");

                        int newSetCount = copyArrayListReps.size();

                        for (int a = 0; a < newSetCount; a++) {
                            // Convert the items above to int
                            int intReps = copyArrayListReps.get(a);
                            int intWeight = copyArrayListWeight.get(a);

                            Log.i("IntRepsIntWeight", "GhostValue: " + ghostValue2 + "   intReps: " + intReps + "  intWeight: " + intWeight);
                            // --------------------------------------------------------
                            // Save data into SETS_TABLE and Use the ghost value above for the SETS_TABLE foreign key arg
                            //ON ADD FUNCTION
                            setsData.insertData(ghostValue2, intReps, intWeight);
                        }
                    }
                    /********************************************************************************************************************/
                    //____________________________________________________________________________________________________________________


                    /*******************



                     // Save into EXERCISE_TABLE (name, sets) //THIS IS DONE OUTSIDE
                     // OF THE LOOP SO THAT ONLY ONE INSTANCE IS SAVED

                     //ON ADD FUNCTION -----------------------------------------------------------------ADD DATA
                     if (loadData == false || resetDone == true) {
                     /*exerciseName = editName.getText().toString();
                     exerciseData.insertData(exerciseName, setCount);
                     if (resetDone == true) {
                     WorkoutDataEdit workoutDataEdit = new WorkoutDataEdit(getActivity());
                     workoutDataEdit.deleteExercise(dataPosition);
                     }*
                     } else if (loadData == true && resetDone == false) {
                     //ON EDIT FUNCTION ------------------------------------------------------------EDIT DATA
                     int intDataPos = dataPosition;
                     String intUpdateDataPos = "" + (dataPosition + 1);
                     Log.i("GetName", "" + excNameList.get(intDataPos));
                     //Reload the edit name get text
                     exerciseName = editName.getText().toString();
                     Log.i("NameToSave", "  " + exerciseName);
                     exerciseData.updateData(excIDList.get(dataPosition), exerciseName, setCount);
                     //if extra sets were added, insert the extra corresponding data
                     }
                     //------------------------------------------------------------------------------------------



                     ////UPDATE SETS DATA   (((((OMG IT FINALLY FUCKING WORKED)))))))  :)  ------------------------SETS DATA UPDATE
                     if (loadData == true && resetDone == false) {
                     int indexRP2 = 0;
                     int updateDataPos2 = dataPosition + 1;
                     for (int e = 0; e < setsExcIdList.size(); e++) {
                     Log.i("OnLoadData", "DataPosition: " + updateDataPos2 + "  setsExcIdList: " + setsExcIdList.get(e));
                     if (updateDataPos2 == Integer.parseInt(setsExcIdList.get(e))) {
                     Log.i("ItDoesEqual", updateDataPos2 + "");
                     int loadIndex = e;
                     int editReps = arrayListReps.get(indexRP2);
                     int editWeight = arrayListWeight.get(indexRP2);
                     indexRP2++;
                     Log.i("MockUpdate", "INDEX: " + e + "      DATAPOSITION: " + updateDataPos2 + "\n (" + setIdList.get(loadIndex) + "), ("
                     + setsExcIdList.get(loadIndex) + "), (" + editReps + ", (" + editWeight + ")");
                     //Update data (((TESTING))))
                     setsData.updateData(setIdList.get(loadIndex) + "", setsExcIdList.get(loadIndex), editReps, editWeight);
                     }
                     }
                     }


                     ******************************/


                    // Load the parent fragment (WorkoutTab4) again to update
                    // data
                    // in listView
                    if (currentFragment == null) {
                    } else {
                        fragmentTransaction.remove(currentFragment);
                    }
                    // Restart the fragment view
                    EVERYTHING();
                    popupWindow.dismiss();
                }
            }
        });

		/*
         * ///Update the main ListView (which is outside of the popup)
		 * retrieveExercise(); mainAdapter.notifyDataSetChanged();
		 */
        // =======================================================================================================================


        // If the PopupWindow should be focusable
        popupWindow.setFocusable(true);
        // If you need the PopupWindow to dismiss when when touched outside
        popupWindow.setBackgroundDrawable(new ColorDrawable());
        int location[] = new int[2];
        // Get the View's(the one that was clicked in the Fragment) location
        view.getLocationOnScreen(location);
        // Using location, the PopupWindow will be displayed right under
        // anchorView
        popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, location[0], location[1] + view.getHeight());
    }
    /// _______________________________________________________________________________________________________________________

    /// _______________________________________________________________________________________________________________________

    // This is the popup of sets for the corresponding exercise
    public void showPopupSets(View view, int position, String label) {
        View popupView = getActivity().getLayoutInflater().inflate(R.layout.tab4_sets_popup, null);
        PopupWindow popupWindow = new PopupWindow(popupView, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        popupSetsList = (ListView) popupView.findViewById(R.id.listPopupSets);
        excLabel = (TextView) popupView.findViewById(R.id.tab3PopupExcLabel);

        popupArrayListSets = new ArrayList<String>();
        setsAdapter = new ArrayAdapter<String>(getActivity(), R.layout.mycustom_list_item, popupArrayListSets);
        popupSetsList.setAdapter(setsAdapter);

        // Set TextView to the current exercise name
        excLabel.setText(label);
        int intExcPosition = position;
        Log.i("POsitionOfList", position + "");
        for (int i = 0; i < setsExcIdList.size(); i++) {
            if (Integer.parseInt(setsExcIdList.get(i)) == Integer.parseInt(excIDList.get(position))) {
                popupArrayListSets.add(setsDataList.get(i));
            }
        }
        setsAdapter.notifyDataSetChanged();

        // If the PopupWindow should be focusable
        popupWindow.setFocusable(true);
        // If you need the PopupWindow to dismiss when when touched outside
        popupWindow.setBackgroundDrawable(new ColorDrawable());
        int location[] = new int[2];
        // Get the View's(the one that was clicked in the Fragment) location
        view.getLocationOnScreen(location);
        // Using location, the PopupWindow will be displayed right under
        // anchorView
        popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, location[0], location[1] + view.getHeight());
    }
    /// _______________________________________________________________________________________________________________________


    /// ======THESE METHODS CONTAIN THE CONTENT THAT EITHER LOADS OR SAVES DATA
    /// FROM BOTH TABLES==========================
    // Get keys from EXERCISE_TABLE
    public void retrieveExerciseKeys() {
        Cursor excKeyCursor;
        try {
            excKeyCursor = exerciseData.getAllData();

            // Check for data
            if (excKeyCursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();
            } else {
                while (excKeyCursor.moveToNext()) {
                    // Get data from index //GET ID WHICH IS POSITION 0
                    ExcKeyList.add(Integer.parseInt(excKeyCursor.getString(0)));
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "No Data Loaded", Toast.LENGTH_SHORT).show();
        }
    }

    // Get all data from EXERCISE_TABLE
    public void retrieveExercise() {
        // Load everything into arraylists
        // ============================================================================================================
        // Use an EXERCISE_TABLE arraylist for the ListView items

        // Retrieve data from EXERCISE_TABLE if any into excDataList
        Cursor excCursor;
        try {
            excCursor = exerciseData.getAllData();

            // Check for data
            if (excCursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();
            } else {
                while (excCursor.moveToNext()) {
                    // Get data from index //GET ID WHICH IS POSITION 0
                    excIDList.add(excCursor.getString(0));
                    excNameList.add(excCursor.getString(1));
                    excSetsList.add(excCursor.getString(2));
                    excDataList.add(excCursor.getString(1) + "     SETS " + excCursor.getString(2));
                }
                for (int i = 0; i < excIDList.size(); i++) {
                    Log.i("ExcIdList", excIDList.get(i) + "");
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "No Data Loaded", Toast.LENGTH_SHORT).show();
        }
    }

    // Retrieve data from SETS_TABLE into arraylist setsExcIdList and setsDataList
    public void retrieveSets() {
        Cursor setsCursor;
        try {
            setsCursor = setsData.getAllData();

            // Check for data
            if (setsCursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();
            } else {
                while (setsCursor.moveToNext()) {
                    // Get data from index
                    setIdList.add(setsCursor.getString(0));
                    setsExcIdList.add(setsCursor.getString(1));
                    setsRepsList.add(setsCursor.getString(2));
                    setsWeightList.add(setsCursor.getString(3));
                    setsDataList.add("Reps: " + setsCursor.getString(2) + "  Weight: " + setsCursor.getString(3));
                }
                for (int i = 0; i < setsExcIdList.size(); i++) {
                    Log.i("TestingsetsExcIdList", setsExcIdList.get(i) + "");
                }

                Log.i("SetExcSize", setsExcIdList.size() + "");

                for (int e = 0; e < setIdList.size(); e++) {
                    Log.i("setIdList", setIdList.get(e) + "");
                }

            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "No Data Loaded", Toast.LENGTH_SHORT).show();
        }
    }


}
