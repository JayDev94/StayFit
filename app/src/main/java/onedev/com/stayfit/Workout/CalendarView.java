package onedev.com.stayfit.Workout;

import onedev.com.stayfit.R;
import onedev.com.stayfit.WorkoutDatabase.CalendarColorData;
import onedev.com.stayfit.WorkoutDatabase.CalendarData;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;

public class CalendarView extends LinearLayout {
    // for logging
    private static final String LOGTAG = "Calendar View";
    // how many days to show, defaults to six weeks, 42 days
    private static final int DAYS_COUNT = 42;
    // default date format
    private static final String DATE_FORMAT = "MMM yyyy";
    // date format
    private String dateFormat;
    // current displayed month
    private Calendar currentDate = Calendar.getInstance();
    // event handling
    private EventHandler eventHandler = null;
    private EventHandlerClick eventHandlerClick = null;

    // internal components
    private LinearLayout header;
    private ImageView btnPrev;
    private ImageView btnNext;
    private TextView txtDate;
    private GridView grid;

    //Holds the position of the date clicked on
    SharedPreferences sharedPrefs;
    int colorPosition;

    //Update calendar var
    HashSet<Date> eventsNew;
    ArrayList<String>colorListArray;


    CalendarData calendarData;
    ArrayList<String> dateList;

    CalendarColorData calendarColorData;
    ArrayList<String> colorList;
    ArrayList<Integer> weekList;

    //The selected weekdays
    ArrayList<Integer> selectedWeekArray;
    //The selected weekdays in type (Date)
    ArrayList<Date> datesSelectedWeekArrayList;
    SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");


    int intSelectedMonthPosition = 0;

    // seasons colors
    int[] seasons = new int[]{R.color.summer, R.color.fall, R.color.winter, R.color.spring};

    // month-season association (northern hemisphere, sorry australia :)
    int[] monthSeason = new int[]{2, 2, 3, 3, 3, 0, 0, 0, 1, 1, 1, 2};

    public CalendarView(Context context) {
        super(context);
    }

    public CalendarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        iControl(context, attrs);
    }

    public CalendarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        iControl(context, attrs);
    }

    /**
     * Load control xml layout
     */
    private void iControl(Context context, AttributeSet attrs) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.control_calendar, this);

        loadDateFormat(attrs);
        assignUiElements();
        assignClickHandlers();

        updateCalendar();
    }

    private void loadDateFormat(AttributeSet attrs) {
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.CalendarView);

        try {
            // try to load provided date format, and fallback to default
            // otherwise
            dateFormat = ta.getString(R.styleable.CalendarView_dateFormat);
            if (dateFormat == null)
                dateFormat = DATE_FORMAT;
        } finally {
            ta.recycle();
        }
    }

    private void assignUiElements() {
        // layout is inflated, assign local variables to components
        header = (LinearLayout) findViewById(R.id.calendar_header);
        btnPrev = (ImageView) findViewById(R.id.calendar_prev_button);
        btnNext = (ImageView) findViewById(R.id.calendar_next_button);
        txtDate = (TextView) findViewById(R.id.calendar_date_display);
        grid = (GridView) findViewById(R.id.calendar_grid);
    }


    private void assignClickHandlers() {
        // add one month and refresh UI
        btnNext.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                currentDate.add(Calendar.MONTH, 1);

                retrieveCalendar();
                updateCalendar(eventsNew);
            }
        });

        // subtract one month and refresh UI
        btnPrev.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                currentDate.add(Calendar.MONTH, -1);

                retrieveCalendar();
                updateCalendar(eventsNew);
            }
        });

        // long-pressing a day
        grid.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> view, View cell, int position, long id) {
                // handle long-press
                if (eventHandler == null)
                    return false;
                retrieveCalendar();
                updateCalendar(eventsNew);

                eventHandler.onDayLongPress((Date) view.getItemAtPosition(position));
                return true;
            }
        });
        // on click a day
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                // handle long-press
                eventHandlerClick.onItemClick((Date) parent.getItemAtPosition(position));
                colorPosition = position;

                //Save the month of the selected day to shardPrefs
                SharedPreferences prefs = getContext().getSharedPreferences("MonthSelected", Context.MODE_PRIVATE);
                Log.i("CurrentDateMonth", currentDate.getTime().getMonth() + "");
                prefs.edit().putInt("MonthSelected", currentDate.getTime().getMonth()).commit();


                view.setBackgroundResource(R.drawable.calendar_selection);
                //------------------------------------------------------IGNORE FOR NOW
                String holdDate = ((Date) parent.getItemAtPosition(position)).getDate()+ "";
                String holdStrDay = ((Date) parent.getItemAtPosition(position)).getDay() + "";
                String holdMonth = ((Date) parent.getItemAtPosition(position)).getMonth() +"";
                String holdYear = ((Date) parent.getItemAtPosition(position)).getYear() +"";


                //TESTING GRID DATE CLICK FOR WEEKLY OPTIONS EDIT FUNCTION************************************************************
                Date dateToUse = ((Date)parent.getItemAtPosition(position));
                datesSelectedWeekArrayList = new ArrayList<Date>();

                int dayPosition = Integer.parseInt(holdStrDay);
                int intDate = Integer.parseInt(holdDate);
                int intMonth = Integer.parseInt(holdMonth);
                int intYear = Integer.parseInt(holdYear);

                //Get the number of days in the selected month
                Calendar myCalendar = new GregorianCalendar(intYear, intMonth, intDate);
                int daysOfMonth = myCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);

                Log.i("TestDateCliked", holdDate + " was clicked on");
                Log.i("TestDaySTRINGclicked", holdStrDay + " was clicked on");

                //Test week selection algorithm
                int weekLength = 6;
                int weekLeft = 0;
                int weekRight = 0;
                int fWeekDay = 0;
                int lWeekDay = 0;

                weekLeft = dayPosition;
                weekRight = weekLength - dayPosition;

                fWeekDay = intDate - weekLeft;
                lWeekDay = intDate + weekRight;

                ////////------------------------------GET DATES INTO ARRAYLIST FIRST------------------
                Log.i("ToLeftOfWeek", weekLeft + "");
                String strPresent = formatter.format(dateToUse);
                Log.i("PresentFirst", strPresent);
                Calendar calendarLeft = Calendar.getInstance();
                Calendar calendarRight = Calendar.getInstance();
                try {
                    calendarLeft.setTime(formatter.parse(strPresent));
                    calendarRight.setTime(formatter.parse(strPresent));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                int dateRight = 0, dateLeft = 0;
                dateLeft = weekLeft;
                dateRight = 6 - dateLeft;
                Log.i("LeftAndRight", "Left: " + dateLeft + "   Right: " + dateRight);
                //Check left dates and decrement
                for (int x = 0; x < dateLeft; x++) {   ///Decrement dates
                    calendarLeft.add(Calendar.DATE, -1);
                    String strDecDate = formatter.format(calendarLeft.getTime());
                    Log.i("PresentOnDecrement", strDecDate);
                    //Add the left dates to the list
                    try {
                        datesSelectedWeekArrayList.add(formatter.parse(strDecDate));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                //Add the present date to the list
                datesSelectedWeekArrayList.add(dateToUse);
                //Check Right dates and increment
                for (int x = 0; x < dateRight; x++) {   ///Increment dates
                    calendarRight.add(Calendar.DATE, +1);
                    String strIncDate = formatter.format(calendarRight.getTime());
                    Log.i("PresentOnIncrement", strIncDate);
                    //Add the right dates to the list
                    try {
                        datesSelectedWeekArrayList.add(formatter.parse(strIncDate));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                for (int x = 0; x < datesSelectedWeekArrayList.size(); x++) {
                    Log.i("DateListCheck", datesSelectedWeekArrayList.get(x).getDate() + "");

                }


                //TESTING GRID DATE CLICK FOR WEEKLY OPTIONS EDIT FUNCTION************************************************************


            /**
             *
             *
             *
             * retrieveCalendar();
                retrieveColorWeekData();
             *
             *
             * */
                retrieveCalendar();
                Log.i("EventsLogOnClick", eventsNew.size()+"");

            }
        });
    }

    public void updateCalendarFromWorkoutTab2(){
        retrieveCalendar();
        Log.i("EventsNewLog3", eventsNew.size()+"");
        updateCalendar(eventsNew);
    }


    /**
     * Display dates correctly in grid
     */
    public void updateCalendar() {
        updateCalendar(null);
    }

    /**
     * Display dates correctly in grid
     */
    public void updateCalendar(HashSet<Date> events) {
        Log.i("UpdatedCalender", "Updated");

        ArrayList<Date> cells = new ArrayList<>();
        Calendar calendar = (Calendar) currentDate.clone();

        // determine the cell for current month's beginning
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        int monthBeginningCell = calendar.get(Calendar.DAY_OF_WEEK) - 1;

        // move calendar backwards to the beginning of the week
        calendar.add(Calendar.DAY_OF_MONTH, -monthBeginningCell);

        // fill cells
        while (cells.size() < DAYS_COUNT) {
            cells.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        // update grid
        grid.setAdapter(new CalendarAdapter(getContext(), cells, events));

        // update title
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        txtDate.setText(sdf.format(currentDate.getTime()));

        // set header color according to current season
        int month = currentDate.get(Calendar.MONTH);
        int season = monthSeason[month];
        int color = seasons[season];

        // Set header color CURRENT IS LIGHT GREEN #7AC47C //THIS IS ALSO THE
        // MAIN TAB COLOR
        header.setBackgroundColor(Color.parseColor("#65cf5d"));
    }





    private class CalendarAdapter extends ArrayAdapter<Date> {
        // days with events
        private HashSet<Date> eventDays;

        // for view inflation
        private LayoutInflater inflater;


        public CalendarAdapter(Context context, ArrayList<Date> days, HashSet<Date> eventDays) {
            super(context, R.layout.control_calendar_day, days);
            this.eventDays = eventDays;


            if(eventDays == null){

            }else {
                Log.i("InsideAdapterEvents", eventDays.size() + "");
            }
            inflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            // day in question
            Date date = getItem(position);
            int day = date.getDate();
            int month = date.getMonth();
            int year = date.getYear();


            // today
            Date today = new Date();

            // inflate item if it does not exist yet
            if (view == null)
                view = inflater.inflate(R.layout.control_calendar_day, parent, false);

            // clear styling
            //Calendar all non selected days ui text settings
            ((TextView) view).setTypeface(null, Typeface.NORMAL);
            ((TextView) view).setTextColor(Color.parseColor("#65cf5d"));

            if (month != today.getMonth() || year != today.getYear()) {
                // if this day is outside current month, grey it out
                ((TextView) view).setTextColor(getResources().getColor(R.color.greyed_out));
            }

            // set text
            ((TextView) view).setText(String.valueOf(date.getDate()));

            //Used to decide if the event is selected
            boolean eventSelected = false;

            // if this day has an event, specify event image
            view.setBackgroundResource(0);



            /*if (eventDays != null) {
                for (Date eventDate : eventDays) {
                    if (eventDate.getDate() == day && eventDate.getMonth() == month && eventDate.getYear() == year) {
                        // mark this day for event
                        view.setBackgroundColor(Color.parseColor("#009BFF"));
                        ((TextView) view).setTextColor(getResources().getColor(R.color.whitetext));
                        ((TextView) view).setTypeface(null, Typeface.BOLD);
                        eventSelected = true;
                        break;
                    } else {
                        eventSelected = false;
                    }
                }
            }*/

            sharedPrefs = getContext().getSharedPreferences("MonthSelected", Context.MODE_PRIVATE);
            int selectedMonth = sharedPrefs.getInt("MonthSelected", -1);

            //TESTING NEW ALGORITHM:::::::::::::::::::::::::::::::::::::::::::::::::
            Log.i("SelectedMonth", selectedMonth + "");
            Calendar c = Calendar.getInstance();
            c.setTime(date);

            int intWeek = c.get(Calendar.WEEK_OF_YEAR);
            Calendar eventCalendar = Calendar.getInstance();


            //THIS CODE CREATES TEMPORARY IMAGES FOR THE SELECTED WEEK
            Calendar currentCalendar = Calendar.getInstance();
            currentCalendar.setTime(date);
            intWeek = currentCalendar.get(Calendar.WEEK_OF_YEAR);

            if(!(datesSelectedWeekArrayList == null)) {
                for (int r = 0; r < 7; r++) {
                    if (date.getDate() == datesSelectedWeekArrayList.get(r).getDate() &&
                            date.getMonth() == datesSelectedWeekArrayList.get(r).getMonth() &&
                            date.getYear() == datesSelectedWeekArrayList.get(r).getYear()) {
                        view.setBackgroundColor(Color.parseColor("#D4FFD4"));
                    }
                }
            }


            //THIS CODE CREATES AN IMAGE FOR EVERY EVENT !!!!!IMPORTANT!!!!
            //TESTING WEEK SELECTION _ DAY SELECTION
            int iWeek = c.get(Calendar.WEEK_OF_YEAR);
            Calendar eCalendar = Calendar.getInstance();
            if (eventDays != null) {
                //Testing in LOOP  USING CONTENTS FROM DATABASE
                ////////TESTING--------------------------------
                int colorIndex = 0;
                for (Date eventDate : eventDays) {
                    eCalendar.setTime(eventDate);
                    /*
                    //USE colorWeek OPTIONS ON HERE TO LOOP THROUGH WEEKS WITH THE SAVED COLOR
                    intWeek = eCalendar.get(Calendar.WEEK_OF_YEAR);
                   if (intWeek == colorWeek.get(colorIndex) S&& eventDate.getDate() == day
                   && eventDate.getMonth() == month
                   && eventDate.getYear() == year){
                    */
                    if (eventDate.getDate() == day && eventDate.getMonth() == month && eventDate.getYear() == year) {
                        if (day == eventDate.getDate() &&
                                month == eventDate.getMonth() && year == eventDate.getYear()) {
                            // mark this day for event
                            view.setBackgroundResource(R.drawable.calendar_events);
                            Log.i("DataIsBeingLoaded", "FUCK");
                            //THE EVENTS FOR WEEKLY COLOR ARE CURRENTLY UNAVAILABLE---------
                            //view.setBackgroundColor(Color.parseColor(colorList.get(colorIndex)));
                            ///----------------------------------------------------------------
                            ((TextView) view).setTextColor(getResources().getColor(R.color.whitetext));
                            ((TextView) view).setTypeface(null, Typeface.BOLD);
                        }
                    }
                }

                //Set today's date's view traits         /////////////////------------------------SETS THE CURRENT DATE VIEW
                if (day == today.getDate() && month == today.getMonth() && year == today.getYear()) {
                    // if it is today, set it to blue/bold
                 //   view.setBackgroundResource(R.drawable.calendar_event_selected);
                    view.setBackgroundResource(R.drawable.calendar_events_current_date);
                    ((TextView) view).setTypeface(null, Typeface.BOLD);
                    ((TextView) view).setTextColor(Color.WHITE);
                    //((TextView) view).setTextSize(23);
                }

                //Moving from the various forms of things
            }
            return view;
        }

    }

    /**
     * Assign event handler to be passed needed events
     */
    public void setEventHandler(EventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }

    public void setEventHandlerClick(EventHandlerClick eventHandlerClick) {
        this.eventHandlerClick = eventHandlerClick;
    }

    /**
     * This interface defines what events to be reported to the outside world
     */
    public interface EventHandler {
        void onDayLongPress(Date date);
    }

    public interface EventHandlerClick {
        void onItemClick(Date date);
    }


    public void retrieveCalendar() {
        calendarData = new CalendarData(getContext());
        dateList = new ArrayList<>();
        eventsNew = new HashSet<>();
        colorList = new ArrayList<String>();
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");

        // Load everything into arraylists
        // ============================================================================================================
        // Use an EXERCISE_TABLE arraylist for the ListView items

        // Retrieve data from EXERCISE_TABLE if any into excDataList
        Cursor cursor, cursorD;
        try {
            cursor = calendarData.getAllData();
            cursorD = calendarData.getAllData();
            // Check for data
            if (cursor.getCount() == 0) {
                // Toast.makeText(getActivity(), "NO DATA",
                // Toast.LENGTH_SHORT).show();
            } else {
                // Toast.makeText(getActivity(), "Data is present in
                // TAB3-Popup", Toast.LENGTH_SHORT).show();
                dateList.clear();
                while (cursor.moveToNext()) {
                    // Get data
                    // This arraylist captuers the dates
                    dateList.add(cursor.getString(1));
                    Log.i("TestingCalendarData",
                            "The date: " + cursor.getString(1) + "| The workout KEY: " + cursor.getString(2));
                }
                eventsNew.clear();
                Log.i("Date-List-Size", "" + dateList.size());
                if (!(dateList.isEmpty())) {
                    for (int i = 0; i < dateList.size(); i++) {
                        // Format the date
                        Date d = null;
                        String sd = dateList.get(i);

                        try {
                            d = formatter.parse(sd);
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            Log.i("Data-Not-Parsed", "Error in format");
                        }
                        eventsNew.add(d);
                    }
                }
                Log.i("RetrieveDataEvent", eventsNew.size()+"");
                // Update calendar right after
                updateCalendar(eventsNew);

            }
        } catch (Exception e) {
            // Toast.makeText(getActivity(), "No Data Loaded",
            // Toast.LENGTH_SHORT).show();
            // Update calendar right after
            Log.i("RetrieveDataEvent2", eventsNew.size()+"");

            updateCalendar(eventsNew);
        }



    }





}

/*
*
*
The weekly color changes will be done after the release of the application.
The future increment with this feature will have functional weekly color options that correspond
to the type of week.

Their is a different approach to the coloring of workouts.  That is by coloring the the workout instead of the week.
This will change every workout to a certain color.  For example: chest/biceps colored in blue would
show up blue every time that it exists in the calendar.
This approach is being evaluated but may certainly be the primary function of the coloring options
for the calendar.
*
*
*/