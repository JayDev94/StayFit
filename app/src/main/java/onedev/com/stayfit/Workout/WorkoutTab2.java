package onedev.com.stayfit.Workout;
/*
 * The calandar class- holds the event creator - saves onto CALENDAR_TABLE 
 * 			PKEY|DATE TEXT |WORKOUT INTEGERs
 * 			___________________________________________
 * 
 *			0	|YYYY-MM-DD| 0...n
 */

import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;


import onedev.com.stayfit.R;
import onedev.com.stayfit.WorkoutDatabase.CalendarColorData;
import onedev.com.stayfit.WorkoutDatabase.CalendarData;
import onedev.com.stayfit.WorkoutDatabase.ExerciseData;
import onedev.com.stayfit.WorkoutDatabase.WorkoutData;
import onedev.com.stayfit.WorkoutDatabase.WorkoutDataEdit;

import android.app.AlertDialog;
import android.app.ActionBar.LayoutParams;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.Toast;

public class WorkoutTab2 extends Fragment {
    // In case a method does not provide a view
    View ClassView;
    View viewPort;
    // Database Calendar Table and Workout Table
    CalendarData calendarData;
    WorkoutData workoutData;
    // Data holder lists
    ArrayList<Integer> calendarDataIDList;
    ArrayList<Integer> dateWorkoutKeyList;
    ArrayList<String> dataDateList;
    ArrayList<String> dataDatesList;
    // Workout Table - name 'Will display in a list view inside the popup'
    ArrayList<String> workoutNameList;
    ArrayList<Integer> workoutIdList;
    ArrayList<Integer> workoutExerciseIdList;
    ArrayList<String> dateIdList;
    // CalenderContents
    ListView listViewSelect;
    ListView listViewWeek;
    Button btnDelete;
    Button btnCalendarOptions;
    Button btnCopyPastWeek;
    Button buttonClearWeek;
    //Calendar options
    Button btnClearWeek;
    Button btnClearMonth;
    Button btnCopyWeek;
    Button btnBottomSheet;
    Spinner colorSpinner;
    ArrayList<String> colorList;
    ArrayAdapter<String> spinnerAdapter;
    //Arraylists from data retrieved
    ArrayList<String> rColorList;
    ArrayList<Integer> rWeekList;
    //Calendar settings
    CalendarView calendarView;
    ArrayList<String> calendarArray;
    ArrayAdapter<String> calendarAdapter;
    // Will contain the date of events
    HashSet<Date> eventsSet;
    // Contains the date clicked on
    Date primaryDate;
    // Date formatter
    // SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
    DateFormat formatter = SimpleDateFormat.getDateInstance();                    ////THIS NEEDS TO BE USED INSTEAD OF SimpleDateFormat FOR ALL DATE SATA INSERTS

    //Selected date workout contents
    ListView lvSelectedWorkout;
    ArrayList<String> workoutList;
    ArrayAdapter<String> workoutAdapter;
    //Exercise contents
    ArrayList<String> exerciseList;
    ArrayList<Integer> exerciseSetsList;
    ExerciseData exerciseData;
    //Lates calander settings- plus database table
    int currentSelectedDate = -1;
    Calendar currentCalander;
    CalendarColorData calendarColorData;
    String primaryColor = "";

    //Edit Class
    WorkoutDataEdit WORKOUT_EDIT;
    //Delete var
    int dateDataPosition = -1;
    //Editing var
    ArrayList<Integer> selectedWeekArrayList;
    ArrayList<Integer> copiedWeekArrayList;
    ArrayList<Date> dateCopiedWeekDatesList;
    ArrayList<Date> dataRetrievedDates;
    ArrayList<Integer> pastedWeekArrayList;
    ArrayList<Tab2WeekWorkouts> weekListViewList;
    ArrayList<Date> intoPasteWeekList;


    boolean boolCopyPaste = false;     //Copy = false  ; Paste = true
    Date selectedDate;   //Holds the selected date
    HashMap<Integer, Integer> copiedWeekMap;


    //Bottom sheet var
    private BottomSheetBehavior bottomSheetBehavior;
    ListView exerciseBottomSheetListView;
    ListView workoutDatesBottomSheetListView;
    ArrayList<String> exerciseBottomSheetList;
    ArrayList<String> workoutDatesBottomSheetList;
    ArrayAdapter<String> exerciseBottomSheetAdapter;
    ArrayAdapter<String> workoutBottomSheetAdapter;
    ArrayAdapter<String> weekListAdapter;

    WeekDayAdapter weekDayAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        return inflater.inflate(R.layout.workout_tab2, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        ClassView = view;
        EVERYTHING();
    }

    public void EVERYTHING() {
        // MAIN VIEW
        View view = ClassView;
        viewPort = view;

        //Edit class initialization
        WORKOUT_EDIT = new WorkoutDataEdit(getActivity());
        //Initialize Data Tables
        exerciseData = new ExerciseData(getActivity());
        calendarColorData = new CalendarColorData(getActivity());


        workoutList = new ArrayList<>();

        // Initiate database table class for use
        calendarData = new CalendarData(getActivity());
        workoutData = new WorkoutData(getActivity());
        calendarView = ((CalendarView) view.findViewById(R.id.calendar_view));
        btnDelete = (Button) view.findViewById(R.id.deleteCalendar);
        btnCalendarOptions = (Button) view.findViewById(R.id.btnCalendarOptions);
        lvSelectedWorkout = (ListView) view.findViewById(R.id.lvWorkoutsTab2);
        btnCopyPastWeek = (Button) view.findViewById(R.id.btnCopyPasteWeek);
        buttonClearWeek = (Button) view.findViewById(R.id.buttonClearWeek);
        listViewWeek = (ListView) view.findViewById(R.id.lvWeekDays);

        //Set the btnCopyPaste & btnClearWeek buttons to GONE by default
        btnCopyPastWeek.setVisibility(View.GONE);
        buttonClearWeek.setVisibility(View.GONE);


        // Initialize the data arraylist
        calendarDataIDList = new ArrayList<>();
        dateWorkoutKeyList = new ArrayList<Integer>();
        dataDateList = new ArrayList<String>();
        workoutIdList = new ArrayList<Integer>();
        workoutNameList = new ArrayList<String>();
        workoutExerciseIdList = new ArrayList<Integer>();
        dateIdList = new ArrayList<String>();
        exerciseList = new ArrayList<>();
        exerciseSetsList = new ArrayList<>();
        colorList = new ArrayList<>();
        rColorList = new ArrayList<>();
        rWeekList = new ArrayList<>();
        selectedWeekArrayList = new ArrayList<>();
        copiedWeekArrayList = new ArrayList<>();
        pastedWeekArrayList = new ArrayList<>();
        dataDatesList = new ArrayList<>();
        dateCopiedWeekDatesList = new ArrayList<>();
        dataRetrievedDates = new ArrayList<>();
        exerciseBottomSheetList = new ArrayList<>();
        workoutDatesBottomSheetList = new ArrayList<>();
        weekListViewList = new ArrayList<>();
        intoPasteWeekList = new ArrayList<Date>();

        //Initialise the hashmap
        copiedWeekMap = new HashMap<Integer, Integer>();

        // Initialize set of events
        eventsSet = new HashSet<>();

        //Clear the arraylists
        dataDateList.clear();
        workoutNameList.clear();
        eventsSet.clear();

        // Retrieve workouts
        retrieveWorkouts();
        // Retrieve the calendarData - dataDateList and workoutKeyList will be
        // updated- UPDATE the eventsList if any
        retrieveCalendar();
        //calendarView.updateCalendar(eventsSet);


        //------------------------------------------------------------------------Bottom sheet setup

        //Exercise bottom sheet listview settings


        //Retrieve exercises
        retrieveExercises();
        Log.i("ExerciseListSize", exerciseList.size() + "");
        Log.i("ExerciseSetsSize", exerciseSetsList.size() + "");


        //Retrieve week color list
        retrieveColorWeekData();
        Log.i("Tab2ColorWeekTest", +rWeekList.size() + "");
        for (int t = 0; t < rWeekList.size(); t++) {
            Log.i("TheColorList", rWeekList.get(t) + "");
        }


        currentCalander = Calendar.getInstance();


        //Set up the workout list adapter
        workoutAdapter = new ArrayAdapter<String>(getActivity(), R.layout.exercise_items_tab2, workoutList);
        lvSelectedWorkout.setAdapter(workoutAdapter);

        //Set up the weeklistview adapter
        /*weekListAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, weekListViewList);
        listViewWeek.setAdapter(weekListAdapter);
        weekListViewList.add("Monday");
        weekListViewList.add("Tuesday");
        weekListViewList.add("Wednesday");
        weekListViewList.add("Thursday");
        weekListViewList.add("Friday");
        weekListViewList.add("Saturday");
        weekListViewList.add("Sunday");
        weekListAdapter.notifyDataSetChanged();*/

        weekDayAdapter = new WeekDayAdapter(getActivity(), R.layout.week_items, weekListViewList);
        listViewWeek.setAdapter(weekDayAdapter);


        ///////////////////////////////////ON WEEK LIST ITEM CLICKED//////////////////////////////////////////////
        listViewWeek.setOnItemClickListener(new OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onEventTapedPressed(dateCopiedWeekDatesList.get(position));
            }
        });




        //////////////////////////////////// ON LONG PRESS DAY  //////////////////////////////////////////////////
        calendarView.setEventHandler(new CalendarView.EventHandler() {
            @Override
            public void onDayLongPress(Date date) {
                // TODO Auto-generated method stub
                reloadData();    ////////////////----RELOAD DATA
                // Show the data pressed
                DateFormat dateF = SimpleDateFormat.getDateInstance();
                Toast.makeText(getActivity(), "Today is" + dateF.format(date), Toast.LENGTH_SHORT).show();

                DateFormat df = SimpleDateFormat.getDateInstance();
                // Format the date and check if it exists in the dataDateList
                String current = "" + df.format(date);

                //Get the position of the data selected to use on the deletion-------------DELETE
                int datePos = dataDateList.indexOf(current);
                Log.i("DatePos", datePos + "");

				/*If the calendarIdList is not null or empty or date position is not -1 then select the date on long press -----------EDIT/DELETE
                for date selection init that will be used for edit/delete functions
				 */
                if (!(calendarDataIDList == null || calendarDataIDList.isEmpty() || datePos == -1)) {
                    for (int x = 0; x < calendarDataIDList.size(); x++) {
                        Log.i("CalendarIdList", calendarDataIDList.get(x) + "");
                    }
                    dateDataPosition = Integer.parseInt(calendarDataIDList.get(datePos) + "");
                    Log.i("TestingDataPressed", dateDataPosition + " is selected");
                }

                //eventsSet.contains(object)
                if (dataDateList.contains(current)) {
                    int in = dataDateList.indexOf(current);
                    Log.i("CurrentDate", "" + dataDateList.indexOf(current));
                    String toDelete = dateIdList.indexOf(dataDateList.indexOf(current)) + "";
                    int workoutID = dateWorkoutKeyList.get(in);
                    Log.i("DateWorkoutKeyList", "" + dateWorkoutKeyList.get(in));
                    for (int i = 0; i < workoutNameList.size(); i++) {
                        Log.i("TheWOrkouts", "" + workoutNameList.get(i));
                    }
                    Toast.makeText(getActivity(), workoutNameList.get(workoutID - 1) + "   WORKOUT", Toast.LENGTH_SHORT).show();
                    alertDialog(toDelete);
                } else {
                    Toast.makeText(getActivity(), "No Workout Scheduled",
                            Toast.LENGTH_SHORT).show();
                    // ShowPopup //Contains the updateEvents method which will have
                    // a copy of this current date through pimaryDate
                    primaryDate = date;
                    showPopup(viewPort);
                }
            }
        });

        /////////////////////////////////////////ON CLICK DAY//////////////////////////////////////////////////////////////////////////////
        calendarView.setEventHandlerClick(new CalendarView.EventHandlerClick() {
            @Override
            public void onItemClick(Date date) {
                reloadData(); /// -----------RELOAD DATA

                //TESTING GRID DATE CLICK FOR WEEKLY OPTIONS EDIT FUNCTION************************************************************
                dateCopiedWeekDatesList.clear();
                selectedWeekArrayList.clear();
                String holdStrDay = date.getDay() + "";
                int dayPosition = Integer.parseInt(holdStrDay);

                //Test week selection algorithm
                int weekLeft = 0;
                weekLeft = dayPosition;
                ////////------------------------------GET DATES INTO ARRAYLIST FIRST------------------
                Log.i("ToLeftOfWeek", weekLeft + "");
                String strPresent = formatter.format(date);
                Log.i("PresentFirst", strPresent);
                Calendar calendarLeft = Calendar.getInstance();
                Calendar calendarRight = Calendar.getInstance();
                try {
                    calendarLeft.setTime(formatter.parse(strPresent));
                    calendarRight.setTime(formatter.parse(strPresent));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                int dateRight = 0, dateLeft = 0;
                dateLeft = weekLeft;
                dateRight = 6 - dateLeft;
                Log.i("LeftAndRight", "Left: " + dateLeft + "   Right: " + dateRight);
                //Check left dates and decrement
                for (int x = 0; x < dateLeft; x++) {   ///Decrement dates
                    calendarLeft.add(Calendar.DATE, -1);
                    String strDecDate = formatter.format(calendarLeft.getTime());
                    Log.i("PresentOnDecrement", strDecDate);
                    //Add the left dates to the list
                    try {
                        dateCopiedWeekDatesList.add(formatter.parse(strDecDate));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                //Add the present date to the list
                dateCopiedWeekDatesList.add(date);
                //Check Right dates and increment
                for (int x = 0; x < dateRight; x++) {   ///Increment dates
                    calendarRight.add(Calendar.DATE, +1);
                    String strIncDate = formatter.format(calendarRight.getTime());
                    Log.i("PresentOnIncrement", strIncDate);
                    //Add the right dates to the list
                    try {
                        dateCopiedWeekDatesList.add(formatter.parse(strIncDate));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }


                //SORT THE ARRAYLIST
                Collections.sort(dateCopiedWeekDatesList);
                ////////Load the days listview with the corresponding workouts
                ArrayList<Integer> loadedPositions = new ArrayList<Integer>();
                weekListViewList.clear();
                for(int t =0 ; t < 7; t++){
                    weekListViewList.add(new Tab2WeekWorkouts("----", t));
                }
                boolean boolAddRest = false;
                int duplicationCounter = 0;
                for (int i = 0; i < dateCopiedWeekDatesList.size(); i++) {
                    for (int x = 0; x < dataRetrievedDates.size(); x++) {
                        if (dateCopiedWeekDatesList.get(i).getDate() == dataRetrievedDates.get(x).getDate() &&
                                dateCopiedWeekDatesList.get(i).getMonth() == dataRetrievedDates.get(x).getMonth() &&
                                dateCopiedWeekDatesList.get(i).getYear() == dataRetrievedDates.get(x).getYear()) {
                            Log.i("PositionWhereEqual", " " + i);
                            /////Now go ahead and save the position of the event in the copied week and it's index in database inside a Map
                            int holdPosition = dateCopiedWeekDatesList.get(i).getDay();
                            int holdDataIndex = dateWorkoutKeyList.get(x);
                            Log.i("HoldDataIndex", holdDataIndex + "");

                            int forWorkoutPosittion = holdDataIndex - 1;
                            String wrkName = workoutNameList.get(forWorkoutPosittion);

                            //Get the location using (holdposition) of the weeklistviewlist with empty names and add the workout name to the object
                            weekListViewList.get(holdPosition).setWorkout(wrkName);

                            //Add the positions to the loadedPositions arraylist
                            loadedPositions.add(holdPosition);

                            Log.i("WeekSelectionData", "Position: " + holdPosition + "     useThisIndex: " + holdDataIndex);
                        }
                    }
                }
                weekDayAdapter.notifyDataSetChanged();

                ////////------------------------------------------------------------------------------
                //Load the exercises corresponding to the event pressed on
                onEventTapedPressed(date);

                //Update calendarLeft data (events and colors)
                //retrieveCalendar();
                calendarView.updateCalendar(eventsSet);

            }
        });

        //////ON BTN COPY PASTE WEEK
        btnCopyPastWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Get the date string into date using the present format then use
                String strDate = "";
                Date editDate = new Date();

                for (int i = 0; i < selectedWeekArrayList.size(); i++) {
                    Log.i("SelectedWeekTab2", selectedWeekArrayList.get(i) + "");
                }

                //Copy the data dates list into a list of type (Date) -------------HOLDS DATA DATES IN ORDER
                ArrayList<Date> copyPasteDateList = new ArrayList<Date>();
                for (int i = 0; i < dataDateList.size(); i++) {
                    Log.i("DataDateList", "Data Date List: " + dataDateList.get(i) + "");
                    strDate = dataDateList.get(i);
                    try {
                        editDate = formatter.parse(strDate);
                        copyPasteDateList.add(editDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                ///////////////////////////////////////////////////////COPY WEEK
                if (boolCopyPaste == false) {
                    copiedWeekArrayList.clear();
                    copiedWeekMap.clear();
                    intoPasteWeekList.addAll(dateCopiedWeekDatesList);


                    for (int i = 0; i < dateCopiedWeekDatesList.size(); i++) {
                        for (int x = 0; x < dataRetrievedDates.size(); x++) {
                            if (dateCopiedWeekDatesList.get(i).getDate() == dataRetrievedDates.get(x).getDate() &&
                                    dateCopiedWeekDatesList.get(i).getMonth() == dataRetrievedDates.get(x).getMonth() &&
                                    dateCopiedWeekDatesList.get(i).getYear() == dataRetrievedDates.get(x).getYear()) {
                                Log.i("PositionWhereEqual", " " + i);
                                /////Now go ahead and save the position of the event in the copied week and it's index in database inside a Map
                                int holdPosition = dateCopiedWeekDatesList.get(i).getDay();
                                int holdDataIndex = dateWorkoutKeyList.get(x);
                                copiedWeekMap.put(holdPosition, holdDataIndex);
                                Log.i("HoldDataIndex", holdDataIndex + "");
                            }
                        }
                    }


                    btnCopyPastWeek.setText("Paste Week");
                    boolCopyPaste = true; //Set to true to enable the paste feature
                } else {
                    //CHECK FIRST TO SEE IF THERE IS CONTENT IN THAT WEEK THEN PROMPT THE USER FOR OVERRIDE CONFIRMATION

                    /////////////////////////////////////////////////////////////PASTE WEEK
                    ArrayList<Integer> positionsToPasteList = new ArrayList<Integer>();
                    ArrayList<Integer> dataIndexesToPasteList = new ArrayList<Integer>();
                    //Retrieve the contents from the copy hashmap into corresponding types that will be inserted into the Calendar Table
                    Iterator it = copiedWeekMap.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry pair = (Map.Entry) it.next();
                        Log.i("MapKeyValue", pair.getKey() + "  " + pair.getValue());
                        int thePosition = (Integer) pair.getKey();
                        int theDataIndex = (Integer) pair.getValue();
                        //Find the date for the position above in the week selected to paste
                        Date dateToSave = dateCopiedWeekDatesList.get(intoPasteWeekList.get(thePosition).getDay());
                        int workoutToSave = workoutIdList.get(theDataIndex - 1);

                        Log.i("FullPasteLogTest", "Date to save: " + dateToSave + "   Workout to save: " + workoutToSave);
                        //Now insert that data into the Calendar Table
                        String strDateToSave = formatter.format(dateToSave);
                        calendarData.insertData(strDateToSave, workoutToSave);
                    }
                    btnCopyPastWeek.setText("Copy Week");
                    boolCopyPaste = false; //Set to false to enable the copy again
                    //EVERYTHING();
                    reloadData();

                }
            }
        });

        ///////////////////////////////////ON BUTTON CLEAR WEEK PRESSED
        buttonClearWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClearWeek.setError("LONG CLICK TO CLEAR WEEK");
                Toast.makeText(getActivity(), "\n\n\nLONG CLICK TO CEAR WEEK\n\n\n", Toast.LENGTH_SHORT).show();
            }
        });
        buttonClearWeek.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                for (int i = 0; i < dateCopiedWeekDatesList.size(); i++) {
                    for (int x = 0; x < dataRetrievedDates.size(); x++) {
                        if (dateCopiedWeekDatesList.get(i).getDate() == dataRetrievedDates.get(x).getDate() &&
                                dateCopiedWeekDatesList.get(i).getMonth() == dataRetrievedDates.get(x).getMonth() &&
                                dateCopiedWeekDatesList.get(i).getYear() == dataRetrievedDates.get(x).getYear()) {
                            Log.i("ClearWeekTest", "Positions Exist For Deletion: " + i);
                            /////Now use the data index of the position's event to delete the data for the week
                            int holdPosition = i;
                            String holdDataIndex = calendarDataIDList.get(x) + "";
                            Log.i("ClearIndexDelete", "Data index to delete: " + holdDataIndex);
                            calendarData.deleteData(holdDataIndex);
                        }
                    }
                }

                //Everything
                //calendarView.updateCalendar(eventsSet, rWeekList, rColorList);
                reloadData();
                return false;
            }
        });

        // On btnDelete clicked event - Delete the CALENDAR_TABLE and CALENDAR_COLOR_TABLE
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                calendarData.deleteTable();
                calendarColorData.deleteTable();
                // EVERYTHING();
                reloadFragment();


            }
        });

        //On btnWeekOptions clicked
        btnCalendarOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Week options
                //showWeekOptionsPopup(v);
                if (!(btnCopyPastWeek.getVisibility() == View.VISIBLE)) {
                    btnCopyPastWeek.setVisibility(View.VISIBLE);
                    buttonClearWeek.setVisibility(View.VISIBLE);
                } else {
                    btnCopyPastWeek.setVisibility(View.GONE);
                    buttonClearWeek.setVisibility(View.GONE);
                }


            }
        });
    }





    //Load workoutList with the exercises on grid event tap, or weekList item clicked
    public void onEventTapedPressed(Date date){
        //Clear the listview for use
        workoutList.clear();
        workoutAdapter.notifyDataSetChanged();

        // TODO Auto-generated method stub
        DateFormat df = SimpleDateFormat.getDateInstance();
        // Format the date and check if it exists in the dataDateList
        String current = "" + df.format(date);

        for (int h = 0; h < dataDateList.size(); h++) {
            Log.i("TestingDateList", dataDateList.get(h));
        }
        Log.i("TestingCurrentDate", current);

        //eventsSet.contains(object)
        if (dataDateList.contains(current)) {
            int in = dataDateList.indexOf(current);
            Log.i("CurrentDate", "" + dataDateList.indexOf(current));

            int workoutID = dateWorkoutKeyList.get(in);
            Log.i("DateWorkoutKeyList", "" + dateWorkoutKeyList.get(in));


            for (int i = 0; i < workoutNameList.size(); i++) {
                Log.i("TheWOrkouts", "" + workoutNameList.get(i));
            }

            //Load the listview with the existing workout
            //Decrement the values by 1 since the saving procedure incremented their values
            ArrayList<String> exerciseIdsList = new ArrayList<String>();
            String exValues = "";
                    /*for(int i = 0; i < workoutExerciseIdList.size(); i++){
                        Log.i("Exercises", workoutExerciseIdList.get(i) + "");
						exValues = workoutExerciseIdList.get(i) + "";
					}*/
            exValues = workoutExerciseIdList.get(workoutID - 1) + "";
            Log.i("IntExcValues", exValues + "");
            char[] exChar = exValues.toCharArray();
            for (int i = 0; i < exChar.length; i++) {
                String val = exChar[i] + "";
                int intVal = Integer.parseInt(val) - 1;
                exerciseIdsList.add(intVal + "");
            }

            for (int i = 0; i < exerciseIdsList.size(); i++) {
                workoutList.add(exerciseList.get(Integer.parseInt(exerciseIdsList.get(i))));
            }
            workoutAdapter.notifyDataSetChanged();

            Toast.makeText(getActivity(), workoutNameList.get(workoutID - 1) + "   WORKOUT", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "No Workout Scheduled",
                    Toast.LENGTH_SHORT).show();
        }
        for (int h = 0; h < dateWorkoutKeyList.size(); h++) {
            Log.i("WorkoutKeysDate", " " + dateWorkoutKeyList.get(h));
        }
    }

    // Update the event
    public void updateEvents(Date date) {
        Log.i("testing-data-string", "this is the date: " + date);

        // Put selected dates into the dateSet then update the adapter
        // 'calenderView.updateCalendar(date)'
        eventsSet.add(date);
        calendarView.updateCalendar(eventsSet);

    }

    // _________________________________________________________________________________________________________________________________
    // Popup settings for long click create event
    public void showPopup(View view) {
        View popupView = getActivity().getLayoutInflater().inflate(R.layout.calender_popup, null);
        final PopupWindow popupWindow = new PopupWindow(popupView, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        listViewSelect = (ListView) popupView.findViewById(R.id.listCalendarSelect);
        calendarAdapter = new ArrayAdapter<String>(getActivity(), R.layout.text_item_light, workoutNameList);
        listViewSelect.setAdapter(calendarAdapter);


        // On listViewSelect item select
        listViewSelect.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                // Mark this event - Pass the primaryDate (which contains the
                // date clicked on the list) into updateEvents method
                updateEvents(primaryDate);
                // Add to database - add the date as a string then parse it to
                // date when retrieved
                DateFormat df = SimpleDateFormat.getDateInstance();
                calendarData.insertData(df.format(primaryDate) + "", workoutIdList.get(position));

                Toast.makeText(getActivity(), "This is: " + workoutNameList.get(position), Toast.LENGTH_SHORT).show();

                // Refresh the view
                //EVERYTHING();
                //reloadFragment();


                //PopBackStack will load the previous fragment which automatically loads this fragment making it a seamless transition
                //getActivity().getSupportFragmentManager().popBackStack("WorkoutTab2Tag", FragmentManager.POP_BACK_STACK_INCLUSIVE);

                //Close the window
                popupWindow.dismiss();

            }
        });

        // If the PopupWindow should be focusable
        popupWindow.setFocusable(true);
        // If you need the PopupWindow to dismiss when when touched outside
        popupWindow.setBackgroundDrawable(new ColorDrawable());
        int location[] = new int[2];
        // Get the View's(the one that was clicked in the Fragment) location
        view.getLocationOnScreen(location);
        // Using location, the PopupWindow will be displayed right under
        // anchorView
        popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, location[0], location[1] + view.getHeight());
    }

    //Load the events data here to take some of the load off the adapter
    public void retrieveColorWeekData() {
        calendarColorData = new CalendarColorData(getContext());
        Cursor wCursor;
        try {
            wCursor = calendarColorData.getAllData();
            // Check for data
            if (wCursor.getCount() == 0) {
                Log.i("RetrieveWorkout", "NO DATA : " + wCursor.getCount());
            } else {
                Log.i("RetrieveWorkout", "SUCCESS: " + wCursor.getCount());
                while (wCursor.moveToNext()) {
                    // Get data
                    // This arraylist gets the workout names
                    rWeekList.add(Integer.parseInt(wCursor.getString(1)));
                    // This arraylist gets the workout primary keys
                    rColorList.add(wCursor.getString(2));
                }
            }
        } catch (Exception e) {
            // Toast.makeText(getActivity(), "No Data Loaded",
            // Toast.LENGTH_SHORT).show();
        }
    }

    // ________________________________________________________________________________________________

    /**
     * These options will handle the ability to copy a week's workouts to another chosen week, clear and entire week,
     * clean an entire month, master clear(CLEARS EVERYTHING), and change the week's workout's box color.
     **/
    public void showWeekOptionsPopup(View view) {
        View popupView = getActivity().getLayoutInflater().inflate(R.layout.calendar_options, null);
        final PopupWindow popupWindow = new PopupWindow(popupView, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);


        //Color spinner - allows user to choose a color for the week's workout's box

        // If the PopupWindow should be focusable
        popupWindow.setFocusable(true);
        // If you need the PopupWindow to dismiss when when touched outside
        popupWindow.setBackgroundDrawable(new ColorDrawable());
        int location[] = new int[2];
        // Get the View's(the one that was clicked in the Fragment) location
        view.getLocationOnScreen(location);
        // Using location, the PopupWindow will be displayed right under
        // anchorView
        popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, location[0], location[1] + view.getHeight());
    }

    // __________________________________________________________________________________________
    // DATA RETRIEVAL METHODS
    // Get all data from Calendar table
    public void retrieveCalendar() {
        // Load everything into arraylists
        // ============================================================================================================
        // Use an EXERCISE_TABLE arraylist for the ListView items

        // Retrieve data from EXERCISE_TABLE if any into excDataList
        Cursor cursor, cursorD;
        //Clear before use
        calendarDataIDList.clear();
        dataDatesList.clear();
        dateWorkoutKeyList.clear();
        dataDateList.clear();
        dataRetrievedDates.clear();
        dateIdList.clear();
        eventsSet.clear();
        try {
            cursor = calendarData.getAllData();
            cursorD = calendarData.getAllData();
            // Check for data
            if (cursor.getCount() == 0) {
                //Since there is not data being loaded
                //Clear events and update calendar in case it was previously called for week clear reload
                eventsSet.clear();
                calendarView.updateCalendar(eventsSet);
            } else {
                while (cursor.moveToNext()) {
                    // Get date keys
                    calendarDataIDList.add(Integer.parseInt(cursor.getString(0)));
                    //Get dates
                    dataDatesList.add(cursor.getString(1));
                    // This arraylist captures the workout keys
                    dateWorkoutKeyList.add(Integer.parseInt(cursor.getString(2)));
                    // This arraylist captuers the dates
                    dataDateList.add(cursor.getString(1));
                    //Add the dates into a list of type <Date>
                    dataRetrievedDates.add(formatter.parse(cursor.getString(1)));
                    Log.i("TestingCalendarData",
                            "The date: " + cursor.getString(1) + "| The workout KEY: " + cursor.getString(2));
                    dateIdList.add(cursor.getString(0));
                }
                //The dataDateList is not empty then copy dates in eventsSet
                if (!(dataDateList.isEmpty())) {
                    eventsSet.clear();
                    for (int i = 0; i < dataDateList.size(); i++) {
                        // Format the date
                        Date d = null;
                        String sd = dataDateList.get(i);
                        Log.i("StringBeingLoaded", " " + sd);

                        try {
                            d = formatter.parse(sd);
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            Log.i("Data-Not-Parsed", "Error in format");
                        }
                        Log.i("DataBeingLoadedHere", eventsSet.size() + "");
                        eventsSet.add(d);
                    }
                }
                // Update calendar right after  ---------------------------Saved data loaded
                calendarView.updateCalendar(eventsSet);
            }
        } catch (Exception e) {

        }


    }

    // Retrieve workouts from WORKOUTS_TABLE
    public void retrieveWorkouts() {
        Cursor wCursor;
        Cursor wCursor2;
        try {
            wCursor = workoutData.getAllData();
            wCursor2 = workoutData.getAllData();

            // Check for data
            if (wCursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();
                Log.i("RetrieveWorkout", "NO DATA : " + wCursor.getCount());
            } else {
                Log.i("RetrieveWorkout", "SUCCESS: " + wCursor.getCount());
                //Clear before use
                workoutNameList.clear();
                workoutIdList.clear();
                workoutExerciseIdList.clear();

                while (wCursor.moveToNext()) {
                    // Get data
                    // This arraylist gets the workout names
                    workoutNameList.add(wCursor.getString(1));

                    // This arraylist gets the workout primary keys
                    workoutIdList.add(Integer.parseInt(wCursor.getString(0)));

                    //This arraylist gets the exercise id's
                    workoutExerciseIdList.add(Integer.parseInt(wCursor.getString(2)));
                }
            }
        } catch (Exception e) {
            // Toast.makeText(getActivity(), "No Data Loaded",
            // Toast.LENGTH_SHORT).show();
        }
    }

    // load the exercise keys, names, and set count from EXERCISES_TABLE
    public void retrieveExercises() {
        Cursor exCursor;
        try {
            exCursor = exerciseData.getAllData();
            // Check for data
            if (exCursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();
            } else {
                //Clear before use
                exerciseList.clear();
                exerciseSetsList.clear();
                while (exCursor.moveToNext()) {
                    // Get exercise names
                    exerciseList.add(exCursor.getString(1));
                    // Get exercise set count
                    exerciseSetsList.add(Integer.parseInt(exCursor.getString(2)));
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "No Data Loaded", Toast.LENGTH_SHORT).show();
        }
    }

    // =====================ALERT DIALOG===================//with deleteMethod
    // at the bottom
    public void alertDialog(final String position) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage("Delete Workout");
        builder1.setCancelable(true);

        builder1.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //Delete the selected date
                //calendarData.deleteData("");


                //calendarData.deleteData(""+ dateDataPosition);
                WORKOUT_EDIT.deleteCalendarDate(dateDataPosition);
                //EVERYTHING();
                reloadFragment();

                // Close dialog
                dialog.cancel();
            }
        });

        builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // Close dialog
                dialog.cancel();
            }
        });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    //////////////////////////////////CLEAR ALL THE ARRAYLISTS//////////////////////////////
    public void reloadData() {
        //Clear the lists once completed
        /*calendarDataIDList.clear();
        dateWorkoutKeyList.clear();
        dataDateList.clear();
        workoutIdList.clear();
        workoutNameList.clear();
        workoutExerciseIdList.clear();
        dateIdList.clear();
        exerciseList.clear();
        exerciseSetsList.clear();
        colorList.clear();
        rColorList.clear();
        rWeekList.clear();
        selectedWeekArrayList.clear();
        copiedWeekArrayList.clear();
        pastedWeekArrayList.clear();
        dataDatesList.clear();
        dateCopiedWeekDatesList.clear();
        dataRetrievedDates.clear();
        */


        //EVERYTHING();
        //reloadFragment();
        retrieveWorkouts();
        retrieveCalendar();
        retrieveExercises();
        calendarView.updateCalendarFromWorkoutTab2();

    }

    /////////////////////////////////RELOAD THE FRAGMENT BY CALLING A NEW
    public void reloadFragment() {
        Fragment fragment = new WorkoutTab2();
        FragmentTransaction fT = getActivity().getSupportFragmentManager().beginTransaction();
        fT.replace(R.id.intoWorkoutTab2, fragment, "WorkoutTab2Tag");
        fT.commit();
    }


}