package onedev.com.stayfit.Workout;

/**
 * Created by Juan on 1/4/2017.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import onedev.com.stayfit.R;


/**
 * Created by Juan on 1/1/2017.
 */


public class Tab3PreviewAdapter extends ArrayAdapter {
    private ArrayList<Tab3Exercise> exercisesList;
    private int res;
    private LayoutInflater inflater;
    private Context context;

    public Tab3PreviewAdapter(Context context, int resourceId, ArrayList<Tab3Exercise> exercises) {

        super(context, resourceId, exercises);
        exercisesList = exercises;
        res = resourceId;
        inflater = LayoutInflater.from(context);
        this.context = context;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //Inflate a new layout view
        convertView = (LinearLayout) inflater.inflate(res, null);

        TextView txtName = (TextView) convertView.findViewById(R.id.tvTab3ExcNames);
        txtName.setText(exercisesList.get(position).getName());

        TextView tvSets = (TextView) convertView.findViewById(R.id.tvTab3ExcSets);
        tvSets.setText(exercisesList.get(position).getSets());

        return convertView;
    }


    @Override
    public Object getItem(int position) {

        return super.getItem(position);
    }
}



