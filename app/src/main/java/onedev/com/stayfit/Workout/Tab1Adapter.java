package onedev.com.stayfit.Workout;

/**
 * Created by Juan on 8/2/2016.
 */

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import onedev.com.stayfit.R;

public class Tab1Adapter extends BaseAdapter {

    ArrayList<String> exerciseNameList;
    ArrayList<String> repsList;
    Activity context;
    boolean[] itemChecked;

    //Shared preference var
    SharedPreferences sharedPreferences;
    final String PREFS_KEY = "CheckedItems";

    ViewHolder mainViewHolder;

    public Tab1Adapter(Activity context, ArrayList<String> exerciseNameList, ArrayList<String> repsList) {
        super();
        this.context = context;
        this.exerciseNameList = exerciseNameList;
        this.repsList = repsList;
        itemChecked = new boolean[exerciseNameList.size()];
    }

    private class ViewHolder {
        TextView textView;
        CheckBox ck1;
    }

    public int getCount() {
        return exerciseNameList.size();
    }

    public Object getItem(int position) {
        return exerciseNameList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        LayoutInflater inflater = context.getLayoutInflater();
        //Initialize sharedPreferences
        sharedPreferences = context.getSharedPreferences(PREFS_KEY + "", Context.MODE_PRIVATE);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.costume_listview, null);
            holder = new ViewHolder();
            mainViewHolder = holder;

            holder.textView = (TextView) convertView
                    .findViewById(R.id.viewAdapterReps);
            holder.ck1 = (CheckBox) convertView
                    .findViewById(R.id.viewAdapterCheck);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        String appName = exerciseNameList.get(position);
        holder.textView.setText("Reps " +repsList.get(position));
        holder.textView.setCompoundDrawablePadding(15);
        holder.ck1.setText(appName);
        holder.ck1.setChecked(false);

        if (itemChecked[position]) {
            holder.ck1.setChecked(true);
        } else {
            holder.ck1.setChecked(false);
        }
        holder.ck1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                //Save items to shardprefs
                String savedItems = "";
                int count = getCount();
                int savePosition;
                savedItems = sharedPreferences.getString(PREFS_KEY, "");

                Log.i("TestingPositionSaves", savedItems + "");

                //If sharedPrefs is empty save the first position.
                if (savedItems.isEmpty()) {
                    savedItems = position + "";
                    sharedPreferences.edit().putString(PREFS_KEY, savedItems).commit();
                    Log.i(PREFS_KEY, "Position Saved");
                } else {
                    //Save the position to the string
                    if (holder.ck1.isPressed()) {
                        Log.i("IsClickedPosition", position + "");

                        char[] charArray = savedItems.toCharArray();
                        ArrayList<String> positionList = new ArrayList<String>();
                        //Add the char array of positions to an array list
                        for (int x = 0; x < charArray.length; x++) {
                            positionList.add(charArray[x] + "");
                        }
                        for (int i = 0; i < positionList.size(); i++) {
                            //If position exists, then remove and save the updated string
                            if (positionList.get(i).equals(position + "")) {
                                Log.i("RemovedPosition", "Position REMOVED");
                                positionList.remove(i);
                                String newString = "";
                                for (int x = 0; x < positionList.size(); x++) {
                                    newString = newString + positionList.get(x);
                                }
                                sharedPreferences.edit().putString(PREFS_KEY, newString).commit();
                                break;
                            } else {
                                //Double check for it's existence (ONLY WAY I FOUND TO MAKE IT WORK)
                                if (positionList.contains(position + "")) {
                                /////DO NOTHING HERE//////

                                } else {
                                    //Add the updated positions to a new string
                                    Log.i("Added_Position", "Position ADDED");
                                    positionList.add(position + "");
                                    String newString = "";
                                    for (int x = 0; x < positionList.size(); x++) {
                                        newString = newString + positionList.get(x);
                                    }
                                    sharedPreferences.edit().putString(PREFS_KEY, newString).commit();
                                    break;
                                }
                            }

                        }


                    } else {

                    }

                }


                if (holder.ck1.isChecked()) {
                    itemChecked[position] = true;
                    holder.textView.setTextColor(Color.parseColor("#11BAFF"));
                    holder.ck1.setTextColor(Color.parseColor("#11BAFF"));
                    Log.i("PositionCheked", position + " has been checked");

                } else {
                    itemChecked[position] = false;
                    holder.textView.setTextColor(Color.BLACK);
                    holder.ck1.setTextColor(Color.BLACK);
                }
            }
        });

        return convertView;

    }


}
