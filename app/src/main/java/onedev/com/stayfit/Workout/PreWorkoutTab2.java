package onedev.com.stayfit.Workout;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import onedev.com.stayfit.Progress.RouteViewer;
import onedev.com.stayfit.R;
import onedev.com.stayfit.WorkoutFragment;

/**
 * Created by Juan on 12/28/2016.
 */

/*THIS FRAGMENT IS ONLY USED TO INVOKE THE WORKOUT TAB 2 FRAGMENT.*/
public class PreWorkoutTab2 extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.workout_tab_2_pre, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        WorkoutFragment workoutFragment = new WorkoutFragment();


        Fragment fragment = new WorkoutTab2();
        FragmentTransaction fT = getActivity().getSupportFragmentManager().beginTransaction();
        fT.replace(R.id.intoWorkoutTab2, fragment, "WorkoutTab2Tag");
        fT.addToBackStack("WorkoutTab2Tag");
        fT.commit();

    }


}
