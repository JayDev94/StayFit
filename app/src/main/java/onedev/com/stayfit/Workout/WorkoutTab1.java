package onedev.com.stayfit.Workout;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


import onedev.com.stayfit.R;
import onedev.com.stayfit.WorkoutDatabase.CalendarData;
import onedev.com.stayfit.WorkoutDatabase.ExerciseData;
import onedev.com.stayfit.WorkoutDatabase.SetsData;
import onedev.com.stayfit.WorkoutDatabase.WorkoutData;
import onedev.com.stayfit.WorkoutDatabase.WorkoutStatsData;


import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class WorkoutTab1 extends Fragment {
    // Data Table Classes
    CalendarData calendarData;
    WorkoutData workoutData;
    ExerciseData exerciseData;
    WorkoutStatsData workoutStatsData;
    SetsData setsData;

    // Current date
    DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
    Date date = new Date();
    SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");

    // Workout ids - Workout names - Workout exercise Id's
    ArrayList<String> calendarDates;
    ArrayList<Integer> calendarWList;
    ArrayList<Integer> workoutIdList;
    ArrayList<String> workoutNameList;
    ArrayList<String> workoutExcIdList;

    // Exercise names, Exercise set count, Set's reps, Set's excReference, Set's
    // weight
    ArrayList<Integer> ExercisePKeys;

    ArrayList<Integer> excPrimaryKeys;
    ArrayList<String> excNameList;
    ArrayList<Integer> excSetCountList;
    ArrayList<String> setsRepsWeight;
    ArrayList<String> setsExcReferenceList;
    ArrayList<String> setsRepsOnlyList;

    ArrayList<Integer> holdSortedExcList;
    //UI var
    TextView tvWorkoutName;
    ListView listView;
    Button btnComplete;
    TextView viewWeight;
    TextView viewCalories;
    // Links to the exercise list class
    ArrayList<Exercise> exerciseList;
    ArrayList<String> infoList;
    ArrayAdapter<String> secondAdapter;
    // Main components
    String excName = "";
    String wName = "";
    int setCount = 0;
    int reps = 0;
    int weight = 0;

    boolean workoutOn = true;

    //SharedPreferences for listview checked items
    SharedPreferences sharedPreferences;
    ArrayList<String> checkedItemsList;
    final String PREFS_KEY = "CheckedItems";
    //SharedPreferences for workout completion
    SharedPreferences sharedPreferencesCompleted;
    final String PREFS_COMPLETE_KEY = "WorkoutComplete";
    boolean completedWorkout;


    //Decides exercise completion
    boolean exerciseDone = true;
    int listPosition = 0;
    String exerciseCompletion = "";

    //Checked items listview costume adapter
    Tab1Adapter adapter;


    //Completed lists
    ArrayList<String> completedList1;
    ArrayList<String> completedList2;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        return inflater.inflate(R.layout.workout_tab1, container, false);


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        //Initialize the sharedPreferences
        sharedPreferences = getActivity().getSharedPreferences(PREFS_KEY + "", Context.MODE_PRIVATE);
        sharedPreferencesCompleted = getActivity().getSharedPreferences(PREFS_COMPLETE_KEY, Context.MODE_PRIVATE);


        // Initialize data classes
        calendarData = new CalendarData(getActivity());
        workoutData = new WorkoutData(getActivity());
        exerciseData = new ExerciseData(getActivity());
        setsData = new SetsData(getActivity());
        workoutStatsData = new WorkoutStatsData(getActivity());

        listView = (ListView) view.findViewById(R.id.listWorkout);
        btnComplete = (Button) view.findViewById(R.id.btnFinishWorkout);
        tvWorkoutName = (TextView) view.findViewById(R.id.tab1WorkoutName);

        // Arraylists initialization
        ExercisePKeys = new ArrayList<Integer>();
        calendarDates = new ArrayList<String>();
        calendarWList = new ArrayList<Integer>();
        workoutNameList = new ArrayList<String>();
        workoutIdList = new ArrayList<Integer>();
        workoutExcIdList = new ArrayList<String>();
        excPrimaryKeys = new ArrayList<Integer>();
        excNameList = new ArrayList<String>();
        excSetCountList = new ArrayList<Integer>();
        setsRepsWeight = new ArrayList<String>();
        setsExcReferenceList = new ArrayList<String>();
        holdSortedExcList = new ArrayList<Integer>();
        checkedItemsList = new ArrayList<>();
        setsRepsOnlyList = new ArrayList<>();

        //Completed lists
        completedList1 = new ArrayList<>();
        completedList2 = new ArrayList<>();


        // Holds the exercise ->links to the Exercise class which includes the
        // following args
        exerciseList = new ArrayList<Exercise>();
        infoList = new ArrayList<>();
        infoList.add("Something1");
        infoList.add("Something2");
        infoList.add("Something3");


        // Test: add an exercise
        Exercise exercise;
        /*
         * Exercise exercise = new Exercise("Bench Press", "4", "10", false);
		 * exerciseList.add(exercise); exercise = new Exercise("Squat", "4",
		 * "10", false); exerciseList.add(exercise); exercise = new Exercise(
		 * "Dumbell Curls", "4", "10", false); exerciseList.add(exercise);
		 */

        // Retrieve data
        retrieveExercises();
        retrieveSets();
        retrieveWorkouts();
        retrieveCalendar();

        //Retreive sharedPrefs
        if (sharedPreferences.contains(PREFS_KEY)) {
            exerciseCompletion = sharedPreferences.getString(PREFS_KEY, null);
            Log.i("PREFERENCEKEY", exerciseCompletion);

            char[] charArray = exerciseCompletion.toCharArray();
            //Add that data to an arraylist for the adapter to decide the status of items
            for (int i = 0; i < exerciseCompletion.length(); i++) {
                checkedItemsList.add(charArray[i] + "");
            }
        }
        //Retrieve result from the completion shared preferences
        if (sharedPreferencesCompleted.contains(PREFS_COMPLETE_KEY)) {
            completedWorkout = sharedPreferencesCompleted.getBoolean(PREFS_COMPLETE_KEY, false);
        } else {
            completedWorkout = false;
        }

        if (completedWorkout) {                                                          /////WORKOUT COMPLETION CONDITION
            Log.i("TestingCompletedWrk", completedWorkout + "");
            btnComplete.setText("WORKOUT HAS BEEN COMPLETED");
        }

        if (!(workoutOn == false)) {
            for (int i = 0; i < calendarDates.size(); i++) {
                Log.i("Created-Dates", calendarDates.get(i)); // Worked
            }

            String strToday = dateFormat.format(date);
            if (calendarDates.contains(strToday)) {
                // Get the index of the event
                int calKey = calendarDates.indexOf(strToday);
                // get the workout key of the event
                final int workKey = calendarWList.get(calKey) - 1;
                Log.i("THE_KEY_SOMETHING", "" + workKey); // Worked

                //Get the workout name using the key and set it in the TextView
                tvWorkoutName.setText(workoutNameList.get(workKey));

                // Get the exercise keys using that key
                String workoutExercises = "" + workoutExcIdList.get(workKey);
                Log.i("TESTING_NAME_WORKOUT", "" + workoutExercises);// Worked

                // Get the list of exercises from that workout- use workKey -Add
                // to excPrimaryKeys
                for (int i = 0; i < workoutExercises.length(); i++) {
                    char ch = workoutExercises.charAt(i);
                    String s = "" + ch;
                    ExercisePKeys.add(Integer.parseInt(s));
                }

                for (int i = 0; i < ExercisePKeys.size(); i++) {
                    Log.i("ExercisePKeys", " " + ExercisePKeys.get(i));
                }

                for (int i = 0; i < excPrimaryKeys.size(); i++) {
                    Log.i("excPrimaryKeys", " " + excPrimaryKeys.get(i));
                }

                // Use the ExercisePKeys to get the exercise names and set
                // count- a
                ArrayList<String> loadedExerciseNameList = new ArrayList<>();
                ArrayList<String> loadedSetsList = new ArrayList<>();
                ArrayList<String> loadedExerciseId = new ArrayList<>();
                for (int i = 0; i < excNameList.size(); i++) {
                    // while(excPrimaryKeys.get(i))
                    for (int s = 0; s < ExercisePKeys.size(); s++) {
                        if (excPrimaryKeys.get(i) == ExercisePKeys.get(s)) {
                            holdSortedExcList.add(excPrimaryKeys.get(i));

                            Log.i("New_Sorted_idList", "" + excPrimaryKeys.get(i));
                            loadedExerciseNameList.add(excNameList.get(i));
                            loadedSetsList.add(String.valueOf(excSetCountList.get(i)));
                            loadedExerciseId.add(String.valueOf(excPrimaryKeys.get(i)));

                            // Load the exercises into the custom listview
                            exercise = new Exercise(excNameList.get(i), "" + excSetCountList.get(i), "10", false);
                            exerciseList.add(exercise);
                        }
                    }
                }


                //secondAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_multiple_choice, infoList);
                //listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                //listView.setAdapter(secondAdapter);

                //Load the completed lists with the current list for completion
                completedList1.addAll(loadedExerciseNameList);
                completedList2.addAll(loadedSetsList);

                //Initialize the adapter and override the view to select the sharedPrefs saved data
                adapter = new Tab1Adapter(getActivity(), loadedExerciseNameList, loadedSetsList) {
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View v = super.getView(position, convertView, parent);
                        TextView tv = (TextView) v.findViewById(R.id.viewAdapterReps);
                        CheckBox cB = (CheckBox) v.findViewById(R.id.viewAdapterCheck);

                        for (int i = 0; i < checkedItemsList.size(); i++) {
                            //If the position is in the checkedItems list, set it's checked settings
                            if (position == Integer.parseInt(checkedItemsList.get(i))) {
                                tv.setTextColor(Color.parseColor("#11BAFF"));
                                cB.setTextColor(Color.parseColor("#11BAFF"));
                                tv.setBackgroundColor(Color.parseColor("#D5FFCF"));
                                cB.setBackgroundColor(Color.parseColor("#D5FFCF"));
                                cB.setChecked(true);
                            }
                        }
                        //Checkbox is disabled if the workout is complete
                        if (completedWorkout) {
                            cB.setEnabled(false);
                            //cB.setVisibility(View.GONE);
                        }
                        return v;

                    }
                };
                listView.setAdapter(adapter);

                //Listview on item clicked  -- save the item
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        //Show popup with selected exercise info
                        Log.i("ListViewExcPrime", position + "");
                        for (int i = 0; i < holdSortedExcList.size(); i++) {
                            Log.i("HoldSortedExcList", holdSortedExcList.get(i) + "");
                        }

                        showPopup(view, holdSortedExcList.get(position));
                    }
                });

                //TESTING------DELETE SHAREDPREFS ON BTN CLICKED  ---------NEW TEST: SET SAVE INTO COMPLETION SHARED PREFERENCES
                btnComplete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //deleteSharedPrefs();

                        new AlertDialog.Builder(getActivity()).setTitle("Complete").setMessage("" +
                                "Are you done with this workout?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Log.i("CompletionButton", "Workout Completion Dialog Complete");
                                        //Completion logic

                                        completedWorkout = true;
                                        btnComplete.setText("Workout Completed");
                                        sharedPreferencesCompleted.edit().putBoolean(PREFS_COMPLETE_KEY, completedWorkout).commit();

                                        //Save into database workout stats table
                                        Date dataDate = new Date();
                                        String strSaveDate = formatter.format(dataDate);
                                        Log.i("SavingCurrentDate", strSaveDate + "   Workout ID: " + workoutIdList.get(workKey));
                                        workoutStatsData.insertData(strSaveDate, workoutIdList.get(workKey));

                                        //Set the adapter again with desired effects for completion
                                        adapter = new Tab1Adapter(getActivity(), completedList1, completedList2) {
                                            @Override
                                            public View getView(int position, View convertView, ViewGroup parent) {
                                                View v = super.getView(position, convertView, parent);
                                                TextView tv = (TextView) v.findViewById(R.id.viewAdapterReps);
                                                CheckBox cB = (CheckBox) v.findViewById(R.id.viewAdapterCheck);

                                                for (int i = 0; i < checkedItemsList.size(); i++) {
                                                    //If the position is in the checkedItems list, set it's checked settings
                                                    if (position == Integer.parseInt(checkedItemsList.get(i))) {
                                                        tv.setTextColor(Color.parseColor("#11BAFF"));
                                                        cB.setTextColor(Color.parseColor("#11BAFF"));
                                                        tv.setBackgroundColor(Color.parseColor("#D5FFCF"));
                                                        cB.setBackgroundColor(Color.parseColor("#D5FFCF"));
                                                        cB.setChecked(true);
                                                    }
                                                }
                                                //Checkbox is disabled if the workout is complete
                                                if (completedWorkout) {
                                                    cB.setEnabled(false);
                                                    //cB.setVisibility(View.GONE);
                                                }
                                                return v;

                                            }
                                        };
                                        listView.setAdapter(adapter);


                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        ///////NOTHING FOR THE CANCEL
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                });


                Toast.makeText(getActivity(), "The Event Exists", Toast.LENGTH_SHORT).show();
            } else {
                Log.i("TestingSomething", "Test");
                //if there is no workout for today, delete the sharedprefs
                deleteSharedPrefs();
                btnComplete.setBackgroundColor(Color.parseColor("#F7CE00"));
                btnComplete.setText("No Workout Scheduled");
                tvWorkoutName.setText("No Workout Scheduled For Today");
                tvWorkoutName.setBackgroundColor(Color.parseColor("#EBEBEB"));
                Toast.makeText(getActivity(), "No workout today " + dateFormat.format(date), Toast.LENGTH_SHORT).show();
            }


        } else {

        }


    }


    // This is the PopUp editor for the workout module
    public void showPopup(View view, int id) {
        View popupView = getActivity().getLayoutInflater().inflate(R.layout.workout_exercise_popup, null);
        PopupWindow popupWindow = new PopupWindow(popupView, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        ArrayAdapter<String> setsAdapter;
        ArrayList<String> currentSetsList = new ArrayList<String>();
        ListView list = (ListView) popupView.findViewById(R.id.listToday);
        setsAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_small_text_item_green, currentSetsList);
        list.setAdapter(setsAdapter);

        //Use id to find the sets to load

        Log.i("IdTestAgain", id + "");
        ArrayList<Integer> presentSetsForIdList = new ArrayList<>();
        for (int x = 0; x < setsExcReferenceList.size(); x++) {
            if (id == Integer.parseInt(setsExcReferenceList.get(x))) {
                currentSetsList.add(setsRepsWeight.get(x));
            }
        }
        for (int i = 0; i < currentSetsList.size(); i++) {
            Log.i("CurrentSetsList", currentSetsList.get(i));
        }

        setsAdapter.notifyDataSetChanged();


        // If the PopupWindow should be focusable
        popupWindow.setFocusable(true);
        // If you need the PopupWindow to dismiss when when touched outside
        popupWindow.setBackgroundDrawable(new ColorDrawable());
        int location[] = new int[2];
        // Get the View's(the one that was clicked in the Fragment) location
        view.getLocationOnScreen(location);
        // Using location, the PopupWindow will be displayed right under
        // anchorView
        popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, location[0], location[1] + view.getHeight());
    }

    /// ________________________________________Exercise_and_Sets_Data_Methods_______________________________________________________________________
    // load the exercise keys, names, and set count from EXERCISES_TABLE
    public void retrieveExercises() {
        Cursor cursor;
        try {
            cursor = exerciseData.getAllData();
            // Check for data
            if (cursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();
                workoutOn = false;
            } else {
                while (cursor.moveToNext()) {
                    // Get exercise pimary keys (ID's)
                    excPrimaryKeys.add(Integer.parseInt(cursor.getString(0)));
                    // Get exercise names
                    excNameList.add(cursor.getString(1));
                    // Get exercise set count
                    excSetCountList.add(Integer.parseInt(cursor.getString(2)));
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "No Data Loaded", Toast.LENGTH_SHORT).show();
        }
    }

    // Retrieve data from SETS_TABLE into arraylist setsExcIdList and setsDataList
    public void retrieveSets() {
        Cursor cursor;
        try {
            cursor = setsData.getAllData();
            // Check for data
            if (cursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();
                workoutOn = false;
            } else {
                while (cursor.moveToNext()) {
                    // Get data from index
                    setsExcReferenceList.add(cursor.getString(1));
                    setsRepsWeight.add("Reps: " + cursor.getString(2) + "  Weight: " + cursor.getString(3));
                    setsRepsOnlyList.add(cursor.getString(2));
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "No Data Loaded", Toast.LENGTH_SHORT).show();
        }
    }

    // _________________________________________WORKOUTDATA_________________________________________________________
    // Retrieve workouts from WORKOUTS_TABLE
    public void retrieveWorkouts() {
        Cursor wCursor;
        Cursor cursor;
        Cursor eCursor;

        try {
            wCursor = workoutData.getAllData();
            cursor = workoutData.getAllData();
            eCursor = workoutData.getAllData();

            // Check for data
            if (wCursor.getCount() == 0) {
                Toast.makeText(getActivity(), "NO DATA", Toast.LENGTH_SHORT).show();
                Log.i("RetrieveWorkout", "NO DATA : " + wCursor.getCount());
                workoutOn = false;
            } else {
                Log.i("RetrieveWorkout", "SUCCESS: " + wCursor.getCount());
                while (wCursor.moveToNext()) {
                    // Get data
                    // This arraylist gets the workout primary keys
                    workoutIdList.add(Integer.parseInt(wCursor.getString(0)));
                }
                while (cursor.moveToNext()) {
                    workoutNameList.add(cursor.getString(1));
                }
                while (eCursor.moveToNext()) {
                    workoutExcIdList.add(eCursor.getString(2));
                }
            }
        } catch (Exception e) {
            // Toast.makeText(getActivity(), "No Data Loaded",
            // Toast.LENGTH_SHORT).show();
        }
    }

    // _________________________________________CALENDAR_DATA____________________________________________________________________
    public void retrieveCalendar() {
        // Load everything into arraylists
        // ============================================================================================================
        // Use an EXERCISE_TABLE arraylist for the ListView items

        // Retrieve data from EXERCISE_TABLE if any into excDataList
        Cursor cursor;
        try {
            cursor = calendarData.getAllData();
            // Check for data
            if (cursor.getCount() == 0) {
                // Toast.makeText(getActivity(), "NO DATA",
                // Toast.LENGTH_SHORT).show();
                workoutOn = false;
            } else {
                // Toast.makeText(getActivity(), "Data is present in
                // TAB3-Popup", Toast.LENGTH_SHORT).show();
                while (cursor.moveToNext()) {
                    // Get data
                    // This arraylist captures the workout keys
                    calendarWList.add(Integer.parseInt(cursor.getString(2)));
                    // This arraylist captures the dates - Convert date format
                    Date d = formatter.parse(cursor.getString(1));
                    String formatedString = formatter.format(d);
                    calendarDates.add(formatedString);
                    Log.i("TestingCalendarData",
                            "The date: " + cursor.getString(1) + "| The workout KEY: " + cursor.getString(2));
                }
            }
        } catch (Exception e) {
            // Toast.makeText(getActivity(), "No Data Loaded",
            // Toast.LENGTH_SHORT).show();
        }

    }

    public void deleteSharedPrefs() {
        sharedPreferences.edit().clear().commit();
        sharedPreferencesCompleted.edit().clear().commit();
        Toast.makeText(getActivity(), "SharedPreferences DELETED", Toast.LENGTH_SHORT).show();
    }

}
